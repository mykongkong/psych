package com.ssm.psych.service.test;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.TesterVo;

import java.util.List;
import java.util.Map;

/**
 * @author ：
 * @date ：Created in 2022/12/16 10:52
 * @description：
 * @modified By：
 * @version:
 */

public interface TblTestResultSasService {
    int insertResultSas(List<Map<String, Object>> answers, Integer testerId);

    AjaxResult saveResultSas(List<Map<String, Object>> answers, Integer testerId);

    String getResult(Integer testerId);

    AjaxResult queryPhone(TesterVo testerVo);
}