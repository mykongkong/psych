package com.ssm.psych.service.test;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.TesterVo;
import com.ssm.psych.domain.ViewTestPlan;

import java.util.List;

/**
 * @author ：
 * @date ：Created in 2022/12/13 19:30
 * @description：
 * @modified By：
 * @version:
 */

public interface ViewTestPlanService {
    AjaxResult queryAll(Integer testCode, TesterVo testerVo);
    List<ViewTestPlan> findAll(Integer testCode);
}
