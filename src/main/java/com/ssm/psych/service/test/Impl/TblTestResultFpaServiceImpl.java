package com.ssm.psych.service.test.Impl;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.fpa.ViewTestResultFpaDao;
import com.ssm.psych.dao.test.TblTestResultDao;
import com.ssm.psych.dao.test.TblTesterDao;
import com.ssm.psych.domain.TesterVo;
import com.ssm.psych.domain.ViewTestResultFpa;
import com.ssm.psych.service.fpa.ViewTestResultFpaService;
import com.ssm.psych.service.test.TblTestResultFpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-16 09:59
 * @Version 1.0
 */
@Service
public class TblTestResultFpaServiceImpl implements TblTestResultFpaService {
    @Autowired
    private TblTesterDao tblTesterDao;
    @Autowired
    private ViewTestResultFpaDao viewTestResultFpaDao;
    @Autowired
    private TblTestResultDao tblTestResultDao;
    AjaxResult ajaxResult = new AjaxResult();
    @Override
    public int insertResultFpa(List<Map<String, Object>> answers, Integer testerId) {
        int i = tblTestResultDao.insertResultFpa(answers, testerId);
        return i;
    }

    @Override
    public AjaxResult saveResultFpa(List<Map<String, Object>> answers,Integer testerId) {
        int i = this.insertResultFpa(answers, testerId);
        if (i>0){
            String result = this.getResult(testerId);
            ajaxResult.setMessage(result);
            return ajaxResult;
        }else {
            return null;
        }
    }

    @Override
    public String getResult(Integer testerId) {
        Integer redCount=0;
        Integer blueCount=0;
        Integer greenCount=0;
        Integer yellowCount=0;
        for (ViewTestResultFpa viewTestResultFpa : viewTestResultFpaDao.queryID(testerId)) {
            redCount = viewTestResultFpa.getRedCount();
            blueCount = viewTestResultFpa.getBlueCount();
            greenCount = viewTestResultFpa.getGreenCount();
            yellowCount = viewTestResultFpa.getYellowCount();
        }
        if(redCount>blueCount&&redCount>greenCount&&redCount>yellowCount){
            return "/test/red.jsp";
        }else if(yellowCount>redCount&&yellowCount>greenCount&&yellowCount>blueCount){
            return "/test/yellow.jsp";
        }else if(greenCount>redCount&&greenCount>yellowCount&&greenCount>blueCount) {
            return "/test/green.jsp";
        }else if(blueCount>redCount&&blueCount>yellowCount&&blueCount>greenCount){
            return "/test/blue.jsp";
        }else {
            return "/test/all.jsp";
        }
    }

    @Override
    public AjaxResult queryPhone(TesterVo testerVo) {
        List<TesterVo> testerVos = tblTesterDao.queryPhoneFpa(testerVo);
        if(testerVos.size()>0){
            ajaxResult.setMessage("此电话号已经做过测试！");
            ajaxResult.setStatus(false);
            return ajaxResult;
        }else{
            ajaxResult.setStatus(true);
            return ajaxResult;
        }
    }

}
