package com.ssm.psych.service.test.Impl;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.sas.ViewTestResultSasDao;
import com.ssm.psych.dao.test.TblTestResultDao;
import com.ssm.psych.dao.test.TblTesterDao;
import com.ssm.psych.domain.TesterVo;
import com.ssm.psych.domain.ViewTestResultSas;
import com.ssm.psych.service.test.TblTestResultSasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-16 10:53
 * @Version 1.0
 */
@Service
public class TblTestResultSasServiceImpl implements TblTestResultSasService {
    @Autowired
    private TblTesterDao tblTesterDao;
    @Autowired
    private ViewTestResultSasDao viewTestResultSasDao;
    @Autowired
    private TblTestResultDao tblTestResultDao;
    AjaxResult ajaxResult=new AjaxResult();
    @Override
    public int insertResultSas(List<Map<String, Object>> answers, Integer testerId) {
        int i = tblTestResultDao.insertResultSas(answers, testerId);
        return i;
    }

    @Override
    public AjaxResult saveResultSas(List<Map<String, Object>> answers, Integer testerId) {
        int i = this.insertResultSas(answers, testerId);
        if(i>0){
            String result = this.getResult(testerId);
            ajaxResult.setMessage(result);
            return ajaxResult;
        }else {
            return null;
        }
    }

    @Override
    public String getResult(Integer testerId) {
        Integer forward=0;
        Integer inversion=0;
        for (ViewTestResultSas viewTestResultSas : viewTestResultSasDao.queryID(testerId)) {
            forward = viewTestResultSas.getForward();
            inversion = viewTestResultSas.getInversion();
        }
        Integer result=forward+inversion;
        if(result<=50){
            return "/test/SasNormal.jsp";
        } else if (result<60) {
            return "/test/SasMild.jsp";
        } else if (result<70) {
            return "/test/SasModerate.jsp";
        }else {
            return "test/SasSevere.jsp";
        }
    }
    public AjaxResult queryPhone(TesterVo testerVo) {
        List<TesterVo> testerVos = tblTesterDao.queryPhoneSas(testerVo);
        if(testerVos.size()>0){
            ajaxResult.setMessage("此电话号已经做过测试！");
            ajaxResult.setStatus(false);
            return ajaxResult;
        }else{
            ajaxResult.setStatus(true);
            return ajaxResult;
        }
    }
}
