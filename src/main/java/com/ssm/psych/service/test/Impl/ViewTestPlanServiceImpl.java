package com.ssm.psych.service.test.Impl;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.test.ViewTestPlanDao;
import com.ssm.psych.domain.TesterVo;
import com.ssm.psych.domain.ViewTestPlan;
import com.ssm.psych.service.test.TblTestResultFpaService;
import com.ssm.psych.service.test.TblTestResultSasService;
import com.ssm.psych.service.test.TblTestResultSdsService;
import com.ssm.psych.service.test.ViewTestPlanService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-13 19:30
 * @Version 1.0
 */
@Service
public class ViewTestPlanServiceImpl implements ViewTestPlanService {
    private final ViewTestPlanDao viewTestPlanDao;
    private final TblTestResultFpaService testResultFpaService;
    private final TblTestResultSasService testResultSasService;
    private final TblTestResultSdsService testResultSdsService;

    public ViewTestPlanServiceImpl(ViewTestPlanDao viewTestPlanDao,
                                   TblTestResultFpaService testResultFpaService,
                                   TblTestResultSasService testResultSasService,
                                   TblTestResultSdsService testResultSdsService) {
        this.viewTestPlanDao = viewTestPlanDao;
        this.testResultFpaService = testResultFpaService;
        this.testResultSasService = testResultSasService;
        this.testResultSdsService = testResultSdsService;
    }

    @Override
    public AjaxResult queryAll(Integer testCode, TesterVo testerVo) {
        AjaxResult ajaxResult = new AjaxResult();
        Date testBegin = null;
        Date testEnd = null;
        String type = null;
        for (ViewTestPlan viewTestPlan : viewTestPlanDao.findAll(testCode)) {
            testBegin = viewTestPlan.getTestBegin();
            testEnd = viewTestPlan.getTestEnd();
            type = viewTestPlan.getType();
        }
        Date now = new Date();
        if (testBegin == null) {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("验证码不存在");
            return ajaxResult;
        } else {
            if (now.after(testBegin) && now.before(testEnd)) {
                if (type.equals("fpa")) {
                    return testResultFpaService.queryPhone(testerVo);
                } else if (type.equals("sas")) {
                    return testResultSasService.queryPhone(testerVo);
                } else if (type.equals("sds")) {
                    return testResultSdsService.queryPhone(testerVo);
                } else {
                    return testResultSdsService.queryPhone(testerVo);
                }
            } else {
                ajaxResult.setStatus(false);
                ajaxResult.setMessage("验证码超期");
                return ajaxResult;
            }
        }
    }

    @Override
    public List<ViewTestPlan> findAll(Integer testCode) {
        return viewTestPlanDao.findAll(testCode);
    }
}