package com.ssm.psych.service.test;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.TesterVo;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author ：
 * @date ：Created in 2022/12/16 9:58
 * @description：
 * @modified By：
 * @version:
 */

public interface TblTestResultFpaService {
    int insertResultFpa(List<Map<String, Object>> answers,Integer testerId);
    AjaxResult saveResultFpa(List<Map<String, Object>> answers, Integer testerId);
    String getResult(Integer testerId);

    AjaxResult queryPhone(TesterVo testerVo);
}
