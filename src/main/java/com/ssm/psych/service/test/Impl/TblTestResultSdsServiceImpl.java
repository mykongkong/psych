package com.ssm.psych.service.test.Impl;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.sds.ViewTestResultSdsDao;
import com.ssm.psych.dao.test.TblTestResultDao;
import com.ssm.psych.dao.test.TblTesterDao;
import com.ssm.psych.domain.TesterVo;
import com.ssm.psych.domain.ViewTestResultSds;
import com.ssm.psych.service.test.TblTestResultSdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-16 14:35
 * @Version 1.0
 */
@Service
public class TblTestResultSdsServiceImpl implements TblTestResultSdsService {
    @Autowired
    private TblTesterDao tblTesterDao;
    @Autowired
    private TblTestResultDao tblTestResultDao;
    @Autowired
    private ViewTestResultSdsDao viewTestResultSdsDao;
    AjaxResult ajaxResult = new AjaxResult();

    @Override
    public int insertResultSds(List<Map<String, Object>> answers, Integer testerId) {
        return tblTestResultDao.insertResultSds(answers, testerId);
    }

    @Override
    public AjaxResult saveResultSds(List<Map<String, Object>> answers, Integer testerId) {
        int i = this.insertResultSds(answers, testerId);
        if (i > 0) {
            String result = this.getResult(testerId);
            ajaxResult.setMessage(result);
            return ajaxResult;
        } else {
            return null;
        }
    }

    @Override
    public String getResult(Integer testerId) {
        Integer forward = 0;
        Integer inversion = 0;
        for (ViewTestResultSds viewTestResultSds : viewTestResultSdsDao.queryID(testerId)) {
            forward = viewTestResultSds.getForward();
            inversion = viewTestResultSds.getInversion();
        }
        Integer result = forward + inversion;
        if (result <= 50) {
            return "/test/SdsNormal.jsp";
        } else if (result < 60) {
            return "/test/SdsMild.jsp";
        } else if (result < 70) {
            return "/test/SdsModerate.jsp";
        } else {
            return "test/SdsSevere.jsp";
        }
    }

    public AjaxResult queryPhone(TesterVo testerVo) {
        List<TesterVo> testerVos = tblTesterDao.queryPhoneSds(testerVo);
        if (testerVos.size() > 0) {
            ajaxResult.setMessage("此电话号已经做过测试！");
            ajaxResult.setStatus(false);
            return ajaxResult;
        } else {
            ajaxResult.setStatus(true);
            return ajaxResult;
        }
    }
}