package com.ssm.psych.service.test;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.TesterVo;

import java.util.List;
import java.util.Map;

/**
 * @author ：dpj
 * @Date ：Created in 2022/12/16 14:34
 * @Description
 */

public interface TblTestResultSdsService {
    int insertResultSds(List<Map<String, Object>> answers, Integer testerId);

    AjaxResult saveResultSds(List<Map<String, Object>> answers, Integer testerId);

    String getResult(Integer testerId);

    AjaxResult queryPhone(TesterVo testerVo);
}