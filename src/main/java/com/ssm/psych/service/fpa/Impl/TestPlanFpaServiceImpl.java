package com.ssm.psych.service.fpa.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.fpa.TestPlanFpaDao;
import com.ssm.psych.domain.TestPlanFpa;
import com.ssm.psych.service.fpa.TestPlanFpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

/**
 * @Description 性格测试-测试计划-服务层
 * @Author 情雅圣
 * @Date 2022-12-01 16:06
 * @Version 1.0
 */
@Service
public class TestPlanFpaServiceImpl implements TestPlanFpaService {
    @Autowired
    private TestPlanFpaDao testPlanFpaDao;

    /**
     * 调用DAO层的queryAll的方法查询所有的数据
     * @param page 分页的页码
     * @param limit 分页每页的数据数
     * @return 返回查询到的所有数据
     */
    @Override
    public PageInfo<TestPlanFpa> queryAll(Integer page,Integer limit) {
        PageHelper.startPage(page,limit);
        List<TestPlanFpa> planFpas = testPlanFpaDao.queryAll();
        return new PageInfo<>(planFpas);
    }

    /**
     * 调用DAO层的insert的方法添加数据到数据库
     * @param testPlanFpa　传入想要添加的数据
     * @return 返回添加结果的数据
     */
    @Override
    public AjaxResult insert(TestPlanFpa testPlanFpa) {
        AjaxResult ajaxResult = new AjaxResult();
        int insert = testPlanFpaDao.insert(testPlanFpa);
        if(insert>0){
            ajaxResult.setStatus(true);
            ajaxResult.setMessage("添加成功");
        }else {
            ajaxResult.setMessage("添加失败");
            ajaxResult.setStatus(false);
        }
        return ajaxResult;
    }

    /**
     * 获取一个六位的随机数
     * @return 返回得到的六位数
     */
    @Override
    public int getRandom() {
        int i=new Random().nextInt(10);
        int j=new Random().nextInt(10)*10;
        int k=new Random().nextInt(10)*100;
        int l=new Random().nextInt(10)*1000;
        int m=new Random().nextInt(10)*10000;
        int n=new Random().nextInt(10)*100000;
        return n+m+l+k+j+i;
    }

    /**
     * 调用DAO层的update的方法修改数据
     * @param testPlanFpa 传入需要修改的数据
     * @return 返回修改结果的数据
     */
    @Override
    public AjaxResult update(TestPlanFpa testPlanFpa) {
        AjaxResult ajaxResult = new AjaxResult();
        int update = testPlanFpaDao.update(testPlanFpa);
        if(update>0){
            ajaxResult.setStatus(true);
            ajaxResult.setMessage("修改成功");
            return ajaxResult;
        }else {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("修改失败");
            return ajaxResult;
        }
    }

    /**
     * 调用DAO层的deleteByIds的方法删除指定Id的数据
     * @param idArray 传入需要删除数据的ID
     * @return 返回删除结果的数据
     */
    @Override
    public AjaxResult deleteByIds(Integer[] idArray) {
        AjaxResult ajaxResult=new AjaxResult();
        int ids = testPlanFpaDao.deleteByIds(idArray);
        if(ids>0){
            ajaxResult.setStatus(true);
            ajaxResult.setMessage("修改成功");
            return ajaxResult;
        }else {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("修改失败");
            return ajaxResult;
        }
    }

    /**
     * 调用DAO层的fuzzyQuery的方法对数据库进行模糊查询
     * @param testPlanFpa 传入需要查询的数据
     * @return 返回查询得到的结果
     */
    @Override
    public PageInfo<TestPlanFpa> fuzzyQuery(TestPlanFpa testPlanFpa) {
        List<TestPlanFpa> testPlanFpas = testPlanFpaDao.fuzzyQuery(testPlanFpa);
        PageInfo<TestPlanFpa> pageInfo = new PageInfo<>(testPlanFpas);
        return pageInfo;
    }

}
