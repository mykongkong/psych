package com.ssm.psych.service.fpa;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.TestPlanFpa;

import java.util.List;

/**
 * @author ：
 * @date ：Created in 2022/12/1 16:05
 * @description：
 * @modified By：
 * @version:
 */

public interface TestPlanFpaService {
    //查询所有数据
    PageInfo<TestPlanFpa> queryAll(Integer page,Integer limit);
    //添加数据
    AjaxResult insert(TestPlanFpa testPlanFpa);
    //获取测试邀请码
    int getRandom();
    //修改数据
    AjaxResult update(TestPlanFpa testPlanFpa);
    AjaxResult deleteByIds(Integer[] idArray);
    //模糊查询
    PageInfo<TestPlanFpa> fuzzyQuery(TestPlanFpa testPlanFpa);
}
