package com.ssm.psych.service.fpa.Impl;

import com.github.pagehelper.PageHelper;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.fpa.QuestionFpaDao;
import com.ssm.psych.dao.test.TblTesterDao;
import com.ssm.psych.domain.QuestionFpa;
import com.ssm.psych.domain.TesterVo;
import com.ssm.psych.service.fpa.QuestionFpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description 1.性格测试-题目管理-服务层
 * @Author dpj
 * @Date 2022-11-26 10:17
 * @Version 1.0
 */
@Service
public class QuestionFpaServiceImpl implements QuestionFpaService {
    @Autowired
    private TblTesterDao tblTesterDao;
    @Autowired
    private QuestionFpaDao questionFpaDao;

    @Autowired
    public void setQuestionFpaDao(QuestionFpaDao questionFpaDao) {
        this.questionFpaDao = questionFpaDao;
    }

    /**
     * 查询数据库中的所有数据
     * @return 返回查到的数据
     */
    @Override
    public List<QuestionFpa> queryAll() {
        return questionFpaDao.findAll();
    }

    /**
     * 调用DAO层的findALL方法查询所有的数据
     * @param page 分页的页码
     * @param limit 分页的每页的数据数
     * @return 返回实体类的集合
     */
    @Override
    public List<QuestionFpa> findAll(Integer page,Integer limit) {
        PageHelper.startPage(page,limit);
        return questionFpaDao.findAll();
    }

    /**
     * 调用DAO层的add方法添加指定的数据
     * @param questionFpa 传入添加的数据
     * @return 返回添加的结果数据
     */
    @Override
    public AjaxResult add(QuestionFpa questionFpa) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setStatus(true);
        questionFpaDao.add(questionFpa);
        return ajaxResult;
    }

    /**
     * 调用DAO层的deleteId的方法删除指定ID的数据
     * @param id 传入指定的ID
     * @return 返回影响的行数
     */
    @Override
    public int deleteId(int id){
        return questionFpaDao.deleteId(id);
    }

    /**
     * 调用DAO层的update的方法修改指定的数据
     * @param questionFpa 传入修改的数据
     * @return 返回修改的结果数据
     */
    @Override
    public AjaxResult update(QuestionFpa questionFpa){
        AjaxResult ajaxResult = new AjaxResult();
        int update = questionFpaDao.update(questionFpa);
        if(update>0){
            ajaxResult.setStatus(true);
            ajaxResult.setMessage("修改成功");
        }else {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("修改失败");
        }
        return ajaxResult;
    }

    /**
     * 调用DAO层的fuzzyQuery的方法对数据库进行模糊查询
     * @param questionFpa 传入需要查询的数据
     * @return 返回查询到的数据
     */
    @Override
    public List<QuestionFpa> fuzzyQuery(QuestionFpa questionFpa){
        return questionFpaDao.fuzzyQuery(questionFpa);
    }

    /**
     * 调用DAO层的deleteByIds的方法删除批量的数据
     * @param idList 传入整形的数组
     */
    @Override
    public void deleteByIds(Integer[] idList) {
        questionFpaDao.deleteByIds(idList);
    }

    @Override
    public void insertFpa(TesterVo tester) {
        tblTesterDao.insertFpa(tester);
    }
}