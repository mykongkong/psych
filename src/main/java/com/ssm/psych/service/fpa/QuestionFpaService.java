package com.ssm.psych.service.fpa;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.QuestionFpa;
import com.ssm.psych.domain.TesterVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface QuestionFpaService {
    List<QuestionFpa> queryAll();
    //查询数据库数据
    List<QuestionFpa> findAll(Integer page,Integer limit);
    //添加数据到数据库
    AjaxResult add(QuestionFpa questionFpa);
    //删除指定id的数据
    int deleteId(int id);
    //修改指定的数据
    AjaxResult update(QuestionFpa questionFpa);
    //模糊查询数据
    List<QuestionFpa> fuzzyQuery(QuestionFpa questionFpa);
    //批量删除
    void deleteByIds(@Param("idList") Integer[] idList);
    void insertFpa(TesterVo tester);
}