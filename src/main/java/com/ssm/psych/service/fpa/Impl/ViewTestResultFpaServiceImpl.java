package com.ssm.psych.service.fpa.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.fpa.ViewTestResultFpaDao;
import com.ssm.psych.domain.ResultCountFpa;
import com.ssm.psych.domain.TblResultDiv;
import com.ssm.psych.domain.ViewTestResultFpa;
import com.ssm.psych.service.fpa.ViewTestResultFpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @Description 性格测试-测试结果-服务层
 * @Author 情雅圣
 * @Date 2022-12-05 15:40
 * @Version 1.0
 */
@Service
public class ViewTestResultFpaServiceImpl implements ViewTestResultFpaService {
    @Autowired
    private ViewTestResultFpaDao viewTestResultFpaDao;

    /**
     * 调用DAO层的QueryAll的方法查询所有的数据
     * @param page 分页的页码
     * @param limit 分页的每页的数据数
     * @return 返回查询得到的结果
     */
    @Override
    public PageInfo<ViewTestResultFpa> queryAll(Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        List<ViewTestResultFpa> fpas = viewTestResultFpaDao.queryAll();
        return new PageInfo<>(fpas);
    }

    /**
     * 调用DAO层的delete的方法删除指定的数据
     * @param idList　传入指定的ID
     * @return 返回删除结果的数据
     */
    @Override
    public AjaxResult delete(Integer[] idList) {
        AjaxResult ajaxResult=new AjaxResult();
        int delete = viewTestResultFpaDao.delete(idList);
        if(delete>0){
            ajaxResult.setStatus(true);
            ajaxResult.setMessage("删除成功");
            return ajaxResult;
        }else {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("删除失败");
            return ajaxResult;
        }
    }

    /**
     * 调用DAO层的fuzzyQuery的方法模糊查询
     * @param viewTestResultFpa 传入需要查询的数据
     * @return 返回查询得到的数据
     */
    @Override
    public PageInfo<ViewTestResultFpa> fuzzyQuery(ViewTestResultFpa viewTestResultFpa) {
        List<ViewTestResultFpa> list = viewTestResultFpaDao.fuzzyQuery(viewTestResultFpa);
        return new PageInfo<>(list);
    }

    /**
     * 调用DAO层的queryID的方法查询指定ID的数据
     * @param testerId 传入指定的ID
     * @return 返回查询得到的数据
     */
    @Override
    public PageInfo<ViewTestResultFpa> queryID(Integer testerId) {
        List<ViewTestResultFpa> viewTestResultFpas = viewTestResultFpaDao.queryID(testerId);
        return new PageInfo<>(viewTestResultFpas);
    }

    /**
     * 调用DAO层的queryDiv的方法得到查询结果的数据
     * @param id　传入指定的ID
     * @return 返回查询到的数据
     */
    @Override
    public PageInfo<TblResultDiv> queryDiv(Integer id) {
        List<TblResultDiv> div = viewTestResultFpaDao.queryDiv(id);
        return new PageInfo<>(div);
    }

    /**
     * 获取报表统计的数据
     * @return 返回数据的对象
     */
    @Override
    public ResultCountFpa getResult() {
        int redNumber = 0;
        int blueNumber = 0;
        int greenNumber = 0;
        int yellowNumber = 0;
        int reNumber=0;
        for (ViewTestResultFpa resultFpa : viewTestResultFpaDao.queryAll()) {
            int red = resultFpa.getRedCount();
            int green = resultFpa.getGreenCount();
            int blue = resultFpa.getBlueCount();
            int yellow = resultFpa.getYellowCount();
            Integer[] arr = {red, green, blue, yellow};
            Arrays.sort(arr);
            int x=0;
            int max=arr[arr.length-1];
            if(max==red){
                if(x<1){
                    redNumber++;
                }
                x++;
            }
            if(max==green){
                if(x<1){
                    greenNumber++;
                }else if(x==1){
                    redNumber--;
                }
                x++;
            }
            if(max==blue){
                if(x<1){
                    blueNumber++;
                }else if(x==1){
                    greenNumber--;
                }
                x++;
            }
            if(max==yellow){
                if(x<1){
                    yellowNumber++;
                }else if(x==1){
                    blueNumber--;
                }
                x++;
            }
            if(x>1){
                reNumber++;
            }
        }
        ResultCountFpa resultCount=new ResultCountFpa(redNumber,blueNumber,yellowNumber,greenNumber,reNumber);
        return resultCount;
    }

}
