package com.ssm.psych.service.fpa;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.ResultCountFpa;
import com.ssm.psych.domain.TblResultDiv;
import com.ssm.psych.domain.ViewTestResultFpa;
import org.springframework.stereotype.Service;

/**
 * @author ：
 * @date ：Created in 2022/12/5 15:38
 * @description：
 * @modified By：
 * @version:
 */
@Service
public interface ViewTestResultFpaService {
    //查询数据
    PageInfo<ViewTestResultFpa> queryAll(Integer page,Integer limit);
    //删除数据
    AjaxResult delete(Integer[] idList);
    //模糊查询
    PageInfo<ViewTestResultFpa> fuzzyQuery(ViewTestResultFpa viewTestResultFpa);
    //通过ID查数据
    PageInfo<ViewTestResultFpa> queryID(Integer testerId);
    //查询Div数据
    PageInfo<TblResultDiv> queryDiv(Integer id);
    ResultCountFpa getResult();
}
