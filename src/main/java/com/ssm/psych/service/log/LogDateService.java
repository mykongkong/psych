package com.ssm.psych.service.log;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.domain.Log;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author ：
 * @date ：Created in 2022/12/9 16:54
 * @description：
 * @modified By：
 * @version:
 */

public interface LogDateService {
    PageInfo<Log> findAll(Integer page,Integer limit);
    void insert(Log log);
}
