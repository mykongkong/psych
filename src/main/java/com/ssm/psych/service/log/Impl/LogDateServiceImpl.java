package com.ssm.psych.service.log.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.psych.dao.log.LogDateDao;
import com.ssm.psych.domain.Log;
import com.ssm.psych.service.log.LogDateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-09 16:55
 * @Version 1.0
 */
@Service
public class LogDateServiceImpl implements LogDateService {
    @Autowired
    private LogDateDao logDateDao;

    /**
     * 调用DAO层的findAll的方法查询数据库的所有数据
     * @param page 分页的页码
     * @param limit 分页每页显示的数据数
     * @return 返回查询到的数据
     */
    @Override
    public PageInfo<Log> findAll(Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        List<Log> all = logDateDao.findAll();
        return new PageInfo<>(all);
    }

    /**
     * 调用DAO层的insert的方法向数据库添加指定的数据
     * @param log 传入需要添加的数据
     */
    @Override
    public void insert(Log log) {
        logDateDao.insert(log);
    }
}
