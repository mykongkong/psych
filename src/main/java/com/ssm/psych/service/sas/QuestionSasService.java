package com.ssm.psych.service.sas;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.QuestionSas;
import com.ssm.psych.domain.TesterVo;

import java.util.List;

public interface QuestionSasService {
    List<QuestionSas> findAll();
    QuestionSas queryById(Integer id);
    //查询所有数据
    PageInfo<QuestionSas> queryAll(Integer pageNum, Integer pageSize);
    //修改数据
    AjaxResult update(QuestionSas questionSas);
    //添加数据
    AjaxResult insert(QuestionSas questionSas);
    //模糊查询
    PageInfo<QuestionSas> fuzzyQuery(QuestionSas questionSas);
    //删除数据
    AjaxResult delete(Integer[] idList);
    void insertSas(TesterVo tester);
}