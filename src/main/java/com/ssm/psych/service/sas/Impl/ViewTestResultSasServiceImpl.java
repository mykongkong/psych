package com.ssm.psych.service.sas.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.sas.ViewTestResultSasDao;
import com.ssm.psych.domain.ResultCountSas;
import com.ssm.psych.domain.ViewTestResultSas;
import com.ssm.psych.service.fpa.ViewTestResultFpaService;
import com.ssm.psych.service.sas.ViewTestResultSasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description 焦虑测试-测试结果-服务层
 * @Author 情雅圣
 * @Date 2022-12-07 19:41
 * @Version 1.0
 */
@Service
public class ViewTestResultSasServiceImpl implements ViewTestResultSasService {
    @Autowired
    private ViewTestResultSasDao viewTestResultSasDao;

    /**
     * 调用DAO层的queryAll的方法查询数据的所有数据
     * @param page 分页的页码
     * @param limit 分页的每页的数据数
     * @return 返回查询到的结果
     */
    @Override
    public PageInfo<ViewTestResultSas> queryAll(Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        List<ViewTestResultSas> sas = viewTestResultSasDao.queryAll();
        return new PageInfo<>(sas);
    }

    /**
     * 调用DAO层的queryID的方法查询指定ID的数据
     * @param id 传入需要查询的ID
     * @return 返回查询到的结果
     */
    @Override
    public PageInfo<ViewTestResultSas> queryID(Integer id) {
        List<ViewTestResultSas> sas = viewTestResultSasDao.queryID(id);
        return new PageInfo<>(sas);
    }

    /**
     * 调用DAO层的delete的方法删除数据库指定的数据
     * @param idList　传入需要删除的ID
     * @return 返回删除的结果数据
     */
    @Override
    public AjaxResult delete(Integer[] idList) {
        int delete = viewTestResultSasDao.delete(idList);
        AjaxResult result=new AjaxResult();
        if(delete>0){
            result.setStatus(true);
            result.setMessage("删除成功");
            return result;
        }else{
            result.setStatus(false);
            result.setMessage("删除失败");
            return result;
        }
    }

    /**
     * 调用DAO层的fuzzyQuery的方法对数据库进行模糊查询
     * @param viewTestResultSas　传入需要查询的数据
     * @return 返回查询到的结果
     */
    @Override
    public PageInfo<ViewTestResultSas> fuzzyQuery(ViewTestResultSas viewTestResultSas) {
        List<ViewTestResultSas> list = viewTestResultSasDao.fuzzyQuery(viewTestResultSas);
        return new PageInfo<>(list);
    }

    /**
     * 获取不同焦虑程度的统计个数
     * @return 将得到的统计个数给返回
     */
    @Override
    public ResultCountSas getCount() {
        int normal=0;
        int mild=0;
        int middle=0;
        int severe=0;
        for (ViewTestResultSas testResultSas : viewTestResultSasDao.queryAll()) {
            int f=testResultSas.getForward();
            int ins=testResultSas.getInversion();
            int score=f+ins;
            if(score<40){
                normal++;
            }else if (score<=46){
                mild++;
            }else if (score<=55){
                middle++;
            }else if (score>55){
                severe++;
            }
        }
        ResultCountSas resultCountSas=new ResultCountSas(normal,mild,middle,severe);
        return resultCountSas;
    }
}
