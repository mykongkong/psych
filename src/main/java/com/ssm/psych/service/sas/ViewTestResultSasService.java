package com.ssm.psych.service.sas;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.ResultCountSas;
import com.ssm.psych.domain.ViewTestResultSas;

/**
 * @author ：
 * @date ：Created in 2022/12/7 19:40
 * @description：
 * @modified By：
 * @version:
 */

public interface ViewTestResultSasService {
    //查询所有的数据
    PageInfo<ViewTestResultSas> queryAll(Integer page,Integer limit);
    //通过ID查询数据
    PageInfo<ViewTestResultSas> queryID(Integer id);
    //删除数据
    AjaxResult delete(Integer[] idList);
    //模糊查询
    PageInfo<ViewTestResultSas> fuzzyQuery(ViewTestResultSas viewTestResultSas);
    ResultCountSas getCount();
}
