package com.ssm.psych.service.sas.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.sas.QuestionSasDao;
import com.ssm.psych.dao.test.TblTesterDao;
import com.ssm.psych.domain.QuestionFpa;
import com.ssm.psych.domain.QuestionSas;
import com.ssm.psych.domain.TestPlanFpa;
import com.ssm.psych.domain.TesterVo;
import com.ssm.psych.service.sas.QuestionSasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description 3.焦虑测试-题目管理-服务层
 * @Author dpj
 * @Date 2022-11-26 10:22
 * @Version 1.0
 */
@Service
public class QuestionSasServiceImpl implements QuestionSasService {
    @Autowired
    private TblTesterDao tblTesterDao;
    private QuestionSasDao questionSasDao;

    @Autowired
    public void setQuestionSasDao(QuestionSasDao questionSasDao) {
        this.questionSasDao = questionSasDao;
    }

    @Override
    public List<QuestionSas> findAll() {
        return questionSasDao.queryAll();
    }

    @Override
    public QuestionSas queryById(Integer id) {
        return questionSasDao.queryById(id);
    }

    @Override
    public PageInfo<QuestionSas> queryAll(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        return new PageInfo<>(questionSasDao.queryAll());
    }
    @Override
    public AjaxResult update(QuestionSas questionSas){
        AjaxResult ajaxResult=new AjaxResult();
        int update = questionSasDao.update(questionSas);
        if (update>0){
            ajaxResult.setMessage("修改成功");
            ajaxResult.setStatus(true);
            return ajaxResult;
        }else {
            ajaxResult.setMessage("修改失败");
            ajaxResult.setStatus(false);
            return ajaxResult;
        }
    }

    @Override
    public AjaxResult insert(QuestionSas questionSas) {
        AjaxResult ajaxResult=new AjaxResult();
        int insert = questionSasDao.insert(questionSas);
        if(insert>0){
            ajaxResult.setMessage("添加成功");
            ajaxResult.setStatus(true);
            return ajaxResult;
        }else {
            ajaxResult.setMessage("添加失败");
            ajaxResult.setStatus(false);
            return ajaxResult;
        }
    }

    @Override
    public PageInfo<QuestionSas> fuzzyQuery(QuestionSas questionSas) {
        List<QuestionSas> fuzzyQuery= questionSasDao.fuzzyQuery(questionSas);
        PageInfo<QuestionSas> pageInfo = new PageInfo<>(fuzzyQuery);
        return pageInfo;
    }

    @Override
    public AjaxResult delete(Integer[] idList) {
        AjaxResult ajaxResult=new AjaxResult();
        int delete = questionSasDao.delete(idList);
        if(delete>0){
            ajaxResult.setMessage("删除成功");
            ajaxResult.setStatus(true);
            return ajaxResult;
        }else{
            ajaxResult.setMessage("删除失败");
            ajaxResult.setStatus(false);
            return ajaxResult;
        }
    }

    @Override
    public void insertSas(TesterVo tester) {
        tblTesterDao.insertSas(tester);
    }

}