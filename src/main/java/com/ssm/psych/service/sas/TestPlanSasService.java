package com.ssm.psych.service.sas;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.TestPlanSas;

/**
 * @author ：
 * @date ：Created in 2022/12/5 10:40
 * @description：
 * @modified By：
 * @version:
 */

public interface TestPlanSasService {
    //查询数据
    PageInfo<TestPlanSas> queryAll(Integer page,Integer limit);
    //修改数据
    AjaxResult update(TestPlanSas testPlanSas);
    //添加数据
    AjaxResult insert(TestPlanSas testPlanSas);
    //删除数据
    AjaxResult delete(Integer[] idList);
    //模糊查询
    PageInfo<TestPlanSas> fuzzyQuery(String testName,String createBy);
}
