package com.ssm.psych.service.sys;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.Dept;
import com.ssm.psych.domain.Role;
import com.ssm.psych.domain.User;

import java.util.List;

public interface UserService {
    User queryById(Integer userid);

    User queryByName(String username);

    User queryByAccount(String account);

    AjaxResult hasAccount(String account, Integer accountType);

    AjaxResult accountDuplicate(String account, Integer accountType, Integer userid);

    PageInfo<User> queryAll(Integer pageNum, Integer pageSize);

    PageInfo<User> fuzzyQuery(Integer userid, String username, Integer pageNum, Integer pageSize);

    List<Dept> queryDeptList();

    List<Role> queryRoleList();

    AjaxResult insert(User user);

    AjaxResult update(User user);

    AjaxResult delete(Integer userid);

    AjaxResult deleteByIds(Integer[] userIds);

    User signin(String account, String password);

    AjaxResult resetPwd(String username, String pwdSignin);

    AjaxResult modifyPwd(String account, String oldPwd, String newPwd);
}