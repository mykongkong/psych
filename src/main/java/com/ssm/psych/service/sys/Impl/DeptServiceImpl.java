package com.ssm.psych.service.sys.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.sys.DeptDao;
import com.ssm.psych.domain.Dept;
import com.ssm.psych.service.sys.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @Description 4.系统管理-部门管理-服务层
 * @Author dpj
 * @Date 2022-11-25 20:35
 * @Version 1.0
 */
@Service
public class DeptServiceImpl implements DeptService {
    private DeptDao deptDao;

    @Autowired
    public void setDeptDao(DeptDao deptDao) {
        this.deptDao = deptDao;
    }

    @Override
    public Dept queryById(Integer deptId) {
        return deptDao.queryById(deptId);
    }

    @Override
    public PageInfo<Dept> queryAll(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return new PageInfo<>(deptDao.queryAll());
    }

    @Override
    public PageInfo<Dept> fuzzyQuery(String deptName, String createBy, Integer pageNum,
                                     Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return new PageInfo<>(deptDao.fuzzyQuery(deptName, createBy));
    }

    @Override
    public AjaxResult insert(Dept dept) {
        AjaxResult result = new AjaxResult();
        // 1.如果部门不存在，直接添加
        if (deptDao.queryByName(dept.getDeptName()) == null) {
            deptDao.insert(dept);
            result.setStatus(true);
            return result;
        }
        // 2.如果部门已存在，返回提示信息
        result.setStatus(false);
        result.setMessage(dept.getDeptName() + "已存在！");
        return result;
    }

    @Override
    public AjaxResult update(Dept dept) {
        AjaxResult result = new AjaxResult();
        // 1.如果修改后的部门和其他部门不重名，直接修改
        if (deptDao.queryByName(dept.getDeptName()) == null
                || Objects.equals(deptDao.queryById(dept.getDeptId()).getDeptName(),
                                  dept.getDeptName())) {
            try {
                deptDao.update(dept);
                result.setStatus(true);
            } catch (Exception e) {
                e.printStackTrace();
                result.setStatus(false);
                result.setMessage("修改失败!");
            }
        } else {
            // 2.如果修改后的部门和其他部门重名（不包括自身），返回提示信息
            result.setStatus(false);
            result.setMessage(dept.getDeptName() + "已存在！");
        }
        return result;
    }

    @Override
    public AjaxResult delete(Integer deptId) {
        AjaxResult result = new AjaxResult();
        deptDao.delete(deptId);
        result.setStatus(true);
        return result;
    }

    @Override
    public AjaxResult deleteByIds(Integer[] deptIds) {
        AjaxResult result = new AjaxResult();
        deptDao.deleteByIds(deptIds);
        result.setStatus(true);
        return result;
    }

}