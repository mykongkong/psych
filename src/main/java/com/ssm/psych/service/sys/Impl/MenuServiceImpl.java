package com.ssm.psych.service.sys.Impl;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.sys.MenuDao;
import com.ssm.psych.domain.Menu;
import com.ssm.psych.service.sys.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description 4.系统管理-菜单管理-服务层
 * @Author dpj
 * @Date 2022-12-07 09:45
 * @Version 1.0
 */
@Service
public class MenuServiceImpl implements MenuService {
    private MenuDao menuDao;

    @Autowired
    public void setMenuDao(MenuDao menuDao) {
        this.menuDao = menuDao;
    }

    @Override
    public Menu queryById(Integer menuId) {
        return menuDao.queryById(menuId);
    }

    @Override
    public Menu queryByName(String menuName) {
        return menuDao.queryByName(menuName);
    }

    @Override
    public List<Menu> queryAll() {
        return menuDao.queryAll();
    }

    @Override
    public List<Menu> queryListByParentId(Integer parentId) {
        return menuDao.queryListByParentId(parentId);
    }

    @Override
    public AjaxResult insert(Menu menu) {
        AjaxResult result = new AjaxResult();
        // 1.如果菜单不存在，直接添加
        if (menuDao.queryByName(menu.getMenuName()) == null) {
            menuDao.insert(menu);
            result.setStatus(true);
            return result;
        }
        // 2.如果菜单已存在，返回提示信息
        result.setStatus(false);
        result.setMessage(menu.getMenuName() + "已存在！");
        return result;
    }
}