package com.ssm.psych.service.sys;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.Dept;

public interface DeptService {
    Dept queryById(Integer deptId);

    PageInfo<Dept> queryAll(Integer pageNum, Integer pageSize);

    PageInfo<Dept> fuzzyQuery(String deptName, String createBy, Integer pageNum, Integer pageSize);

    AjaxResult insert(Dept dept);

    AjaxResult update(Dept dept);

    AjaxResult delete(Integer deptId);

    AjaxResult deleteByIds(Integer[] deptIds);
}