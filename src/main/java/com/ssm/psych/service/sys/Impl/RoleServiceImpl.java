package com.ssm.psych.service.sys.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.sys.RoleDao;
import com.ssm.psych.domain.Role;
import com.ssm.psych.service.sys.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @Description 4.系统管理-角色管理-服务层
 * @Author dpj
 * @Date 2022-12-08 08:38
 * @Version 1.0
 */
@Service
public class RoleServiceImpl implements RoleService {
    private RoleDao roleDao;

    @Autowired
    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public Role queryById(Integer roleId) {
        return roleDao.queryById(roleId);
    }

    @Override
    public Role queryByName(String roleName) {
        return roleDao.queryByName(roleName);
    }

    @Override
    public PageInfo<Role> queryAll(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return new PageInfo<>(roleDao.queryAll());
    }

    @Override
    public AjaxResult insert(Role role) {
        AjaxResult result = new AjaxResult();
        // 1.如果角色不存在，直接插入
        if (roleDao.queryByName(role.getRoleName()) == null) {
            roleDao.insert(role);
            result.setStatus(true);
            return result;
        }
        // 2.如果角色已存在，返回提示信息
        result.setStatus(false);
        result.setMessage(role.getRoleName() + "已存在！");
        return result;
    }

    @Override
    public AjaxResult update(Role role) {
        AjaxResult result = new AjaxResult();
        // 1.如果修改后的角色和其他角色不重名，直接修改
        if (roleDao.queryByName(role.getRoleName()) == null ||
                Objects.equals(roleDao.queryById(role.getRoleId()).getRoleName(),
                               role.getRoleName())) {
            try {
                roleDao.update(role);
                result.setStatus(true);
            } catch (Exception e) {
                e.printStackTrace();
                result.setStatus(false);
                result.setMessage("角色修改失败！");
            }
        } else {
            // 2.如果修改后的角色和其他角色重名（不包括自身），返回提示信息
            result.setStatus(false);
            result.setMessage(role.getRoleName() + "已存在！");
        }
        return result;
    }

    @Override
    public AjaxResult delete(Integer roleId) {
        AjaxResult result = new AjaxResult();
        roleDao.delete(roleId);
        result.setStatus(true);
        return result;
    }

    @Override
    public AjaxResult insertRoleMenu(Integer roleId, Integer menuId) {
        AjaxResult result = new AjaxResult();
        roleDao.insertRoleMenu(roleId, menuId);
        result.setStatus(true);
        return result;
    }

    @Override
    public AjaxResult deleteRoleMenu(Integer roleId, Integer menuId) {
        AjaxResult result = new AjaxResult();
        roleDao.deleteRoleMenu(roleId, menuId);
        result.setStatus(true);
        return result;
    }

    @Override
    public List<Integer> querySelectedMenuList(Integer roleId) {
        return roleDao.querySelectedMenuList(roleId);
    }
}