package com.ssm.psych.service.sys;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.Role;

import java.util.List;

public interface RoleService {
    Role queryById(Integer roleId);

    Role queryByName(String roleName);

    PageInfo<Role> queryAll(Integer pageNum, Integer pageSize);

    AjaxResult insert(Role role);

    AjaxResult update(Role role);

    AjaxResult delete(Integer roleId);

    AjaxResult insertRoleMenu(Integer roleId, Integer menuId);

    AjaxResult deleteRoleMenu(Integer roleId, Integer menuId);

    List<Integer> querySelectedMenuList(Integer roleId);
}