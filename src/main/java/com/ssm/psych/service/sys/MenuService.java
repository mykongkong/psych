package com.ssm.psych.service.sys;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.Menu;

import java.util.List;

public interface MenuService {
    Menu queryById(Integer menuId);

    Menu queryByName(String menuName);

    List<Menu> queryAll();

    List<Menu> queryListByParentId(Integer parentId);

    AjaxResult insert(Menu menu);
}