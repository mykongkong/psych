package com.ssm.psych.service.sys.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.sys.UserDao;
import com.ssm.psych.domain.Dept;
import com.ssm.psych.domain.Role;
import com.ssm.psych.domain.User;
import com.ssm.psych.service.sys.UserService;
import com.ssm.psych.utils.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @Description 4.系统管理-用户管理-服务层
 * @Author dpj
 * @Date 2022-12-06 12:13
 * @Version 1.0
 */
@Service
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User queryById(Integer userid) {
        return userDao.queryById(userid);
    }

    @Override
    public User queryByName(String username) {
        return userDao.queryByName(username);
    }

    @Override
    public User queryByAccount(String account) {
        return userDao.queryByAccount(account);
    }

    @Override
    public AjaxResult hasAccount(String account, Integer accountType) {
        AjaxResult result = new AjaxResult();
        switch (accountType) {
            case 0:
                if (userDao.queryByAccount(account) != null) {
                    result.setStatus(false);
                    result.setMessage("用户名已注册！");
                } else {
                    result.setStatus(true);
                    result.setMessage("用户名合法！");
                }
                break;
            case 1:
                if (userDao.queryByAccount(account) != null) {
                    result.setStatus(false);
                    result.setMessage("邮箱已注册！");
                } else {
                    result.setStatus(true);
                    result.setMessage("邮箱合法！");
                }
                break;
            case 2:
                if (userDao.queryByAccount(account) != null) {
                    result.setStatus(false);
                    result.setMessage("手机号已注册！");
                } else {
                    result.setStatus(true);
                    result.setMessage("手机号合法！");
                }
                break;
        }
        return result;
    }

    @Override
    public AjaxResult accountDuplicate(String account, Integer accountType, Integer userid) {
        AjaxResult result = new AjaxResult();
        User userQA = userDao.queryByAccount(account);
        User userQI = userDao.queryById(userid);
        switch (accountType) {
            case 0:
                if (userQA != null && !Objects.equals(userQI.getUsername(), account)) {
                    result.setStatus(false);
                    result.setMessage("用户名已注册！");
                    return result;
                }
                break;
            case 1:
                if (userQA != null && !Objects.equals(userQI.getEmail(), account)) {
                    result.setStatus(false);
                    result.setMessage("邮箱已注册！");
                    return result;
                }
                break;
            case 2:
                if (userQA != null && !Objects.equals(userQI.getPhonenumber(), account)) {
                    result.setStatus(false);
                    result.setMessage("手机号已注册！");
                    return result;
                }
                break;
        }
        result.setStatus(true);
        return result;
    }

    @Override
    public PageInfo<User> queryAll(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return new PageInfo<>(userDao.queryAll());
    }

    @Override
    public PageInfo<User> fuzzyQuery(Integer userid, String username, Integer pageNum,
                                     Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return new PageInfo<>(userDao.fuzzyQuery(userid, username));
    }

    @Override
    public List<Dept> queryDeptList() {
        return userDao.queryDeptList();
    }

    @Override
    public List<Role> queryRoleList() {
        return userDao.queryRoleList();
    }

    @Override
    public AjaxResult insert(User user) {
        AjaxResult result = new AjaxResult();
        // 1.如果用户已存在，返回提示信息
        if (userDao.queryByAccount(user.getUsername()) != null) {
            result.setStatus(false);
            result.setMessage("用户名" + user.getUsername() + "已注册！");
            return result;
        }
        // 2.如果邮箱已注册，返回提示信息
        if (userDao.queryByAccount(user.getEmail()) != null) {
            result.setStatus(false);
            result.setMessage("邮箱" + user.getEmail() + "已注册！");
            return result;
        }
        // 3.如果手机号已注册，返回提示信息
        if (userDao.queryByAccount(user.getPhonenumber()) != null) {
            result.setStatus(false);
            result.setMessage("手机号" + user.getPhonenumber() + "已注册！");
            return result;
        }
        // 4.如果用户、邮箱和手机号均未注册，直接添加
        // 4.0把明文密码改为MD5密码加盐
        String[] md5PwdAndSalt = MD5Utils.getMD5PwdAndSalt(user.getUsername(), user.getPassword());
        user.setPassword(md5PwdAndSalt[0]);
        user.setSalt(md5PwdAndSalt[1]);
        userDao.insert(user);
        result.setStatus(true);
        return result;
    }

    @Override
    public AjaxResult update(User user) {
        AjaxResult result = new AjaxResult();
        // 1.如果用户已存在，且用户名不是当前用户名，返回提示信息
        if (userDao.queryByAccount(user.getUsername()) != null &&
                !Objects.equals(userDao.queryById(user.getUserid()).getUsername(),
                                user.getUsername())) {
            result.setStatus(false);
            result.setMessage("用户名" + user.getUsername() + "已注册！");
            return result;
        }
        // 2.如果邮箱已注册，且邮箱不是当前邮箱，返回提示信息
        if (userDao.queryByAccount(user.getEmail()) != null &&
                !Objects.equals(userDao.queryById(user.getUserid()).getEmail(),
                                user.getEmail())) {
            result.setStatus(false);
            result.setMessage("邮箱" + user.getEmail() + "已注册！");
            return result;
        }
        // 3.如果手机号已注册，且手机号不是当前手机号，返回提示信息
        if (userDao.queryByAccount(user.getPhonenumber()) != null &&
                !Objects.equals(userDao.queryById(user.getUserid()).getPhonenumber(),
                                user.getPhonenumber())) {
            result.setStatus(false);
            result.setMessage("手机号" + user.getPhonenumber() + "已注册！");
            return result;
        }
        // 4.如果用户、邮箱和手机号均未注册，直接修改
        userDao.update(user);
        result.setStatus(true);
        return result;
    }

    @Override
    public AjaxResult delete(Integer userid) {
        AjaxResult result = new AjaxResult();
        userDao.delete(userid);
        result.setStatus(true);
        return result;
    }

    @Override
    public AjaxResult deleteByIds(Integer[] userIds) {
        AjaxResult result = new AjaxResult();
        userDao.deleteByIds(userIds);
        result.setStatus(true);
        return result;
    }

    @Override
    public User signin(String account, String pwdSignin) {
        User user = userDao.queryByAccount(account);
        boolean isMD5Pwd = MD5Utils.isMD5Pwd(user.getSalt(), pwdSignin, user.getPassword());
        if (user == null || !isMD5Pwd) {
            // 用户不存在或密码错误
            return null;
        }
        return user;
    }

    @Override
    public AjaxResult resetPwd(String username, String pwdSignin) {
        AjaxResult result = new AjaxResult();
        String[] md5PwdAndSalt = MD5Utils.getMD5PwdAndSalt(username, pwdSignin);
        userDao.resetPwd(username, md5PwdAndSalt[0], md5PwdAndSalt[1]);
        result.setStatus(true);
        return result;
    }

    @Override
    public AjaxResult modifyPwd(String account, String oldPwd, String newPwd) {
        AjaxResult result = new AjaxResult();
        User user = userDao.queryByAccount(account);
        // 只有旧密码正确，才修改成新密码
        if (!MD5Utils.isMD5Pwd(user.getSalt(), oldPwd, user.getPassword())) {
            result.setStatus(false);
            result.setMessage("帐号或密码错误！");
            return result;
        }
        resetPwd(account, newPwd);
        result.setStatus(true);
        result.setMessage("成功修改密码");
        return result;
    }
}