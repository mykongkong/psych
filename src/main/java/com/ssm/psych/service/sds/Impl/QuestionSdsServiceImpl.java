package com.ssm.psych.service.sds.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.sds.QuestionSdsDao;
import com.ssm.psych.dao.test.TblTesterDao;
import com.ssm.psych.domain.QuestionSds;
import com.ssm.psych.domain.TesterVo;
import com.ssm.psych.service.sds.QuestionSdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @Description 2.抑郁测试-题目管理-服务层
 * @Author dpj
 * @Date 2022-11-26 10:24
 * @Version 1.0
 */
@Service
public class QuestionSdsServiceImpl implements QuestionSdsService {
    @Autowired
    private TblTesterDao tblTesterDao;
    private QuestionSdsDao questionSdsDao;

    @Autowired
    public void setQuestionSdsDao(QuestionSdsDao questionSdsDao) {
        this.questionSdsDao = questionSdsDao;
    }

    @Override
    public List<QuestionSds> findAll() {
        return questionSdsDao.queryAll();
    }

    @Override
    public QuestionSds queryById(Integer id) {
        return questionSdsDao.queryById(id);
    }

    @Override
    public PageInfo<QuestionSds> queryAll(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return new PageInfo<>(questionSdsDao.queryAll());
    }

    @Override
    public PageInfo<QuestionSds> fuzzyQuery(String question, String createBy, Integer pageNum,
                                            Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return new PageInfo<>(questionSdsDao.fuzzyQuery(question, createBy));
    }

    @Override
    public AjaxResult insert(QuestionSds questionSds) {
        AjaxResult result = new AjaxResult();
        // 1.如果问题不存在，直接添加
        if (questionSdsDao.queryByQuestion(questionSds.getQuestion()) == null) {
            questionSdsDao.insert(questionSds);
            result.setStatus(true);
            return result;
        }
        // 2.如果问题已存在，返回提示信息
        result.setStatus(false);
        result.setMessage("此问题已存在！");
        return result;
    }

    @Override
    public AjaxResult update(QuestionSds questionSds) {
        AjaxResult result = new AjaxResult();
        // 1.如果修改后的题目和其他题目不重名，直接修改
        if(questionSdsDao.queryByQuestion(questionSds.getQuestion())==null
        || Objects.equals(questionSdsDao.queryById(questionSds.getId()).getQuestion(),
                          questionSds.getQuestion())){
            try {
                questionSdsDao.update(questionSds);
                result.setStatus(true);
            } catch (Exception e) {
                e.printStackTrace();
                result.setStatus(false);
                result.setMessage("修改失败！");
            }
        }else {
            // 2.如果修改后的部门和其他部门重名（不包括自身），返回提示信息
            result.setStatus(false);
            result.setMessage("此问题已存在！");
        }
        return result;
    }

    @Override
    public AjaxResult delete(Integer id) {
        AjaxResult result = new AjaxResult();
        questionSdsDao.delete(id);
        result.setStatus(true);
        return result;
    }

    @Override
    public AjaxResult deleteByIds(Integer[] ids) {
        AjaxResult result = new AjaxResult();
        questionSdsDao.deleteByIds(ids);
        result.setStatus(true);
        return result;
    }

    @Override
    public void insertSds(TesterVo testerVo) {
        tblTesterDao.insertSds(testerVo);
    }
}