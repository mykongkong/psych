package com.ssm.psych.service.sds;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.ResultCountSds;
import com.ssm.psych.domain.ViewTestResultSds;

/**
 * @author ：dpj
 * @Date ：Created in 2022/12/7 19:40
 * @Description 3.抑郁测试-测试结果-service层接口
 */
public interface ViewTestResultSdsService {
    // 查询所有的数据
    PageInfo<ViewTestResultSds> queryAll(Integer page, Integer limit);

    // 通过ID查询数据
    PageInfo<ViewTestResultSds> queryID(Integer id);

    // 删除数据
    AjaxResult delete(Integer[] idList);

    // 模糊查询
    PageInfo<ViewTestResultSds> fuzzyQuery(ViewTestResultSds viewTestResultSds);

    ResultCountSds getCount();
}