package com.ssm.psych.service.sds.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.sds.ViewTestResultSdsDao;
import com.ssm.psych.domain.ResultCountSds;
import com.ssm.psych.domain.ViewTestResultSds;
import com.ssm.psych.service.sds.ViewTestResultSdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description 3.抑郁测试-测试结果-服务层
 * @Author dpj
 * @Date 2022-12-07 19:41
 * @Version 1.0
 */
@Service
public class ViewTestResultSdsServiceImpl implements ViewTestResultSdsService {
    private ViewTestResultSdsDao viewTestResultSdsDao;

    @Autowired
    public void setViewTestResultSdsDao(ViewTestResultSdsDao viewTestResultSdsDao) {
        this.viewTestResultSdsDao = viewTestResultSdsDao;
    }

    /**
     * 调用DAO层的queryAll的方法查询数据的所有数据
     *
     * @param page  分页的页码
     * @param limit 分页的每页的数据数
     * @return 返回查询到的结果
     */
    @Override
    public PageInfo<ViewTestResultSds> queryAll(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        List<ViewTestResultSds> sds = viewTestResultSdsDao.queryAll();
        return new PageInfo<>(sds);
    }

    /**
     * 调用DAO层的queryID的方法查询指定ID的数据
     *
     * @param id 传入需要查询的ID
     * @return 返回查询到的结果
     */
    @Override
    public PageInfo<ViewTestResultSds> queryID(Integer id) {
        List<ViewTestResultSds> sds = viewTestResultSdsDao.queryID(id);
        return new PageInfo<>(sds);
    }

    /**
     * 调用DAO层的delete的方法删除数据库指定的数据
     *
     * @param idList 　传入需要删除的ID
     * @return 返回删除的结果数据
     */
    @Override
    public AjaxResult delete(Integer[] idList) {
        int delete = viewTestResultSdsDao.delete(idList);
        AjaxResult result = new AjaxResult();
        if (delete > 0) {
            result.setStatus(true);
            result.setMessage("删除成功");
            return result;
        } else {
            result.setStatus(false);
            result.setMessage("删除失败");
            return result;
        }
    }

    /**
     * 调用DAO层的fuzzyQuery的方法对数据库进行模糊查询
     *
     * @param viewTestResultSds 　传入需要查询的数据
     * @return 返回查询到的结果
     */
    @Override
    public PageInfo<ViewTestResultSds> fuzzyQuery(ViewTestResultSds viewTestResultSds) {
        List<ViewTestResultSds> list = viewTestResultSdsDao.fuzzyQuery(viewTestResultSds);
        return new PageInfo<>(list);
    }

    /**
     * 获取不同焦虑程度的统计个数
     *
     * @return 将得到的统计个数给返回
     */
    @Override
    public ResultCountSds getCount() {
        int normal = 0;
        int mild = 0;
        int middle = 0;
        int severe = 0;
        for (ViewTestResultSds testResultSds : viewTestResultSdsDao.queryAll()) {
            int f = testResultSds.getForward();
            int ins = testResultSds.getInversion();
            int score = f + ins;
            if (score < 40) {
                normal++;
            } else if (score <= 46) {
                mild++;
            } else if (score <= 55) {
                middle++;
            } else if (score > 55) {
                severe++;
            }
        }
        return new ResultCountSds(normal, mild, middle, severe);
    }
}