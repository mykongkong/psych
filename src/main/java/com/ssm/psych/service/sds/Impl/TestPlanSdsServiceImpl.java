package com.ssm.psych.service.sds.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.sds.TestPlanSdsDao;
import com.ssm.psych.domain.TestPlanSds;
import com.ssm.psych.service.sds.TestPlanSdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description 3.抑郁测试-测试计划-服务层
 * @Author 邓鹏杰
 * @Date 2022-12-05 10:41
 * @Version 1.0
 */
@Service
public class TestPlanSdsServiceImpl implements TestPlanSdsService {
    private TestPlanSdsDao testPlanSdsDao;

    @Autowired
    public void setTestPlanSdsDao(TestPlanSdsDao testPlanSdsDao) {
        this.testPlanSdsDao = testPlanSdsDao;
    }

    /**
     * 调用DAO层的queryAll的方法查询数据库中的所有的数据
     *
     * @param page  分页的页码
     * @param limit 分页的每页的数据数
     * @return 返回查询到的数据
     */
    @Override
    public PageInfo<TestPlanSds> queryAll(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        return new PageInfo<>(testPlanSdsDao.queryAll());
    }

    /**
     * 调用DAO层的update的方法修改数据库中指定的数据
     *
     * @param testPlanSds 传入需要修改的数据
     * @return 返回修改结果的数据
     */
    @Override
    public AjaxResult update(TestPlanSds testPlanSds) {
        AjaxResult ajaxResult = new AjaxResult();
        int update = testPlanSdsDao.update(testPlanSds);
        if (update > 0) {
            ajaxResult.setStatus(true);
            ajaxResult.setMessage("修改成功");
            return ajaxResult;
        } else {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("修改失败");
            return ajaxResult;
        }
    }

    /**
     * 调用DAO层的insert的方法添加指定的数据到数据库
     *
     * @param testPlanSds 传入需要添加的数据
     * @return 返回添加的结果数据
     */
    @Override
    public AjaxResult insert(TestPlanSds testPlanSds) {
        AjaxResult ajaxResult = new AjaxResult();
        int insert = testPlanSdsDao.insert(testPlanSds);
        if (insert > 0) {
            ajaxResult.setStatus(true);
            ajaxResult.setMessage("添加成功");
            return ajaxResult;
        } else {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("添加失败");
            return ajaxResult;
        }
    }

    /**
     * 调用DAO层的delete的方法删除数据库中指定的数据
     *
     * @param idList 　传入指定ID
     * @return 返回删除的结果的数据
     */
    @Override
    public AjaxResult delete(Integer[] idList) {
        int delete = testPlanSdsDao.delete(idList);
        AjaxResult ajaxResult = new AjaxResult();
        if (delete > 0) {
            ajaxResult.setStatus(true);
            ajaxResult.setMessage("删除成功");
            return ajaxResult;
        } else {
            ajaxResult.setStatus(false);
            ajaxResult.setMessage("删除失败");
            return ajaxResult;
        }
    }

    /**
     * 调用DAO层的fuzzyQuery的方法对数据库进行模糊查询
     *
     * @param testName 传入测试名
     * @param createBy 传入创建的名字
     * @return 返回查询到的结果
     */
    @Override
    public PageInfo<TestPlanSds> fuzzyQuery(String testName, String createBy) {
        List<TestPlanSds> testPlanSds = testPlanSdsDao.fuzzyQuery(testName, createBy);
        return new PageInfo<>(testPlanSds);
    }
}