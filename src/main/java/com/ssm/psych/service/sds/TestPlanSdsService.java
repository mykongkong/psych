package com.ssm.psych.service.sds;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.TestPlanSds;

/**
 * @author ：dpj
 * @Date ：Created in 2022/12/5 10:40
 * @Description 3.抑郁测试SDS-测试计划-service层接口
 */
public interface TestPlanSdsService {
    // 查询数据
    PageInfo<TestPlanSds> queryAll(Integer page, Integer limit);

    // 修改数据
    AjaxResult update(TestPlanSds testPlanSds);

    // 添加数据
    AjaxResult insert(TestPlanSds testPlanSds);

    // 删除数据
    AjaxResult delete(Integer[] idList);

    // 模糊查询
    PageInfo<TestPlanSds> fuzzyQuery(String testName, String createBy);
}