package com.ssm.psych.service.sds;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.QuestionSds;
import com.ssm.psych.domain.TesterVo;

import java.util.List;

public interface QuestionSdsService {
    List<QuestionSds> findAll();
    QuestionSds queryById(Integer id);

    PageInfo<QuestionSds> queryAll(Integer pageNum, Integer pageSize);

    PageInfo<QuestionSds> fuzzyQuery(String question, String createBy, Integer pageNum,
                                     Integer pageSize);

    AjaxResult insert(QuestionSds questionSds);

    AjaxResult update(QuestionSds questionSds);

    AjaxResult delete(Integer id);

    AjaxResult deleteByIds(Integer[] ids);
    void insertSds(TesterVo testerVo);
}