package com.ssm.psych.utils;

public interface UserConstants {
    String SESSION_USER = "SESSION_USER";
    String SESSION_AVATAR = "SESSION_AVATAR";
}