package com.ssm.psych.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.util.DigestUtils;

/**
 * @Description MD5加盐加密工具类
 * @Author dpj
 * @Date 2022-12-17 09:31
 * @Version 1.0
 */
public class MD5Utils {
    // 添加时生成盐和密码
    public static String[] getMD5PwdAndSalt(String username, String pwdSignin) {
        char[] saltBase = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        String salt = RandomStringUtils.random(32, saltBase);
        String pwdMD5 = DigestUtils.md5DigestAsHex((salt + pwdSignin).getBytes());
        return new String[]{pwdMD5, salt};
    }

    // 登录时验证加盐密码
    public static boolean isMD5Pwd(String salt, String pwdSignin, String pwdMD5) {
        return DigestUtils.md5DigestAsHex((salt + pwdSignin).getBytes()).equals(pwdMD5);
    }
}