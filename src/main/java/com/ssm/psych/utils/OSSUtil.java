package com.ssm.psych.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

/**
 * @Description 阿里云OSS工具
 * @Author dpj
 * @Date 2022-11-22 09:16
 * @Version 1.0
 */
public class OSSUtil {
    private static final Properties properties = new Properties();

    static {
        InputStream inputStream = OSSUtil.class.getResourceAsStream("/oss.properties");
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String uploadAvatarImg(MultipartFile multipartFile) {
        InputStream inputStream = null;
        OSS ossClient = null;
        try {
            // 1.获取上传文件的文件名称
            String originalFilename = multipartFile.getOriginalFilename();
            // 2.获取上传文件的后缀名
            assert originalFilename != null;
            String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
            // 3.随机生成新的文件名，防止和已经存在的文件重名
            String newFileName = UUID.randomUUID() + suffix;
            // 4.获取上传文件的输入流
            inputStream = multipartFile.getInputStream();
            // 5.创建OSSClient实例
            ossClient = new OSSClientBuilder().build(properties.getProperty("oss.endpoint"),
                                                     properties.getProperty("oss.accessKeyId"),
                                                     properties.getProperty("oss.accessKeySecret"));
            // 6.创建PutObject请求
            ossClient.putObject(properties.getProperty("oss.bucketName"),
                                "avatarImg/" + newFileName, inputStream);
            String url = properties.getProperty("oss.url") + "/" + "avatarImg/" + newFileName;
            ossClient.shutdown();
            return url;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}