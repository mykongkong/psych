package com.ssm.psych.filter;

import com.ssm.psych.utils.UserConstants;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description 登录过滤器
 * @Author dpj
 * @Date 2022-12-19 12:00
 * @Version 1.0
 */
public class SigninFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (request.getSession().getAttribute(UserConstants.SESSION_USER) == null
                && !request.getRequestURI().contains("/signin")) {
            // 如果没有登录，则强制跳转到登录界面
            request.getRequestDispatcher("/").forward(request, response);
        } else {
            // 如果已经登录，则继续请求下一级资源
            filterChain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}