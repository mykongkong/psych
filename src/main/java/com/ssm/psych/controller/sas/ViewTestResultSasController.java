package com.ssm.psych.controller.sas;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.ResultCountSas;
import com.ssm.psych.domain.ViewTestResultSas;
import com.ssm.psych.service.sas.ViewTestResultSasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description 焦虑测试-测试结果-控制层
 * @Author 情雅圣
 * @Date 2022-12-07 19:48
 * @Version 1.0
 */
@Controller
@RequestMapping("/sas/result")
public class ViewTestResultSasController {
    @Autowired
    private ViewTestResultSasService viewTestResultSasService;

    /**
     * 查询焦虑测试－测试结果的数据库所有数据
     * @param page　分页的页码
     * @param limit　分页的每页的数据数
     * @return 返回查到的数据给前端
     */
    @RequestMapping("/queryAll")
    @ResponseBody
    public TableData<ViewTestResultSas> queryAll(Integer page,Integer limit){
        PageInfo<ViewTestResultSas> all = viewTestResultSasService.queryAll(page, limit);
        return new TableData<>(all.getTotal(),all.getList());
    }

    /**
     * 查询指定ID的数据
     * @param id　传入的Id
     * @return 返回查询得到的数据
     */
    @RequestMapping("/queryID")
    @ResponseBody
    public TableData<ViewTestResultSas> queryID(Integer id){
        PageInfo<ViewTestResultSas> queryID = viewTestResultSasService.queryID(id);
        return new TableData<>(queryID.getTotal(),queryID.getList());
    }

    /**
     * 删除数据库中指定Id的数据
     * @param idList　传入指定的ID
     * @return 返回删除的结果并返亏给前端
     */
    @RequestMapping("/delete")
    @ResponseBody
    public AjaxResult delete(@RequestBody Integer[] idList){
        return viewTestResultSasService.delete(idList);
    }

    /**
     * 模糊查询测试结果的数据
     * @param viewTestResultSas　传入需要查询的数据
     * @return 返回查询都得到的数据
     */
    @RequestMapping("/fuzzyQuery")
    @ResponseBody
    public TableData<ViewTestResultSas> fuzzyQuery(ViewTestResultSas viewTestResultSas){
        PageInfo<ViewTestResultSas> fuzzyQuery = viewTestResultSasService.fuzzyQuery(viewTestResultSas);
        return new TableData<>(fuzzyQuery.getTotal(),fuzzyQuery.getList());
    }

    /**
     * 报表统计查询的数据统计的不同焦虑程度的个数
     * @return 将查询的数据返回给前端
     */
    @RequestMapping("/resultCount")
    @ResponseBody
    public ResultCountSas resultCount(){
        return viewTestResultSasService.getCount();
    }
} 
