package com.ssm.psych.controller.sas;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.QuestionSas;
import com.ssm.psych.interceptor.MyAdvice;
import com.ssm.psych.service.sas.QuestionSasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description 3.焦虑测试-题目管理-控制层
 * @Author dpj
 * @Date 2022-11-26 10:33
 * @Version 1.0
 */
@Controller
@RequestMapping("/sas/question")
public class QuestionSasController {

    private QuestionSasService questionSasService;

    @Autowired
    public void setQuestionSasService(QuestionSasService questionSasService) {
        this.questionSasService = questionSasService;
    }

    /**
     * 查询焦虑测试－题目管理的数据的所有数据
     * @param page　分页的页码
     * @param limit　分页的每页的数据数
     * @return 将查询的到的数据返回给前端
     */
    @RequestMapping("/queryAll")
    @ResponseBody
    public TableData<QuestionSas> queryAll(Integer page, Integer limit) {
        PageInfo<QuestionSas> pageInfo = questionSasService.queryAll(page, limit);
        return new TableData<>(pageInfo.getTotal(), pageInfo.getList());
    }

    /**
     * 修改焦虑测试－题目管理中指定id的数据
     * @param questionSas　传入需要修改的数据
     * @return 返回修改结果的数据
     */
    @RequestMapping("/update")
    @ResponseBody
    public AjaxResult update(QuestionSas questionSas, HttpServletRequest request){
        MyAdvice myAdvice=new MyAdvice();
        String userName = myAdvice.getUserName(request);
        questionSas.setUpdateBy(userName);
        return questionSasService.update(questionSas);
    }

    /**
     * 向焦虑测试－题目管理的数据库添加指定的数据
     * @param questionSas　需要添加的数据
     * @return 返回添加结果的数据
     */
    @RequestMapping("/insert")
    @ResponseBody
    public AjaxResult insert(QuestionSas questionSas, HttpServletRequest request){
        MyAdvice myAdvice=new MyAdvice();
        String userName = myAdvice.getUserName(request);
        questionSas.setCreateBy(userName);
        return questionSasService.insert(questionSas);
    }

    /**
     * 对数据进行模糊查询
     * @param questionSas　传入需要查询的数据
     * @return 返回查询的到数据
     */
    @RequestMapping("/fuzzyQuery")
    @ResponseBody
    public TableData<QuestionSas> fuzzyQuery(QuestionSas questionSas){
        PageInfo<QuestionSas> info = questionSasService.fuzzyQuery(questionSas);
        return new TableData<>(info.getTotal(),info.getList());
    }

    /**
     * 删除焦虑测试－题目管理数据库中指定id的数据
     * @param idList　传入指定改的ID
     * @return 返回删除结果的数据
     */
    @RequestMapping("/delete")
    @ResponseBody
    public AjaxResult delete(@RequestBody Integer[] idList){
        return questionSasService.delete(idList);
    }
}