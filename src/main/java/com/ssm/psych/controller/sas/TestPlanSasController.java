package com.ssm.psych.controller.sas;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.TestPlanFpa;
import com.ssm.psych.domain.TestPlanSas;
import com.ssm.psych.interceptor.MyAdvice;
import com.ssm.psych.service.fpa.TestPlanFpaService;
import com.ssm.psych.service.sas.TestPlanSasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @Description 焦虑测试-测试计划-控制层
 * @Author 情雅圣
 * @Date 2022-12-05 10:47
 * @Version 1.0
 */
@Controller
@RequestMapping("/sas/test")
public class TestPlanSasController {
    @Autowired
    private TestPlanSasService testPlanSasService;
    @Autowired
    private TestPlanFpaService testPlanFpaService;

    /**
     * 查询焦虑测试－测试计划数据库的所有数据
     * @param page　分页的页码
     * @param limit　分页的每页显示的数据数
     * @return 返回查询得到的数据
     */
    @RequestMapping("/queryAll")
    @ResponseBody
    public TableData queryAll(Integer page,Integer limit){
        PageInfo<TestPlanSas> pageInfo = testPlanSasService.queryAll(page, limit);
        return new TableData<>(pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 修改焦虑测试－测试计划数据库中指定ID的数据
     * @param testPlanSas　传入需要修改的数据
     * @return 返回修改结果的数据
     */
    @RequestMapping("/update")
    @ResponseBody
    public AjaxResult update(TestPlanSas testPlanSas, HttpServletRequest request){
        MyAdvice myAdvice=new MyAdvice();
        String userName = myAdvice.getUserName(request);
        testPlanSas.setUpdateBy(userName);
        return testPlanSasService.update(testPlanSas);
    }

    /**
     * 添加指定改的数据到焦虑测试－测试计划的数据
     * @param testPlanSas　需要添加的数据
     * @return 返回添加成功的结果数据
     */
    @RequestMapping("/insert")
    @ResponseBody
    public AjaxResult insert(TestPlanSas testPlanSas,HttpServletRequest request){
        MyAdvice myAdvice=new MyAdvice();
        String userName = myAdvice.getUserName(request);
        testPlanSas.setCreateBy(userName);
        int code = testPlanFpaService.getRandom();
        testPlanSas.setTestCode(code);
        return testPlanSasService.insert(testPlanSas);
    }

    /**
     *　删除指定给ID的数据
     * @param idList 传入指定的ID
     * @return 返回删除数据结果的数据
     */
    @RequestMapping("/delete")
    @ResponseBody
    public AjaxResult delete(@RequestBody Integer[] idList){
        return testPlanSasService.delete(idList);
    }

    /**
     * 模糊查询焦虑测试-测试计划数据库中的数据
     * @param testName 传入查询的测试计划的名字
     * @param createBy 传入需要查询的创建者名字
     * @return 返回查询得到的数据
     */
    @RequestMapping("/fuzzyQuery")
    @ResponseBody
    public TableData<TestPlanSas> fuzzyQuery(String testName,String createBy){
        PageInfo<TestPlanSas> query = testPlanSasService.fuzzyQuery(testName, createBy);
        return new TableData<>(query.getTotal(),query.getList());
    }
    @RequestMapping("/proofDate")
    @ResponseBody
    public AjaxResult proofDate(TestPlanFpa testPlanFpa){
        Date testEnd = testPlanFpa.getTestEnd();
        AjaxResult ajaxResult=new AjaxResult();
        if(new Date().after(testEnd)){
            ajaxResult.setStatus(true);
            ajaxResult.setMessage("日期不可以在今天之前");
            return ajaxResult;
        }else {
            ajaxResult.setStatus(false);
            return ajaxResult;
        }
    }
} 
