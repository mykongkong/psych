package com.ssm.psych.controller.fpa;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.TestPlanFpa;
import com.ssm.psych.interceptor.MyAdvice;
import com.ssm.psych.service.fpa.TestPlanFpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Random;

/**
 * @Description 性格测试-测试计划-控制层
 * @Author 情雅圣
 * @Date 2022-12-01 17:08
 * @Version 1.0
 */
@Controller
@RequestMapping("/fpa/test")
public class TestPlanFpaController {
    @Autowired
    private TestPlanFpaService testPlanFpaService;

    /**
     * 查询数据库中所有的数据并返回给前端
     * @param page 分页的页码
     * @param limit 分页之后一页显示的数据条数
     * @return 将得到的数据封装到TableData中返回给前端
     */
    @RequestMapping("/queryAll")
    @ResponseBody//将Java对象转成JSON数据的注解
    public TableData queryAll(Integer page, Integer limit){
        PageInfo<TestPlanFpa> pageInfo = testPlanFpaService.queryAll(page, limit);
        return new TableData<TestPlanFpa>(pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 添加数据到数据库
     * @param testPlanFpa 把需要添加的数据封装带此实体类
     * @return 返回添加后给前端的反馈
     */
    @RequestMapping("/insert")
    @ResponseBody
    public AjaxResult insert(TestPlanFpa testPlanFpa,HttpServletRequest request){
        MyAdvice myAdvice=new MyAdvice();
        String userName = myAdvice.getUserName(request);
        testPlanFpa.setCreateBy(userName);
        int code = testPlanFpaService.getRandom();
        testPlanFpa.setTestCode(code);
        return testPlanFpaService.insert(testPlanFpa);
    }

    /**
     * 修改数据库中的数据
     * @param testPlanFpa 将修改的数据封装到实体类，给到后端
     * @return 返回修改之后的结果反馈给前端
     */
    @RequestMapping("/update")
    @ResponseBody
    public AjaxResult update(TestPlanFpa testPlanFpa, HttpServletRequest request){
        MyAdvice myAdvice=new MyAdvice();
        String userName = myAdvice.getUserName(request);
        testPlanFpa.setUpdateBy(userName);
        return testPlanFpaService.update(testPlanFpa);
    }

    /**
     * 删除想要删除指定id的数据
     * @param idArray 可以是单个id，也可以是多个id
     * @return 返回删除的结果反馈给前端
     */
    @RequestMapping("/delete")
    @ResponseBody
    public AjaxResult deleteByIds(@RequestBody Integer[] idArray){
        return testPlanFpaService.deleteByIds(idArray);
    }

    /**
     * 模糊查询功能的控制层逻辑
     * @param testPlanFpa 需要查询的数据信息依实体类传给后端
     * @return 返回查询得到的数据
     */
    @RequestMapping("/fuzzyQuery")
    @ResponseBody
    public TableData fuzzyQuery(TestPlanFpa testPlanFpa){
        PageInfo<TestPlanFpa> pageInfo = testPlanFpaService.fuzzyQuery(testPlanFpa);
        return new TableData<>(pageInfo.getTotal(), pageInfo.getList());
    }
    @RequestMapping("/proofDate")
    @ResponseBody
    public AjaxResult proofDate(TestPlanFpa testPlanFpa){
        Date testEnd = testPlanFpa.getTestEnd();
        AjaxResult ajaxResult=new AjaxResult();
        if(new Date().after(testEnd)){
            ajaxResult.setStatus(true);
            ajaxResult.setMessage("日期不可以在今天之前");
            return ajaxResult;
        }else {
            ajaxResult.setStatus(false);
            return ajaxResult;
        }
    }
}
