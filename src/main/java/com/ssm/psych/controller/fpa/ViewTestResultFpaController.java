package com.ssm.psych.controller.fpa;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.ResultCountFpa;
import com.ssm.psych.domain.TblResultDiv;
import com.ssm.psych.domain.ViewTestResultFpa;
import com.ssm.psych.service.fpa.ViewTestResultFpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description 性格测试-测试结果-控制层
 * @Author 情雅圣
 * @Date 2022-12-05 16:18
 * @Version 1.0
 */
@Controller
@RequestMapping("/fpa/result")
public class ViewTestResultFpaController {
    @Autowired
    private ViewTestResultFpaService viewTestResultFpaService;

    /**
     * 查询所有数据并返回给前端进行数据渲染
     * @param page 显示的前端页码
     * @param limit 显示的前端一页显示的数据条数
     * @return 将查询的所有数据返回给前端
     */
    @RequestMapping("/queryAll")
    @ResponseBody
    public TableData<ViewTestResultFpa> queryAll(Integer page,Integer limit){
        PageInfo<ViewTestResultFpa> all = viewTestResultFpaService.queryAll(page, limit);
        return new TableData<>(all.getTotal(),all.getList());
    }

    /**
     * 删除数据库中的数据
     * @param idList 可以传入单个ID，也可以传入多个ID数组
     * @return 返回删除数据的结果反馈给前端
     */
    @RequestMapping("/delete")
    @ResponseBody
    public AjaxResult delete(@RequestBody Integer[] idList){
        return viewTestResultFpaService.delete(idList);
    }

    /**
     * 模糊查询数据
     * @param viewTestResultFpa 需要查询的数据依实体类传入后端
     * @return 返回查询得到的数据使其渲染给前端
     */
    @RequestMapping("/fuzzyQuery")
    @ResponseBody
    public TableData<ViewTestResultFpa> fuzzyQuery(ViewTestResultFpa viewTestResultFpa){
        PageInfo<ViewTestResultFpa> pageInfo = viewTestResultFpaService.fuzzyQuery(viewTestResultFpa);
        return new TableData<>(pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 通过Id查询指定的数据
     * @param testerId 传入指定的Id
     * @return 返回查询得到的数据
     */
    @RequestMapping("/queryID")
    @ResponseBody
    public TableData<ViewTestResultFpa> queryID(Integer testerId){
        PageInfo<ViewTestResultFpa> id = viewTestResultFpaService.queryID(testerId);
        return new TableData<>(id.getTotal(),id.getList());
    }

    /**
     * 查询Div数据库得到查询结果对应的结果DIV
     * @param id 传入指定的id
     * @return 返回查询得到的数据
     */
    @RequestMapping("/queryDiv")
    @ResponseBody
    public TableData<TblResultDiv> queryDiv(Integer id){
        PageInfo<TblResultDiv> pageInfo = viewTestResultFpaService.queryDiv(id);
        return new TableData<>(pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 通过后端得到的数据结果的统计数返回给前端
     * @return 返回相应的统计数
     */
    @RequestMapping("/resultCount")
    @ResponseBody
    public ResultCountFpa resultCount(){
        ResultCountFpa result = viewTestResultFpaService.getResult();
        return result;
    }
} 
