package com.ssm.psych.controller.fpa;

import com.alibaba.fastjson2.JSON;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.fpa.QuestionFpaDao;
import com.ssm.psych.domain.Beans;
import com.ssm.psych.domain.QuestionFpa;
import com.ssm.psych.interceptor.MyAdvice;
import com.ssm.psych.service.fpa.QuestionFpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Description 1.性格测试-题目管理-控制层
 * @Author dpj
 * @Date 2022-11-26 10:31
 * @Version 1.0
 */
@Controller
@RequestMapping("/fpa/question")
public class QuestionFpaController {
    @Autowired
    private QuestionFpaDao questionFpaDao;
    private QuestionFpaService questionFpaService;

    @Autowired
    public void setQuestionFpaService(QuestionFpaService questionFpaService) {
        this.questionFpaService = questionFpaService;
    }

    /**
     * 查询出所有的数据
     * @param response 把数据返回给前端页面的对象
     * @param request 接收前端数据的对象
     * @param page 分页的页码
     * @param limit 每页显示的数据
     * @throws IOException
     */
    @RequestMapping("/findAll")
    public void findAll(HttpServletResponse response,
            HttpServletRequest request,Integer page,Integer limit) throws IOException {
        List<QuestionFpa> all = questionFpaService.findAll(page,limit);
        List<QuestionFpa> daoAll = questionFpaDao.findAll();
        int size = daoAll.size();
        Beans beans=new Beans<QuestionFpa>(0,"",size,all);
        String s = JSON.toJSONString(beans);
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write(s);
    }

    /**
     * 数据库添加数据的操作
     * @param questionFpa 需要添加的对象
     * @return 返回一个结果值
     */
    @RequestMapping("/add")
    @ResponseBody
    public AjaxResult add(QuestionFpa questionFpa,HttpServletRequest request)  {
        MyAdvice myAdvice=new MyAdvice();
        String userName = myAdvice.getUserName(request);
        questionFpa.setCreateBy(userName);
        AjaxResult all = questionFpaService.add(questionFpa);
        System.out.println(all);
        return all;
    }

    /**
     * 删除数据库中指定ID的数据
     * @param id 传入指定的Id值
     */
    @RequestMapping("/delete")
    public void delete(int id){
        int i = questionFpaService.deleteId(id);
        System.out.println(i);
    }

    /**
     * 修改测试计划数据库中指定Id的数据
     * @param questionFpa 传入指定的想要修改为的对象
     * @return 返回修改成功或失败的数据
     */
    @RequestMapping("/update")
    @ResponseBody
    public AjaxResult update(QuestionFpa questionFpa,HttpServletRequest request){
        MyAdvice myAdvice=new MyAdvice();
        String userName = myAdvice.getUserName(request);
        questionFpa.setUpdateBy(userName);
        AjaxResult ajaxResult = questionFpaService.update(questionFpa);
        return ajaxResult;
    }

    /**
     * 模糊查询查询数据
     * @param response 向前端传入数据的对象
     * @param request 获取前端数据的对象
     * @param questionFpa 查询得到的对象
     * @throws IOException 抛出异常
     *
     */
    @RequestMapping("/fuzzyQuery")
    public void fuzzyQuery(HttpServletResponse response,
                        HttpServletRequest request,QuestionFpa questionFpa) throws IOException {
        List<QuestionFpa> all = questionFpaService.fuzzyQuery(questionFpa);
        Beans beans=new Beans<QuestionFpa>(0,"",100,all);
        String s = JSON.toJSONString(beans);
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write(s);
    }

    /**
     * 删除多条数据
     * @param idList 传入id的数组
     */
    @RequestMapping("/deleteByIds")
    public void deleteByIds(@RequestBody Integer[] idList){
        questionFpaService.deleteByIds(idList);
    }
}