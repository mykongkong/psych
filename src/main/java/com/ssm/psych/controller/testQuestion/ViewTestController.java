package com.ssm.psych.controller.testQuestion;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.*;
import com.ssm.psych.service.fpa.QuestionFpaService;
import com.ssm.psych.service.sas.QuestionSasService;
import com.ssm.psych.service.sds.QuestionSdsService;
import com.ssm.psych.service.test.TblTestResultFpaService;
import com.ssm.psych.service.test.TblTestResultSasService;
import com.ssm.psych.service.test.TblTestResultSdsService;
import com.ssm.psych.service.test.ViewTestPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-13 20:28
 * @Version 1.0
 */
@Controller
@RequestMapping("/question")
public class ViewTestController {
    @Autowired
    private TblTestResultFpaService testResultFpaService;
    @Autowired
    private TblTestResultSasService testResultSasService;
    @Autowired
    private TblTestResultSdsService testResultSdsService;
    @Autowired
    private ViewTestPlanService viewTestPlanService;
    @Autowired
    private QuestionFpaService questionFpaService;
    @Autowired
    private QuestionSasService questionSasService;
    @Autowired
    private QuestionSdsService questionSdsService;

    @RequestMapping("/queryTestCode")
    @ResponseBody
    public AjaxResult queryTestCode(TesterVo tester, HttpSession httpSession) {
        httpSession.setAttribute("tester", tester);
        Integer testCode = tester.getTestCode();
        return viewTestPlanService.queryAll(testCode, tester);
    }

    @RequestMapping(value = "/queryQuestion", method = RequestMethod.GET)
    public ModelAndView queryQuestion(HttpSession httpSession, ModelAndView modelAndView) {
        TesterVo tester = (TesterVo) httpSession.getAttribute("tester");
        Integer testCode = tester.getTestCode();
        String type = null;
        for (ViewTestPlan viewTestPlan : viewTestPlanService.findAll(testCode)) {
            type = viewTestPlan.getType();
        }
        if (type.equals("fpa")) {
            List<QuestionFpa> questionFpas = questionFpaService.queryAll();
            modelAndView.addObject("questionList", questionFpas);
        } else if (type.equals("sas")) {
            List<QuestionSas> questionSass = questionSasService.findAll();
            modelAndView.addObject("questionList", questionSass);
        } else if (type.equals("sds")) {
            List<QuestionSds> questionSds = questionSdsService.findAll();
            modelAndView.addObject("questionList", questionSds);
        }
        modelAndView.setViewName("testQuestion");
        return modelAndView;
    }

    @RequestMapping("/insertAll")
    @ResponseBody
    public AjaxResult insertAll(@RequestBody List<Map<String, Object>> answerList,
                                HttpSession httpSession) {
        TesterVo tester = (TesterVo) httpSession.getAttribute("tester");
        Integer testCode = tester.getTestCode();
        Integer testPlanId = 0;
        String type = null;
        for (ViewTestPlan viewTestPlan : viewTestPlanService.findAll(testCode)) {
            type = viewTestPlan.getType();
            testPlanId = viewTestPlan.getId();
        }
        tester.setTestPlanId(testPlanId);
        if (type.equals("fpa")) {
            questionFpaService.insertFpa(tester);
            Integer testerId = tester.getId();
            return testResultFpaService.saveResultFpa(answerList, testerId);
        } else if (type.equals("sas")) {
            questionSasService.insertSas(tester);
            Integer testerId = tester.getId();
            return testResultSasService.saveResultSas(answerList, testerId);
        } else if (type.equals("sds")) {
            questionSdsService.insertSds(tester);
            Integer testerId = tester.getId();
            return testResultSdsService.saveResultSds(answerList, testerId);
        } else {
            questionSdsService.insertSds(tester);
            Integer testerId = tester.getId();
            return null;
        }
    }
} 