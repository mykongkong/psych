package com.ssm.psych.controller.sds;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.ResultCountSds;
import com.ssm.psych.domain.ViewTestResultSds;
import com.ssm.psych.service.sds.ViewTestResultSdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description 3.抑郁测试-测试结果-控制层
 * @Author dpj
 * @Date 2022-12-07 19:48
 * @Version 1.0
 */
@Controller
@RequestMapping("/sds/result")
public class ViewTestResultSdsController {
    private ViewTestResultSdsService viewTestResultSdsService;

    @Autowired
    public void setViewTestResultSdsService(ViewTestResultSdsService viewTestResultSdsService) {
        this.viewTestResultSdsService = viewTestResultSdsService;
    }

    /**
     * 查询焦虑测试－测试结果的数据库所有数据
     *
     * @param page  　分页的页码
     * @param limit 　分页的每页的数据数
     * @return 返回查到的数据给前端
     */
    @RequestMapping("/queryAll")
    @ResponseBody
    public TableData<ViewTestResultSds> queryAll(Integer page, Integer limit) {
        PageInfo<ViewTestResultSds> all = viewTestResultSdsService.queryAll(page, limit);
        return new TableData<>(all.getTotal(), all.getList());
    }

    /**
     * 查询指定ID的数据
     *
     * @param id 　传入的Id
     * @return 返回查询得到的数据
     */
    @RequestMapping("/queryID")
    @ResponseBody
    public TableData<ViewTestResultSds> queryID(Integer id) {
        PageInfo<ViewTestResultSds> queryID = viewTestResultSdsService.queryID(id);
        return new TableData<>(queryID.getTotal(), queryID.getList());
    }

    /**
     * 删除数据库中指定Id的数据
     *
     * @param idList 　传入指定的ID
     * @return 返回删除的结果并返亏给前端
     */
    @RequestMapping("/delete")
    @ResponseBody
    public AjaxResult delete(@RequestBody Integer[] idList) {
        return viewTestResultSdsService.delete(idList);
    }

    /**
     * 模糊查询测试结果的数据
     *
     * @param viewTestResultSds 　传入需要查询的数据
     * @return 返回查询都得到的数据
     */
    @RequestMapping("/fuzzyQuery")
    @ResponseBody
    public TableData<ViewTestResultSds> fuzzyQuery(ViewTestResultSds viewTestResultSds) {
        PageInfo<ViewTestResultSds> fuzzyQuery =
                viewTestResultSdsService.fuzzyQuery(viewTestResultSds);
        return new TableData<>(fuzzyQuery.getTotal(), fuzzyQuery.getList());
    }

    /**
     * 报表统计查询的数据统计的不同焦虑程度的个数
     *
     * @return 将查询的数据返回给前端
     */
    @RequestMapping("/resultCount")
    @ResponseBody
    public ResultCountSds resultCount() {
        return viewTestResultSdsService.getCount();
    }
} 