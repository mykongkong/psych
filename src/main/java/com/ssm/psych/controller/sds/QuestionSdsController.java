package com.ssm.psych.controller.sds;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.QuestionSds;
import com.ssm.psych.service.sds.QuestionSdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description 2.抑郁测试-题目管理-控制层
 * @Author dpj
 * @Date 2022-11-26 10:33
 * @Version 1.0
 */
@Controller
@RequestMapping("/sds/question")
public class QuestionSdsController {
    private QuestionSdsService questionSdsService;

    @Autowired
    public void setQuestionSdsService(QuestionSdsService questionSdsService) {
        this.questionSdsService = questionSdsService;
    }

    @RequestMapping("/queryAll")
    @ResponseBody
    public TableData<QuestionSds> queryAll(Integer page, Integer limit) {
        PageInfo<QuestionSds> pageInfo = questionSdsService.queryAll(page, limit);
        return new TableData<>(pageInfo.getTotal(), pageInfo.getList());
    }

    @RequestMapping("/fuzzyQuery")
    @ResponseBody
    public TableData<QuestionSds> fuzzQuery(String question, String createBy,
                                            Integer page, Integer limit) {
        PageInfo<QuestionSds> pageInfo = questionSdsService.fuzzyQuery(question, createBy,
                                                                       page, limit);
        return new TableData<>(pageInfo.getTotal(), pageInfo.getList());
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult insert(QuestionSds questionSds) {
        return questionSdsService.insert(questionSds);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult update(QuestionSds questionSds){
        return questionSdsService.update(questionSds);
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult delete(Integer id){
        return questionSdsService.delete(id);
    }

    @RequestMapping(value = "deleteByIds", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult deleteByIds(Integer[] ids){
        return questionSdsService.deleteByIds(ids);
    }
}