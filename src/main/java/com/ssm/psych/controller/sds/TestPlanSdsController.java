package com.ssm.psych.controller.sds;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.TestPlanSds;
import com.ssm.psych.interceptor.MyAdvice;
import com.ssm.psych.service.sds.TestPlanSdsService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @Description 3.抑郁测试-测试计划-控制层
 * @Author 邓鹏杰
 * @Date 2022-12-05 10:47
 * @Version 1.0
 */
@Controller
@RequestMapping("/sds/test")
public class TestPlanSdsController {
    private TestPlanSdsService testPlanSdsService;

    @Autowired
    public void setTestPlanSdsService(TestPlanSdsService testPlanSdsService) {
        this.testPlanSdsService = testPlanSdsService;
    }

    /**
     * 查询焦虑测试－测试计划数据库的所有数据
     *
     * @param page  　分页的页码
     * @param limit 　分页的每页显示的数据数
     * @return 返回查询得到的数据
     */
    @RequestMapping("/queryAll")
    @ResponseBody
    public TableData<TestPlanSds> queryAll(Integer page, Integer limit) {
        PageInfo<TestPlanSds> pageInfo = testPlanSdsService.queryAll(page, limit);
        return new TableData<>(pageInfo.getTotal(), pageInfo.getList());
    }

    /**
     * 修改焦虑测试－测试计划数据库中指定ID的数据
     *
     * @param testPlanSds 　传入需要修改的数据
     * @return 返回修改结果的数据
     */
    @RequestMapping("/update")
    @ResponseBody
    public AjaxResult update(TestPlanSds testPlanSds, HttpServletRequest request) {
        MyAdvice myAdvice = new MyAdvice();
        String userName = myAdvice.getUserName(request);
        testPlanSds.setUpdateBy(userName);
        return testPlanSdsService.update(testPlanSds);
    }

    /**
     * 添加指定改的数据到焦虑测试－测试计划的数据
     *
     * @param testPlanSds 　需要添加的数据
     * @return 返回添加成功的结果数据
     */
    @RequestMapping("/insert")
    @ResponseBody
    public AjaxResult insert(TestPlanSds testPlanSds, HttpServletRequest request) {
        MyAdvice myAdvice = new MyAdvice();
        String userName = myAdvice.getUserName(request);
        testPlanSds.setCreateBy(userName);
        testPlanSds.setUpdateBy(userName);
        Integer code = Integer.valueOf(RandomStringUtils.randomNumeric(6));
        testPlanSds.setTestCode(code);
        return testPlanSdsService.insert(testPlanSds);
    }

    /**
     * 　删除指定给ID的数据
     *
     * @param idList 传入指定的ID
     * @return 返回删除数据结果的数据
     */
    @RequestMapping("/delete")
    @ResponseBody
    public AjaxResult delete(@RequestBody Integer[] idList) {
        return testPlanSdsService.delete(idList);
    }

    /**
     * 模糊查询焦虑测试-测试计划数据库中的数据
     *
     * @param testName 传入查询的测试计划的名字
     * @param createBy 传入需要查询的创建者名字
     * @return 返回查询得到的数据
     */
    @RequestMapping("/fuzzyQuery")
    @ResponseBody
    public TableData<TestPlanSds> fuzzyQuery(String testName, String createBy) {
        PageInfo<TestPlanSds> query = testPlanSdsService.fuzzyQuery(testName, createBy);
        return new TableData<>(query.getTotal(), query.getList());
    }

    @RequestMapping("/proofDate")
    @ResponseBody
    public AjaxResult proofDate(TestPlanSds testPlanSds) {
        Date testEnd = testPlanSds.getTestEnd();
        AjaxResult ajaxResult = new AjaxResult();
        if (new Date().after(testEnd)) {
            ajaxResult.setStatus(true);
            ajaxResult.setMessage("截止日期不能选在今天之前！");
            return ajaxResult;
        } else {
            ajaxResult.setStatus(false);
            return ajaxResult;
        }
    }
} 