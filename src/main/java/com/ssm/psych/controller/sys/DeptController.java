package com.ssm.psych.controller.sys;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.Dept;
import com.ssm.psych.service.sys.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description 4.系统管理-部门管理-控制层
 * @Author dpj
 * @Date 2022-11-26 07:53
 * @Version 1.0
 */
@Controller
@RequestMapping("/sys/dept")
public class DeptController {
    private DeptService deptService;

    @Autowired
    public void setDeptService(DeptService deptService) {
        this.deptService = deptService;
    }

    @RequestMapping("/queryAll")
    @ResponseBody
    public TableData<Dept> queryAll(Integer page, Integer limit) {
        PageInfo<Dept> pageInfo = deptService.queryAll(page, limit);
        return new TableData<>(pageInfo.getTotal(), pageInfo.getList());
    }

    @RequestMapping("/fuzzyQuery")
    @ResponseBody
    public TableData<Dept> fuzzyQuery(String deptName,
                                      String createBy, Integer page, Integer limit) {
        PageInfo<Dept> pageInfo = deptService.fuzzyQuery(deptName, createBy, page, limit);
        return new TableData<>(pageInfo.getTotal(), pageInfo.getList());
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult insert(Dept dept) {
        return deptService.insert(dept);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult update(Dept dept) {
        return deptService.update(dept);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult delete(Integer deptId) {
        return deptService.delete(deptId);
    }

    @RequestMapping(value = "/deleteByIds", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult deleteByIds(Integer[] deptIds) {
        return deptService.deleteByIds(deptIds);
    }
}