package com.ssm.psych.controller.sys;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.Dept;
import com.ssm.psych.domain.Role;
import com.ssm.psych.domain.User;
import com.ssm.psych.service.sys.UserService;
import com.ssm.psych.utils.OSSUtil;
import com.ssm.psych.utils.UserConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Description 4.系统管理-用户管理-控制层
 * @Author dpj
 * @Date 2022-12-06 17:46
 * @Version 1.0
 */
@Controller
@RequestMapping("/sys/user")
public class UserController {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/hasAccount")
    @ResponseBody
    public AjaxResult hasAccount(String account, Integer accountType) {
        return userService.hasAccount(account, accountType);
    }

    @RequestMapping("/accountDuplicate")
    @ResponseBody
    public AjaxResult accountDuplicate(String account, Integer accountType, Integer userid) {
        return userService.accountDuplicate(account, accountType, userid);
    }

    @RequestMapping(value = "/queryByName", method = RequestMethod.POST)
    @ResponseBody
    public User queryByName(String username){
        return userService.queryByName(username);
    }

    @RequestMapping("/queryAll")
    @ResponseBody
    public TableData<User> queryAll(Integer page, Integer limit) {
        PageInfo<User> pageInfo = userService.queryAll(page, limit);
        return new TableData<>(pageInfo.getTotal(), pageInfo.getList());
    }

    @RequestMapping("/fuzzyQuery")
    @ResponseBody
    public TableData<User> fuzzyQuery(Integer userid, String username, Integer page,
                                      Integer limit) {
        PageInfo<User> pageInfo = userService.fuzzyQuery(userid, username, page, limit);
        return new TableData<>(pageInfo.getTotal(), pageInfo.getList());
    }

    @RequestMapping("/queryDeptList")
    @ResponseBody
    public List<Dept> queryDeptList() {
        return userService.queryDeptList();
    }

    @RequestMapping("/queryRoleList")
    @ResponseBody
    public List<Role> queryRoleList() {
        return userService.queryRoleList();
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult insert(User user) {
        return userService.insert(user);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult update(User user) {
        return userService.update(user);
    }

    @RequestMapping(value = "/resetPwd", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult resetPwd(String username, String pwdSignin){
        return userService.resetPwd(username, pwdSignin);
    }

    @RequestMapping(value = "/modifyPwd", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult modifyPwd(String account, String oldPwd, String newPwd){
        return userService.modifyPwd(account, oldPwd, newPwd);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult delete(Integer userid) {
        return userService.delete(userid);
    }

    @RequestMapping(value = "/deleteByIds", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult deleteByIds(Integer[] userIds) {
        return userService.deleteByIds(userIds);
    }

    @RequestMapping(value = "/uploadAvatar", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult uploadAvatar(MultipartFile multipartFile, HttpSession session){
        AjaxResult result = new AjaxResult();
        String avatarImgUrl = OSSUtil.uploadAvatarImg(multipartFile);
        result.setStatus(true);
        result.setMessage(avatarImgUrl);
        session.setAttribute(UserConstants.SESSION_AVATAR, avatarImgUrl);
        return result;
    }
}