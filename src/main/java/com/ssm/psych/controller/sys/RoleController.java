package com.ssm.psych.controller.sys;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.Role;
import com.ssm.psych.service.sys.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Description 4.系统管理-角色管理-控制层
 * @Author dpj
 * @Date 2022-12-08 17:37
 * @Version 1.0
 */
@Controller
@RequestMapping("/sys/role")
public class RoleController {
    private RoleService roleService;

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @RequestMapping("/queryAll")
    @ResponseBody
    public TableData<Role> queryAll(Integer page, Integer limit) {
        PageInfo<Role> pageInfo = roleService.queryAll(page, limit);
        return new TableData<>(pageInfo.getTotal(), pageInfo.getList());
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult insert(Role role, List<Integer> insertMenuList) {
        AjaxResult result = roleService.insert(role);
        for (Integer menuId : insertMenuList) {
            roleService.insertRoleMenu(role.getRoleId(), menuId);
        }
        return result;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult update(Role role, List<Integer> updateMenuList) {
        AjaxResult result = roleService.update(role);
        // 0.获取修改前选中的菜单列表
        List<Integer> selectedMenuList = roleService.querySelectedMenuList(role.getRoleId());
        // 1.插入新选中的菜单
        List<Integer> insertMenuList = updateMenuList;
        insertMenuList.removeAll(selectedMenuList);
        for (Integer menuId : insertMenuList) {
            roleService.insertRoleMenu(role.getRoleId(), menuId);
        }
        // 2.删除取消选中的菜单
        List<Integer> deleteMenuList = selectedMenuList;
        deleteMenuList.removeAll(updateMenuList);
        for (Integer menuId : deleteMenuList) {
            roleService.deleteRoleMenu(role.getRoleId(), menuId);
        }
        return result;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult delete(Integer roleId, List<Integer> deleteMenuList) {
        AjaxResult result = new AjaxResult();
        for (Integer menuId : deleteMenuList) {
            roleService.deleteRoleMenu(roleId, menuId);
        }
        result.setStatus(true);
        return result;
    }

    @RequestMapping(value = "/querySelectedMenuList", method = RequestMethod.GET)
    @ResponseBody
    public List<Integer> querySelectedMenuList(Integer roleId){
        return roleService.querySelectedMenuList(roleId);
    }
}