package com.ssm.psych.controller.sys;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.MenuTree;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.Menu;
import com.ssm.psych.service.sys.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Description 4.系统管理-菜单管理-控制层
 * @Author dpj
 * @Date 2022-12-27 22:44
 * @Version 1.0
 */
@Controller
@RequestMapping("/sys/menu")
public class MenuController {
    private MenuService menuService;

    @Autowired
    public void setMenuService(MenuService menuService) {
        this.menuService = menuService;
    }

    @RequestMapping(value = "/queryAll")
    @ResponseBody
    public TableData<Menu> queryAll() {
        List<Menu> menuList = menuService.queryAll();
        return new TableData<>(menuList);
    }

    @RequestMapping(value = "/queryAllAsTree")
    @ResponseBody
    public List<MenuTree> queryAllAsTree() {
        List<MenuTree> result = new ArrayList<>();
        List<Menu> menuList = menuService.queryAll();
        int len = menuList.size();
        for (Menu menu : menuList) {
            if (menu.getParentId() == 0) {
                MenuTree level1Node = new MenuTree();
                level1Node.setId(menu.getMenuId());
                level1Node.setTitle(menu.getMenuName());
                result.add(level1Node);
                List<Menu> menuList1 = menuService.queryListByParentId(menu.getMenuId());
                ArrayList<MenuTree> menuTreeList1 = new ArrayList<>();
                level1Node.setChildren(menuTreeList1);
                len--;
                for (Menu menu1 : menuList1) {
                    MenuTree level2Node = new MenuTree();
                    level2Node.setId(menu1.getMenuId());
                    level2Node.setTitle(menu1.getMenuName());
                    menuTreeList1.add(level2Node);
                    List<Menu> menuList2 = menuService.queryListByParentId(menu1.getMenuId());
                    ArrayList<MenuTree> menuTreeList2 = new ArrayList<>();
                    level2Node.setChildren(menuTreeList2);
                    len--;
                    for (Menu menu2 : menuList2) {
                        MenuTree level3Node = new MenuTree();
                        level3Node.setId(menu2.getMenuId());
                        level3Node.setTitle(menu2.getMenuName());
                        menuTreeList2.add(level3Node);
                        len--;
                    }
                }
            }
        }
        return result;
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult insert(Menu menu) {
        return menuService.insert(menu);
    }
}