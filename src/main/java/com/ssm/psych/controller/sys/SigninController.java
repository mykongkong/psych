package com.ssm.psych.controller.sys;

import com.ssm.psych.domain.User;
import com.ssm.psych.service.sys.UserService;
import com.ssm.psych.utils.UserConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.Objects;

/**
 * @Description 登录功能控制层
 * @Author dpj
 * @Date 2022-12-13 09:23
 * @Version 1.0
 */
@Controller
public class SigninController {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    @ResponseBody
    public String signin(String account, String pwdSignin, String rememberMe, Model model,
                         HttpSession session, HttpServletRequest request,
                         HttpServletResponse response) {
        User user = userService.signin(account, pwdSignin);
        if (user == null) {
            model.addAttribute("message", "帐号或密码错误！");
            return "0";
        }
        model.addAttribute("message", "登录成功！");
        session.setAttribute(UserConstants.SESSION_USER, user.getUsername());
        session.setAttribute(UserConstants.SESSION_AVATAR, user.getAvatar());
        System.out.println("rememberMe---->" + rememberMe);
        try {
            isMemory(account, pwdSignin, rememberMe, request, response);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "1";
    }

    @RequestMapping(value = "/signout")
    @ResponseBody
    public String signout(HttpSession session) {
        session.invalidate();
        return "1";
    }

    /**
     * 添加cookie，用来记住帐户和密码
     *
     * @param account    当前登录帐户
     * @param pwdSignin  登录密码
     * @param rememberMe 是否记住帐户和密码
     * @param request    请求对象
     * @param response   响应对象
     */
    public void isMemory(String account, String pwdSignin, String rememberMe,
                         HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        if (Objects.equals(rememberMe, "true")) {
            // 创建cookie对象
            Cookie accountCookie = new Cookie("accountCookie", account);
            accountCookie.setPath("/");
            Cookie pwdCookie = new Cookie("pwdCookie", pwdSignin);
            pwdCookie.setPath("/");

            // 设置cookie有效期为30天，单位为秒
            accountCookie.setMaxAge((60 * 60 * 24) * 30);
            pwdCookie.setMaxAge((60 * 60 * 24) * 30);

            // 将cookie返回给客户端
            response.addCookie(accountCookie);
            response.addCookie(pwdCookie);
        } else {
            // 如果没有选择记住密码，则删除客户端的帐户和密码cookie
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("accountCookie") || cookie.getName().equals(
                            "pwdCookie")) {
                        System.out.println("删除的cookie---->" + cookie.getName());
                        cookie.setMaxAge(0);
                        cookie.setPath("/");
                        response.addCookie(cookie);
                    }
                }
            }
        }
    }
}