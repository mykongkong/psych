package com.ssm.psych.controller.log;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.Log;
import com.ssm.psych.service.log.LogDateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description 日志管理-控制层
 * @Author 情雅圣
 * @Date 2022-12-09 17:04
 * @Version 1.0
 */
@Controller
@RequestMapping("/log/date")
public class LogDateController {
    @Autowired
    private LogDateService logDateService;

    /**
     * 查询日志数据库中所有的数据并返回给前端
     * @param page 分页的页码
     * @param limit 分页的每页显示的数据数
     * @return 将查询的数据返回给前端
     */
    @RequestMapping("/findAll")
    @ResponseBody
    public TableData<Log> findAll(Integer page,Integer limit){
        PageInfo<Log> all = logDateService.findAll(page, limit);
        return new TableData<>(all.getTotal(),all.getList());
    }
} 
