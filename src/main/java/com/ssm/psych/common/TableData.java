package com.ssm.psych.common;

import java.util.List;

/**
 * @Description 封装数据库查询数据
 * @Author dpj
 * @Date 2022-11-28 10:58
 * @Version 1.0
 */
public class TableData<T> {
    private int code;
    private String msg = "";
    private long count;
    private List<T> data;

    public TableData() {
    }

    public TableData(List<T> data) {
        super();
        this.count = data.size();
        this.data = data;
    }

    public TableData(long count, List<T> data) {
        this.count = count;
        this.data = data;
    }

    @Override
    public String toString() {
        return "TableData{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", count=" + count +
                ", data=" + data +
                '}';
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}