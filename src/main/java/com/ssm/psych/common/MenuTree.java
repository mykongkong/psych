package com.ssm.psych.common;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 树形菜单工具类
 * @Author dpj
 * @Date 2022-12-28 01:24
 * @Version 1.0
 */
public class MenuTree {
    private Integer id;
    private String title;
    private List<MenuTree> children = new ArrayList<>();

    public MenuTree() {
    }

    public MenuTree(Integer id, String title, List<MenuTree> children) {
        this.id = id;
        this.title = title;
        this.children = children;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<MenuTree> getChildren() {
        return children;
    }

    public void setChildren(List<MenuTree> children) {
        this.children = children;
    }
}