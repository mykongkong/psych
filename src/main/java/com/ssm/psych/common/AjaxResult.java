package com.ssm.psych.common;

/**
 * @Description ajax返回状态工具类
 * @Author dpj
 * @Date 2022-11-28 09:36
 * @Version 1.0
 */
public class AjaxResult {
    // 操作是否成功
    private boolean status;
    // 网页提示信息
    private String message;
    // 操作成功结果
    private Object result;

    public AjaxResult() {
    }

    public AjaxResult(boolean status, String message, Object result) {
        this.status = status;
        this.message = message;
        this.result = result;
    }

    @Override
    public String toString() {
        return "AjaxResult{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", result=" + result +
                '}';
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}