package com.ssm.psych.dao.sas;

import com.ssm.psych.domain.ViewTestResultSas;

import java.util.List;

/**
 * @author ：
 * @date ：Created in 2022/12/7 17:55
 * @description：
 * @modified By：
 * @version:
 */

public interface ViewTestResultSasDao {
    //查询所有数据
    List<ViewTestResultSas> queryAll();
    //通过ID查数据
    List<ViewTestResultSas> queryID(Integer id);
    //删除数据
    int delete(Integer[] idList);
    List<ViewTestResultSas> fuzzyQuery(ViewTestResultSas viewTestResultSas);
}
