package com.ssm.psych.dao.sas;

import com.ssm.psych.domain.TestPlanSas;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author ：
 * @date ：Created in 2022/12/5 10:26
 * @description：
 * @modified By：
 * @version:
 */

public interface TestPlanSasDao {
    //查询数据
    List<TestPlanSas> queryAll();
    //修改数据
    int update(TestPlanSas testPlanSas);
    //添加数据
    int insert(TestPlanSas testPlanSas);
    //删除数据
    int delete(Integer[] idList);
    //模糊查询
    List<TestPlanSas> fuzzyQuery(@Param("testName")String testName,@Param("createBy") String createBy);
}
