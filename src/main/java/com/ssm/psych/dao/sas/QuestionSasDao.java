package com.ssm.psych.dao.sas;

import com.ssm.psych.domain.QuestionFpa;
import com.ssm.psych.domain.QuestionSas;

import java.util.List;

public interface QuestionSasDao {
    QuestionSas queryById(Integer id);
    //查询所有数据
    List<QuestionSas> queryAll();
    //修改数据
    int update(QuestionSas questionSas);
    //添加数据
    int insert(QuestionSas questionSas);
    //模糊查询
    List<QuestionSas> fuzzyQuery(QuestionSas questionSas);
    //删除功能
    int delete(Integer[] idList);
}