package com.ssm.psych.dao.sds;

import com.ssm.psych.domain.QuestionSds;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface QuestionSdsDao {
    QuestionSds queryById(Integer id);

    QuestionSds queryByQuestion(String question);

    List<QuestionSds> queryAll();

    List<QuestionSds> fuzzyQuery(@Param("question") String question,
                                 @Param("createBy") String createBy);

    void insert(QuestionSds questionSds);

    void update(QuestionSds questionSds);

    void delete(Integer id);

    void deleteByIds(@Param("ids") Integer[] ids);
}