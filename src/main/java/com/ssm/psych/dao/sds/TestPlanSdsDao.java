package com.ssm.psych.dao.sds;

import com.ssm.psych.domain.TestPlanSds;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author ：dpj
 * @Date ：Created in 2022/12/5 10:26
 * @Description 3.抑郁测试SDS-测试计划-DAO层接口
 */

public interface TestPlanSdsDao {
    //查询数据
    List<TestPlanSds> queryAll();
    //修改数据
    int update(TestPlanSds testPlanSds);
    //添加数据
    int insert(TestPlanSds testPlanSds);
    //删除数据
    int delete(Integer[] idList);
    //模糊查询
    List<TestPlanSds> fuzzyQuery(@Param("testName")String testName,
                                 @Param("createBy") String createBy);
}