package com.ssm.psych.dao.sds;

import com.ssm.psych.domain.ViewTestResultSds;

import java.util.List;

/**
 * @author ：
 * @Date ：Created in 2022/12/7 17:55
 * @Description 3.抑郁测试-测试结果-DAO层接口
 */
public interface ViewTestResultSdsDao {
    // 查询所有数据
    List<ViewTestResultSds> queryAll();

    // 通过ID查数据
    List<ViewTestResultSds> queryID(Integer id);

    // 删除数据
    int delete(Integer[] idList);

    List<ViewTestResultSds> fuzzyQuery(ViewTestResultSds viewTestResultSds);
}