package com.ssm.psych.dao.sys;

import com.ssm.psych.domain.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleDao {
    Role queryById(@Param("roleId") Integer roleId);

    Role queryByName(@Param("roleName") String roleName);

    List<Role> queryAll();

    void insert(Role role);

    void update(Role role);

    void delete(@Param("roleId") Integer roleId);

    void insertRoleMenu(@Param("roleId") Integer roleId, @Param("menuId") Integer menuId);

    void deleteRoleMenu(@Param("roleId") Integer roleId, @Param("menuId") Integer menuId);

    List<Integer> querySelectedMenuList(@Param("roleId") Integer roleId);
}