package com.ssm.psych.dao.sys;

import com.ssm.psych.domain.Dept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DeptDao {
    Dept queryById(@Param("deptId") Integer deptId);
    Dept queryByName(@Param("deptName") String deptName);

    List<Dept> queryAll();

    List<Dept> fuzzyQuery(@Param("deptName") String deptName, @Param("createBy") String createBy);

    void insert(Dept dept);

    void update(Dept dept);

    void delete(Integer deptId);

    void deleteByIds(@Param("deptIds") Integer[] deptIds);
}