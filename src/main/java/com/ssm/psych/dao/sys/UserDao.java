package com.ssm.psych.dao.sys;

import com.ssm.psych.domain.Dept;
import com.ssm.psych.domain.Role;
import com.ssm.psych.domain.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {
    User queryById(@Param("userid") Integer userid);

    User queryByName(@Param("username") String username);

    User queryByAccount(@Param("account") String account);

    List<User> queryAll();

    List<User> fuzzyQuery(@Param("userid") Integer userid, @Param("username") String username);

    List<Dept> queryDeptList();

    List<Role> queryRoleList();

    void insert(User user);

    void update(User user);

    void delete(@Param("userid") Integer userid);

    void deleteByIds(@Param("userIds") Integer[] userIds);

    void resetPwd(@Param("username") String username, @Param("password") String password, @Param(
            "salt") String salt);
}