package com.ssm.psych.dao.sys;

import com.ssm.psych.domain.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MenuDao {
    Menu queryById(@Param("menuId") Integer menuId);

    Menu queryByName(@Param("menuName") String menuName);

    List<Menu> queryAll();

    List<Menu> queryListByParentId(@Param("parentId") Integer parentId);

    void insert(Menu menu);

    void update(Menu menu);
}