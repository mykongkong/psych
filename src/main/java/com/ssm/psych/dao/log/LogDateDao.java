package com.ssm.psych.dao.log;

import com.ssm.psych.domain.Log;

import java.util.List;

/**
 * @author ：
 * @date ：Created in 2022/12/9 16:47
 * @description：
 * @modified By：
 * @version:
 */

public interface LogDateDao {
    List<Log> findAll();
    void insert(Log log);
}
