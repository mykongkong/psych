package com.ssm.psych.dao.test;

import com.ssm.psych.domain.TesterVo;

import java.util.List;

/**
 * @author ：
 * @date ：Created in 2022/12/15 20:20
 * @description：
 * @modified By：
 * @version:
 */

public interface TblTesterDao {
    void insertFpa(TesterVo tester);

    void insertSas(TesterVo tester);

    void insertSds(TesterVo testerVo);

    List<TesterVo> queryPhoneFpa(TesterVo testerVo);

    List<TesterVo> queryPhoneSas(TesterVo testerVo);

    List<TesterVo> queryPhoneSds(TesterVo testerVo);
}