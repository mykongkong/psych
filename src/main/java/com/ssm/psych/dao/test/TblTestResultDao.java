package com.ssm.psych.dao.test;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author ：
 * @date ：Created in 2022/12/16 9:43
 * @description：
 * @modified By：
 * @version:
 */

public interface TblTestResultDao {
    int insertResultFpa(@Param("entities") List<Map<String, Object>> answer,@Param("testerId") Integer testerId);
    int insertResultSas(@Param("entities") List<Map<String, Object>> answer,@Param("testerId") Integer testerId);
    int insertResultSds(@Param("entities") List<Map<String, Object>> answer,@Param("testerId") Integer testerId);
}
