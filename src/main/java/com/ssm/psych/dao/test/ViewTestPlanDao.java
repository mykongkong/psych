package com.ssm.psych.dao.test;

import com.ssm.psych.domain.ViewTestPlan;

import java.util.List;

/**
 * @author ：
 * @date ：Created in 2022/12/13 13:45
 * @description：
 * @modified By：
 * @version:
 */

public interface ViewTestPlanDao {
    List<ViewTestPlan> findAll(Integer testCode);
}
