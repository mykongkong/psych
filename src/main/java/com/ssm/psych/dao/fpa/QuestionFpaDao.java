package com.ssm.psych.dao.fpa;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.QuestionFpa;

import java.util.List;

public interface QuestionFpaDao {
    //查找所有数据
    List<QuestionFpa> findAll();
    //添加数据
    void add(QuestionFpa questionFpa);
    //删除指定行的数据
    int deleteId(int id);
    //修改指定行的数据
    int update(QuestionFpa questionFpa);
    //模糊查询数据
    List<QuestionFpa> fuzzyQuery(QuestionFpa questionFpa);
    //批量删除
    void deleteByIds(Integer[] idList);
}