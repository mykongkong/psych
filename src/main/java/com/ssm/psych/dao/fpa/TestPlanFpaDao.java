package com.ssm.psych.dao.fpa;

import com.ssm.psych.domain.TestPlanFpa;

import java.util.List;

/**
 * @author ：情雅圣
 * @date ：Created in 2022/12/1 11:28
 * @Description：性格测试-测试计划-DAO层
 * @version:1.0
 */

public interface TestPlanFpaDao {
    //查询所有数据
    List<TestPlanFpa> queryAll();
    //添加数据
    int insert(TestPlanFpa testPlanFpa);
    //修改数据
    int update(TestPlanFpa testPlanFpa);
    //删除数据
    int deleteByIds(Integer[] idArray);
    //模糊查询数据
    List<TestPlanFpa> fuzzyQuery(TestPlanFpa testPlanFpa);
}
