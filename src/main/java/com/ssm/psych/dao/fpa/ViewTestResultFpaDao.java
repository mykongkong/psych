package com.ssm.psych.dao.fpa;

import com.ssm.psych.domain.TblResultDiv;
import com.ssm.psych.domain.ViewTestResultFpa;

import java.util.List;

/**
 * @author ：情雅圣
 * @date ：Created in 2022/12/5 15:34
 * @description：性格测试-测试结果-DAO层
 * @version: 1.0
 */

public interface ViewTestResultFpaDao {
    //查询数据
    List<ViewTestResultFpa> queryAll();
    //删除数据
    int delete(Integer[] idList);
    //模糊查询
    List<ViewTestResultFpa> fuzzyQuery(ViewTestResultFpa viewTestResultFpa);
    //通过ID查数据
    List<ViewTestResultFpa> queryID(Integer testerId);
    //查Div数据
    List<TblResultDiv> queryDiv(Integer id);
}
