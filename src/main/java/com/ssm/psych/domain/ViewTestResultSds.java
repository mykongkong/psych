package com.ssm.psych.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Description 3.抑郁测试-测试结果-实体类
 * @Author dpj
 * @Date 2022-12-07 17:46
 * @Version 1.0
 */
public class ViewTestResultSds {
    private Integer id;
    private String name;
    private String phone;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date create_time;
    private Integer forward;
    private Integer inversion;
    private Integer testPlanId;

    public ViewTestResultSds() {
    }

    public ViewTestResultSds(Integer id, String name, String phone, Date create_time, Integer forward, Integer inversion, Integer testPlanId) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.create_time = create_time;
        this.forward = forward;
        this.inversion = inversion;
        this.testPlanId = testPlanId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreateTime() {
        return create_time;
    }

    public void setCreateTime(Date create_time) {
        this.create_time = create_time;
    }

    public Integer getForward() {
        return forward;
    }

    public void setForward(Integer forward) {
        this.forward = forward;
    }

    public Integer getInversion() {
        return inversion;
    }

    public void setInversion(Integer inversion) {
        this.inversion = inversion;
    }

    public Integer getTestPlanId() {
        return testPlanId;
    }

    public void setTestPlanId(Integer testPlanId) {
        this.testPlanId = testPlanId;
    }

    @Override
    public String toString() {
        return "ViewTestResultSds{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", createTime=" + create_time +
                ", forward=" + forward +
                ", inversion=" + inversion +
                ", testPlanId=" + testPlanId +
                '}';
    }
}