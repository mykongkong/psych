package com.ssm.psych.domain;

/**
 * @Description 3.抑郁测试-测试结果
 * @Author dpj
 * @Date 2022-12-08 09:47
 * @Version 1.0
 */
public class ResultCountSds {
    private Integer normal;
    private Integer mild;
    private Integer middle;
    private Integer severe;

    public ResultCountSds(Integer normal, Integer mild, Integer middle, Integer severe) {
        this.normal = normal;
        this.mild = mild;
        this.middle = middle;
        this.severe = severe;
    }

    public ResultCountSds() {
    }

    public Integer getNormal() {
        return normal;
    }

    public void setNormal(Integer normal) {
        this.normal = normal;
    }

    public Integer getMild() {
        return mild;
    }

    public void setMild(Integer mild) {
        this.mild = mild;
    }

    public Integer getMiddle() {
        return middle;
    }

    public void setMiddle(Integer middle) {
        this.middle = middle;
    }

    public Integer getSevere() {
        return severe;
    }

    public void setSevere(Integer severe) {
        this.severe = severe;
    }

    @Override
    public String toString() {
        return "ResultCountSds{" +
                "normal=" + normal +
                ", mild=" + mild +
                ", middle=" + middle +
                ", severe=" + severe +
                '}';
    }
}