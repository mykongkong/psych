package com.ssm.psych.domain;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-07 16:04
 * @Version 1.0
 */
public class ResultCountFpa {
    private Integer r;
    private Integer b;
    private Integer y;
    private Integer g;

    private Integer h;
    public ResultCountFpa() {
    }

    public ResultCountFpa(Integer r, Integer b, Integer y, Integer g, Integer h) {
        this.r = r;
        this.b = b;
        this.y = y;
        this.g = g;
        this.h = h;
    }

    public Integer getH() {
        return h;
    }

    public void setH(Integer h) {
        this.h = h;
    }

    public Integer getR() {
        return r;
    }

    public void setR(Integer r) {
        this.r = r;
    }

    public Integer getB() {
        return b;
    }

    public void setB(Integer b) {
        this.b = b;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getG() {
        return g;
    }

    public void setG(Integer g) {
        this.g = g;
    }

    @Override
    public String toString() {
        return "ResultCountFpa{" +
                "r=" + r +
                ", b=" + b +
                ", y=" + y +
                ", g=" + g +
                ", h=" + h +
                '}';
    }
}
