package com.ssm.psych.domain;

import java.util.Date;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-14 10:38
 * @Version 1.0
 */
public class TesterVo {
    private Integer id;
    private String name;
    private String phone;
    private Date createTime;
    private Integer testPlanId;
    private Integer testCode;
    private String testName;
    private String type;

    public TesterVo() {
    }

    public TesterVo(Integer id, String name, String phone, Date createTime, Integer testPlanId,
                    Integer testCode, String testName, String type) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.createTime = createTime;
        this.testPlanId = testPlanId;
        this.testCode = testCode;
        this.testName = testName;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getTestPlanId() {
        return testPlanId;
    }

    public void setTestPlanId(Integer testPlanId) {
        this.testPlanId = testPlanId;
    }

    public Integer getTestCode() {
        return testCode;
    }

    public void setTestCode(Integer testCode) {
        this.testCode = testCode;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "TesterVo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", createTime=" + createTime +
                ", testPlanId=" + testPlanId +
                ", testCode=" + testCode +
                ", testName='" + testName + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}