package com.ssm.psych.domain;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-08 09:47
 * @Version 1.0
 */
public class ResultCountSas {
    private Integer normal;
    private Integer mild;
    private Integer middle;
    private Integer severe;

    public ResultCountSas(Integer normal, Integer mild, Integer middle, Integer severe) {
        this.normal = normal;
        this.mild = mild;
        this.middle = middle;
        this.severe = severe;
    }

    public ResultCountSas() {
    }

    public Integer getNormal() {
        return normal;
    }

    public void setNormal(Integer normal) {
        this.normal = normal;
    }

    public Integer getMild() {
        return mild;
    }

    public void setMild(Integer mild) {
        this.mild = mild;
    }

    public Integer getMiddle() {
        return middle;
    }

    public void setMiddle(Integer middle) {
        this.middle = middle;
    }

    public Integer getSevere() {
        return severe;
    }

    public void setSevere(Integer severe) {
        this.severe = severe;
    }

    @Override
    public String toString() {
        return "ResultCountSas{" +
                "normal=" + normal +
                ", mild=" + mild +
                ", middle=" + middle +
                ", severe=" + severe +
                '}';
    }
}
