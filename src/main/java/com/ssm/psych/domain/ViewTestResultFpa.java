package com.ssm.psych.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-05 15:21
 * @Version 1.0
 */
public class ViewTestResultFpa {
    private Integer testerId;
    private Integer redCount;
    private Integer blueCount;
    private Integer yellowCount;
    private Integer greenCount;
    private String phone;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private Integer testPlanId;

    public ViewTestResultFpa() {
    }

    public ViewTestResultFpa(Integer testerId, Integer redCount, Integer blueCount, Integer yellowCount, Integer greenCount, String phone, String name, Date createTime, Integer testPlanId) {
        this.testerId = testerId;
        this.redCount = redCount;
        this.blueCount = blueCount;
        this.yellowCount = yellowCount;
        this.greenCount = greenCount;
        this.phone = phone;
        this.name = name;
        this.createTime = createTime;
        this.testPlanId = testPlanId;
    }

    public Integer getTesterId() {
        return testerId;
    }

    public void setTesterId(Integer testerId) {
        this.testerId = testerId;
    }

    public Integer getRedCount() {
        return redCount;
    }

    public void setRedCount(Integer redCount) {
        this.redCount = redCount;
    }

    public Integer getBlueCount() {
        return blueCount;
    }

    public void setBlueCount(Integer blueCount) {
        this.blueCount = blueCount;
    }

    public Integer getYellowCount() {
        return yellowCount;
    }

    public void setYellowCount(Integer yellowCount) {
        this.yellowCount = yellowCount;
    }

    public Integer getGreenCount() {
        return greenCount;
    }

    public void setGreenCount(Integer greenCount) {
        this.greenCount = greenCount;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getTestPlanId() {
        return testPlanId;
    }

    public void setTestPlanId(Integer testPlanId) {
        this.testPlanId = testPlanId;
    }

    @Override
    public String toString() {
        return "ViewTestResultFpa{" +
                "testerId=" + testerId +
                ", redCount=" + redCount +
                ", blueCount=" + blueCount +
                ", yellowCount=" + yellowCount +
                ", greenCount=" + greenCount +
                ", phone='" + phone + '\'' +
                ", name='" + name + '\'' +
                ", createTime=" + createTime +
                ", testPlanId=" + testPlanId +
                '}';
    }
}
