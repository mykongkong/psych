package com.ssm.psych.domain;

import java.util.Date;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-13 19:19
 * @Version 1.0
 */
public class ViewTestPlan {
    private Integer id;
    private String testName;
    private Integer testCode;
    private Date testBegin;
    private Date testEnd;
    private Date createTime;
    private String createBy;
    private Date updateTime;
    private String updateBy;
    private String remark;
    private String type;

    public ViewTestPlan() {
    }

    public ViewTestPlan(Integer id, String testName, Integer testCode, Date testBegin, Date testEnd, Date createTime, String createBy, Date updateTime, String updateBy, String remark, String type) {
        this.id = id;
        this.testName = testName;
        this.testCode = testCode;
        this.testBegin = testBegin;
        this.testEnd = testEnd;
        this.createTime = createTime;
        this.createBy = createBy;
        this.updateTime = updateTime;
        this.updateBy = updateBy;
        this.remark = remark;
        this.type = type;
    }

    public Integer getTestCode() {
        return testCode;
    }

    public void setTestCode(Integer testCode) {
        this.testCode = testCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public Date getTestBegin() {
        return testBegin;
    }

    public void setTestBegin(Date testBegin) {
        this.testBegin = testBegin;
    }

    public Date getTestEnd() {
        return testEnd;
    }

    public void setTestEnd(Date testEnd) {
        this.testEnd = testEnd;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ViewTestPlan{" +
                "id=" + id +
                ", testName='" + testName + '\'' +
                ", testCode=" + testCode +
                ", testBegin=" + testBegin +
                ", testEnd=" + testEnd +
                ", createTime=" + createTime +
                ", createBy='" + createBy + '\'' +
                ", updateTime=" + updateTime +
                ", updateBy='" + updateBy + '\'' +
                ", remark='" + remark + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
