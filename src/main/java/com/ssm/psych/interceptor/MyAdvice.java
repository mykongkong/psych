package com.ssm.psych.interceptor;

import com.ssm.psych.domain.Log;
import com.ssm.psych.service.log.LogDateService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-09 00:03
 * @Version 1.0
 */
@Aspect
public class MyAdvice {
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private LogDateService logDateService;
    @Before("execution(* com.ssm.psych.controller..*.*(..))")
    public void before(JoinPoint jp) throws ClassNotFoundException, NoSuchMethodException {
        Class<?> clazz = jp.getTarget().getClass();//获取访问的类
        String name1 = jp.getTarget().getClass().getName();//获取访问类的名称
        String name = jp.getSignature().getName();//获取访问方法的名称
        Class[] paramTypes=null;//获取的方法参数类型存入类数组中
        String[] params=null;
        Object[] array= jp.getArgs();//获取访问方法的参数值
        String arrays=Arrays.toString(array);
        Method[] methods = clazz.getMethods();//获取类中所有公共的成员方法
        for(int i=0;i<methods.length;i++){
            if(name.equals(methods[i].getName())){
                Class[] param=methods[i].getParameterTypes();//获取所有方法参数的类型并放入类数组中
                paramTypes=new Class[param.length];
                params=new String[param.length];
                for(int j=0;j< params.length;j++){
                    paramTypes[j]=Class.forName(param[j].getName());
                    params[j]=param[j].getSimpleName();
                }
                break;
            }
        }
        String methodName=null;
        for (int k=0;k<params.length;k++){
            methodName=name+"("+params[k]+")";
        }
        String userName = this.getUserName(request);
        String ip=getIp(request);
        Log log=new Log();
        log.setUserName(userName);
        log.setClassName(name1);
        log.setIP(ip);
        log.setParams(arrays);
        log.setMethodName(methodName);
        log.setCreateTime(new Date());
        logDateService.insert(log);
    }
    public String getIp(HttpServletRequest request){
        String remoteAddr = request.getRemoteAddr();
        if(remoteAddr.equals("0:0:0:0:0:0:0:1")){
            remoteAddr="127.0.0.1";
            return remoteAddr;
        }else {
            remoteAddr=remoteAddr;
            return remoteAddr;
        }
    }
    public String getUserName(HttpServletRequest request){
        String userName=null;
        for (Cookie cookie : request.getCookies()) {
            if(cookie.getName().equals("accountCookie")){
                userName = cookie.getValue();
            }
        }
        return userName;
    }
} 
