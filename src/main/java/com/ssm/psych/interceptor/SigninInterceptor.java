package com.ssm.psych.interceptor;

import com.ssm.psych.utils.UserConstants;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description 登录拦截器
 * @Author dpj
 * @Date 2022-11-21 10:05
 * @Version 1.0
 */
public class SigninInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws IOException {
        // 通过session中是否有用户来判断是否登录
        if (request.getSession().getAttribute(UserConstants.SESSION_USER) == null) {
            // 登录失败或未登录，跳转到登录页
            response.sendRedirect("/psych/");
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object handler, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                                Object handler, Exception ex) {
    }
}