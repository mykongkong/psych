<%--
  User: dpj
  Date: 2022/12/10
  Time: 20:45
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>欢迎页面</title>
        <style type="text/css">
            body {
                font-family: "Kanit", sans-serif;
                margin: 0;
                height: 100vh;
                overflow: hidden;
            }

            .container {
                width: 100%;
                display: flex;
                justify-content: center;
            }

            .effect1 {
                position: absolute;
                margin: 0;
                top: 43%;
                font-size: 2.3em;
                text-transform: uppercase;
                letter-spacing: 0.1em;
                z-index: 3;
                color:#5FB878;
            }

            .effect1 .letter {
                display: inline-block;
                line-height: 1em;
            }

            p {
                position: absolute;
                font-size: 1.8em;
                text-transform: uppercase;
                letter-spacing: 5px;
                color: #5FB878;
                font-weight: 100;
                top: 50%;
                z-index: 3;
                opacity: 0;
                top: 50%;
            }

            section {
                display: grid;
                grid-template-columns: repeat(10, auto);
            }

            .item {
                background-color:#009688;
                height: 100vh;
                z-index: 0;
            }
        </style>
    </head>
    <body>
    <div class="container">
        <div id="dh" style="display:none;">
            <img src="static/img/皮卡丘.gif"/>
        </div>
        <h1 class="effect1">欢迎来进行</h1>
        <p>心理测试</p>
    </div>

    <section>
        <div class="item"></div>
        <div class="item"></div>
        <div class="item"></div>
        <div class="item"></div>
        <div class="item"></div>
        <div class="item"></div>
        <div class="item"></div>
        <div class="item"></div>
        <div class="item"></div>
        <div class="item"></div>
        <div class="item"></div>
    </section>
    <script src="static/js/anime.min.js"></script>
    <script>
        let dh=document.getElementById("dh");
        setInterval(function(){
            dh.style="display:block;"
        },5000);
        let tl = anime.timeline({
            easing: 'easeOutExpo',
            duration: 850
        });

        tl.add({
            targets: 'section .item',
            width: '100%',
            backgroundColor: '#009688',
            delay: anime.stagger(100)
        });

        tl.add({
            targets: 'section .item',
            delay: anime.stagger(70),
            width: '97%',
            backgroundColor: '#7af8e5'
        });

        tl.add({
            targets: 'section .item',
            backgroundColor: '#FFFFFF',
            delay: anime.stagger(50,{from: 'center'})
        });

        tl.add({
            targets: 'p',
            top: '49%',
            duration: 4500,
            opacity: 1
        }, '-=1000')

        //text animation

        //wrap every letter in a span
        var textWrapper = document.querySelector('.effect1');
        textWrapper.innerHTML = textWrapper.textContent.replace(/([^.\s]|\w)/g, "<span class='letter'>$&</span>");

        anime.timeline()
            .add({
                targets: '.effect1 .letter',
                scale: [5,1],
                opacity: [0,1],
                translateZ: 0,
                easing: "easeOutExpo",
                duration: 1350,
                delay: function(el, i) {
                    return 70*i;
                }
            }, 1500);
    </script>
    </body>
</html>