<%--
  User: dpj
  Date: 2022/12/12
  Time: 9:36
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@include file="common.jsp" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>注册页面</title>
        <style>
            * {
                padding: 0;
                margin: 0;
                outline: none;
            }

            html, body {
                width: 100%;
                height: 100%;
            }

            /* 设置网页字体大小 */
            html {
                /* rem 的初始赋值：1rem = 10px */
                font-size: 62.5%;
            }

            body {
                font-size: 1.4rem;
                /* 设置背景图片 */
                background: url("static/img/log2.png") no-repeat center center;
                background-size: cover;
            }

            div {
                display: block;
            }

            label {
                display: inline-block;
                margin-left: -10px;
            }

            /* 设置表单的最小缩放宽度 */
            @media screen and (min-width: 500px) {
                .register-content {
                    padding: 0 20px;
                    width: auto;
                }
            }

            /* 设置注册区域样式 */
            .register-content {
                margin: 5% auto 0;
                width: 440px;
                height: 680px;
                background-color: #ECF0F1;
                opacity: 0.9;
                border-radius: 6px;
                position: relative;
            }

            /* 设置标题样式 */
            .content-title {
                padding-top: 20px;
                margin: 0 auto 20px;
                height: 40px;
                font-size: 3rem;
                font-weight: 400;
                text-align: center;
                letter-spacing: 2px;
            }

            /* 设置注册表单样式 */
            #registerForm {
                margin: 0 auto;
                width: 400px;
                text-align: center;
            }


            /* 性别单选框样式 */
            #sexBox {
                margin-left: -70px;
            }

            /* 设置头像图片样式 */
            #avatarImg {
                width: 100px;
                height: 100px;
                margin-left: -140px;
            }

            /* 设置更改头像按钮样式 */
            #modifyImgBtn {
                margin-left: 20px;
            }

            /* 设置底部按钮样式 */
            #btnBox {
                margin-left: 10px;
            }
        </style>
    </head>
    <body>
        <div class="register-content">
            <div class="content-title">
                <p id="register-title">注册帐户</p>
            </div>
            <form class="layui-form" action="" id="registerForm" lay-filter="registerFilter">
                <input hidden name="createBy">
                <input hidden name="updateBy">
                <div class="layui-form-item">
                    <div class="layui-input-inline">
                        <select name="deptid" lay-verify="required" id="deptList">
                            <option selected hidden disabled value="">请选择用户所属部门</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-inline">
                        <select name="roleId" lay-verify="required" id="roleList">
                            <option selected hidden disabled value="">请选择用户所属身份</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-inline">
                        <input type="text" name="username" required lay-verify="required"
                               placeholder="请输入用户名" value=""
                               class="layui-input">
                    </div>
                    <label></label>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-inline">
                        <input type="text" lay-verify="required|email" name="email" value=""
                               class="layui-input" placeholder="请输入邮箱">
                    </div>
                    <label></label>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-inline">
                        <input type="text" lay-verify="required|phonenumber" name="phonenumber"
                               value=""
                               class="layui-input" placeholder="请输入手机号">
                    </div>
                    <label></label>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block" id="sexBox">
                        <input type="radio" name="sex" value="1" title="男" checked>
                        <input type="radio" name="sex" value="0" title="女">
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-inline">
                        <input type="text" name="password" lay-reqtext="" lay-verify="required"
                               placeholder="请输入密码" value="" class="layui-input">
                    </div>
                    <label></label>
                </div>
                <div class="layui-form-item">
                    <div class="layui-upload">
                        <img class="layui-upload-img" id="avatarImg" src="" alt="头像">
                        <button type="button" class="layui-btn" id="modifyImgBtn">选择头像</button>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-inline">
                        <input type="text" name="remark" value="" class="layui-input"
                               placeholder="请输入备注">
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block" id="btnBox">
                        <button class="layui-btn" id="insertSubmitBtn" lay-submit
                                lay-filter="insertFilterSubmit" disabled="disabled"
                                style="background-color: #C2C2C2; cursor: not-allowed">确定
                        </button>
                        <button type="reset" id="insertResetBtn"
                                class="layui-btn layui-btn-primary">重置
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </body>
    <%--设置输入框提示信息显示条件--%>
    <script>
        layui.use(['layer', 'jquery', 'form', 'upload'], function () {
            let layer = layui.layer;
            let $ = layui.jquery;
            let form = layui.form;
            let upload = layui.upload;

            // 确定按钮
            let insertSubmitBtn = $("#insertSubmitBtn");

            // 确定按钮禁用
            function InsertSubmitBtnBlur() {
                insertSubmitBtn.attr("disabled", "disabled").attr("style", "background-color: #C2C2C2;" +
                    "cursor: not-allowed");
            }

            // 确定按钮可用
            function InsertSubmitBtnClear() {
                insertSubmitBtn.removeAttr("disabled", "disabled").removeAttr("style",
                    "background-color: #C2C2C2;" +
                    "cursor: not-allowed");
            }

            // 表用户名校验
            $("#registerForm input[name='username']").on('blur', function () {
                let usernameBox = $("#registerForm input[name='username']");
                if (usernameBox.val() === "") {
                    usernameBox.parent().next().html("<span id='usernameInsertError'>用户名不能为空！</span>").attr("style",
                        "color: #FF5722;" +
                        "line-height: 38px");
                    InsertSubmitBtnBlur();
                } else {
                    // 唯一性校验
                    $.ajax({
                        url: '${pageContext.request.contextPath}/sys/user/hasAccount',
                        type: 'GET',
                        dataType: 'JSON',
                        data: {
                            account: usernameBox.val(),
                            accountType: 0
                        },
                        success: function (res) {
                            if (res.status) {
                                usernameBox.parent().next().html("<i id='usernameInsertOk' class='layui-icon layui-icon-ok-circle'></i>").attr("style", "color: #5FB878; line-height: 38px; font-size: 30px");
                            } else {
                                usernameBox.parent().next().html("<span id='usernameInsertError'>用户名已注册！</span>").attr("style",
                                    "color: #FF5722;" +
                                    "line-height: 38px");
                                InsertSubmitBtnBlur();
                            }
                        }
                    })
                }
            })

            // 表头工具条添加功能之邮箱校验
            $("#registerForm input[name='email']").on('blur', function () {
                let emailBox = $("#registerForm input[name='email']");
                let emailReg = new RegExp("^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$");
                if (emailBox.val() === "") {
                    emailBox.parent().next().html("<span id='emailInsertError'>邮箱不能为空！</span>").attr("style",
                        "color: #FF5722;" +
                        "line-height: 38px");
                    InsertSubmitBtnBlur();
                } else {
                    if (!emailReg.test(emailBox.val())) {
                        emailBox.parent().next().html("<span id='emailInsertError'>邮箱格式错误！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        InsertSubmitBtnBlur();
                    } else {
                        // 唯一性校验
                        $.ajax({
                            url: '${pageContext.request.contextPath}/sys/user/hasAccount',
                            type: 'GET',
                            dataType: 'JSON',
                            data: {
                                account: emailBox.val(),
                                accountType: 1
                            },
                            success: function (res) {
                                if (res.status) {
                                    emailBox.parent().next().html("<i id='emailInsertOk' class='layui-icon layui-icon-ok-circle'></i>").attr("style", "color: #5FB878; line-height: 38px; font-size: 30px");
                                } else {
                                    emailBox.parent().next().html("<span id='emailInsertError'>邮箱已注册！</span>").attr("style",
                                        "color: #FF5722;" +
                                        "line-height: 38px");
                                    InsertSubmitBtnBlur();
                                }
                            }
                        })
                    }
                }
            })

            // 表头工具条添加功能之手机号校验
            $("#registerForm input[name='phonenumber']").on('blur', function () {
                let phoneBox = $("#registerForm input[name='phonenumber']");
                let phoneReg = new RegExp("^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$");
                if (phoneBox.val() === "") {
                    phoneBox.parent().next().html("<span id='phoneInsertError'>手机号不能为空！</span>").attr("style",
                        "color: #FF5722;" +
                        "line-height: 38px");
                    InsertSubmitBtnBlur();
                } else {
                    if (!phoneReg.test(phoneBox.val())) {
                        phoneBox.parent().next().html("<span id='phoneInsertError'>手机号格式错误！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        InsertSubmitBtnBlur();
                    } else {
                        // 唯一性校验
                        $.ajax({
                            url: '${pageContext.request.contextPath}/sys/user/hasAccount',
                            type: 'GET',
                            dataType: 'JSON',
                            data: {
                                account: phoneBox.val(),
                                accountType: 2
                            },
                            success: function (res) {
                                if (res.status) {
                                    phoneBox.parent().next().html("<i id='phoneInsertOk' class='layui-icon layui-icon-ok-circle'></i>").attr("style", "color: #5FB878; line-height: 38px; font-size: 30px");
                                } else {
                                    phoneBox.parent().next().html("<span id='phoneInsertError'>手机号已注册！</span>").attr("style",
                                        "color: #FF5722;" +
                                        "line-height: 38px");
                                    InsertSubmitBtnBlur();
                                }
                            }
                        })
                    }
                }
            })

            // 表头工具条添加功能之密码校验
            let passwordReg =
                /^(?=.{8})(?=.*?[a-zA-Z])(?=.*?\d)(?=.*?[*?!&￥$%^#,./@";:><\[\]}{\-=+_\\|》《。，、？’‘“”~ `]).*$/;
            let lengthReg = /^(?=.{8}).*$/;
            let letterReg = /^(?=.*?[a-zA-Z]).*$/;
            let numberReg = /^(?=.*?\d).*$/;
            let characterReg = /^(?=.*?[*?!&￥$%^#,./@";:><\[\]}{\-=+_\\|》《。，、？’‘“”~ `]).*$/;
            $("#registerForm input[name='password']").on('blur', function () {
                let passwordBox = $("#registerForm input[name='password']");
                let passwordVal = passwordBox.val();
                if (passwordVal === "") {
                    passwordBox.parent().next().html("<span id='passwordInsertError'>密码不能为空！</span>").attr("style",
                        "color: #FF5722;" +
                        "line-height: 38px");
                    InsertSubmitBtnBlur();
                } else if (!passwordReg.test(passwordVal)) {
                    if (!lengthReg.test(passwordVal)) {
                        passwordBox.parent().next().html("<span id='passwordInsertError'>密码不能小于8位！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        InsertSubmitBtnBlur();
                    } else if (!letterReg.test(passwordVal)) {
                        passwordBox.parent().next().html("<span id='passwordInsertError'>密码必须包含至少一个字母！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        InsertSubmitBtnBlur();
                    } else if (!numberReg.test(passwordVal)) {
                        passwordBox.parent().next().html("<span id='passwordInsertError'>密码必须包含至少一个数字！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        InsertSubmitBtnBlur();
                    } else if (!characterReg.test(passwordVal)) {
                        passwordBox.parent().next().html("<span id='passwordInsertError'>密码必须包含至少一个特殊字符！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        InsertSubmitBtnBlur();
                    }
                } else {
                    passwordBox.parent().next().html("<i id='passwordInsertOk' class='layui-icon layui-icon-ok-circle'></i>").attr("style", "color: #5FB878; line-height: 38px; font-size: 30px");
                }
            })

            // 选择头像
            let avatarSrc = "";
            upload.render({
                elem: '#modifyImgBtn',
                url: '${pageContext.request.contextPath}/sys/user/uploadAvatar',
                method: 'POST',
                field: 'multipartFile',
                accept: 'images',
                acceptMime: 'image/*',
                done: function (res) {
                    if (res.status) {
                        $("#avatarImg").attr('src', res.message);
                        avatarSrc = res.message;
                        // 确定按钮启用
                        InsertSubmitBtnClear();
                    }
                }
            })

            // 重置按钮刷新页面
            $("#insertResetBtn").on("click", function () {
                // 刷新页面
                location.reload();
            })

            // 加载部门下拉列表
            $.ajax({
                url: '${pageContext.request.contextPath}/sys/user/queryDeptList',
                type: 'GET',
                dataType: 'JSON',
                async: false,
                success: function (deptListData) {
                    let deptListSelect = $('#deptList');
                    // 清空下拉框
                    deptListSelect.empty();
                    // 添加提示语
                    deptListSelect.append("<option selected hidden disabled value=''>请选择用户所属部门</option>");
                    // 循环填充下拉框
                    $.each(deptListData, function (index, val) {
                        deptListSelect.append(
                            "<option value='" + val.deptId + "'>" + val.deptName +
                            "</option>"
                        );
                    });
                }
            });

            // 加载角色下拉列表
            $.ajax({
                url: '${pageContext.request.contextPath}/sys/user/queryRoleList',
                type: 'GET',
                dataType: 'JSON',
                async: false,
                success: function (deptListData) {
                    let roleListSelect = $('#roleList');
                    // 清空下拉框
                    roleListSelect.empty();
                    // 添加提示语
                    roleListSelect.append("<option selected hidden disabled value=''>请选择用户所属角色</option>");
                    // 循环填充下拉框
                    $.each(deptListData, function (index, val) {
                        roleListSelect.append(
                            "<option value='" + val.roleId + "'>" + val.roleName +
                            "</option>"
                        );
                    })
                }
            });

            // 渲染添加功能弹出层表单的两个下拉框
            form.render('select', 'registerFilter');

            // 添加用户
            form.on('submit(insertFilterSubmit)', function (data) {
                if (data.field.status === 'on') {
                    data.field.status = 1;
                } else {
                    data.field.status = 0;
                }
                data.field.createBy = data.field.username;
                data.field.updateBy = data.field.username;
                data.field.avatar = avatarSrc + "";
                console.log(data);
                $.ajax({
                    url: '${pageContext.request.contextPath}/sys/user/insert',
                    method: 'POST',
                    dataType: 'JSON',
                    data: data.field,  //jquery获取表单数据
                    success: function (res) {
                        if (res.status) {
                            layer.msg('注册成功，3秒后将自动跳转到登录页面！', {icon: 6});
                            setTimeout(function () {
                                window.location = "signin.jsp";
                            }, 3000);
                        } else {
                            layer.msg(res.message, {icon: 7});
                        }
                    }
                })
                return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
            })
        })
    </script>
</html>