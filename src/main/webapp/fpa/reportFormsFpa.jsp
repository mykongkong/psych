<%--
  Created by IntelliJ IDEA.
  User: 情雅圣
  Date: 2022/12/7
  Time: 16:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common.jsp" %>
<html>
<head>
    <title>性格测试-报表统计</title>
</head>
<body>
<div style="background-color:#3e5c8f;width:1450px;height:730px;">
    <h2>FPA性格测报表统计</h2>
    <div style="width:1300px;height:700px;margin: auto;padding: 0;">
        <div id="zt" style="width:600px;height:300px;margin: auto;padding: 0;"></div>
        <div id="bt" style="width:1200px; height:400px;margin: auto;padding: 0;"></div>
    </div>

</div>
</body>
<script type="text/javascript" src="https://cdn.bootcss.com/echarts/4.2.1-rc1/echarts.min.js"></script>

<script type="text/javascript">
    $.ajax({
        url:'${pageContext.request.contextPath}/fpa/result/resultCount',
        type:'POST',
        success:function (ref){
            console.log(ref.r)
            let myEcharts=echarts.init(document.getElementById("zt"));
            let option={
                colorBy:'data',
                legend: {
                    orient: 'vertical',
                    left: 'left'
                },
                tooltip:{//提示框组件
                    trigger:'item'
                },
                series: [
                    {
                        name: 'FPA性格占比',
                        type: 'pie',
                        data: [
                            { value: ref.r, name: '红色性格',itemStyle: {color: '#ff0000'}},
                            { value: ref.y, name: '黄色性格',itemStyle: {color: '#ffff00'}},
                            { value: ref.g, name: '绿色性格',itemStyle: {color: '#55aa00'}},
                            { value: ref.b, name: '蓝色性格',itemStyle: {color: '#0055ff'}},
                            { value: ref.h, name: '复合性格',itemStyle: {color: '#1ad291'}},
                        ],
                        emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            }
            myEcharts.setOption(option);
            let mEcharts=echarts.init(document.getElementById("bt"));
            let options={
                xAxis: {
                    type: 'category',
                    data: ['红色性格', '黄色性格', '蓝色性格', '绿色性格','复合性格']
                },
                tooltip:{//提示框组件
                    trigger:'item'
                },
                yAxis: {
                    type: 'value'
                },
                series: [
                    {
                        data: [ref.r,ref.y,ref.b,ref.g,ref.h],
                        type: 'line',
                        smooth: true,
                        lineStyle:{
                            color:'#fff'
                        },
                        markPoint:{
                            data:[{type:'max',},{type:'min'}]
                        },
                    }
                ]
            }
            mEcharts.setOption(options);
        }
    })
</script>
</html>
