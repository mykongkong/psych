<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: 情雅圣
  Date: 2022/12/5
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common.jsp" %>
<html>
<head>
    <title>性格测试-测试结果</title>
</head>
<body>
<%--模糊查询form表单--%>
<form class="layui-form" style="margin-top: 10px">
    <div class="layui-inline">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-inline">
            <input type="text" name="name" placeholder="请输入测试名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-inline">
        <label class="layui-form-label">测试ID</label>
        <div class="layui-input-inline">
            <select name="testPlanId">
                <option value=""></option>
                <option value="1">测试ID：1</option>
                <option value="3">测试ID：3</option>
                <option value="8">测试ID：8</option>
            </select>
        </div>
    </div>
    <div class="layui-inline">
        <div class="layui-input-inline">
            <button class="layui-btn" lay-submit lay-filter="queryVague">查询</button>
            <button type="reset" class="layui-btn layui-btn-primary" id="myButton">重置</button>
        </div>
    </div>
</form>
<%--数据渲染的容器--%>
<table class="layui-hide" id="test" lay-filter="test"></table>
<%--查看结果展示--%>
<div id="result" style="width: 100%;height: 100%; display: none;">
    <fieldset id="ceshi">
        <div>
            <div >
                <div id="myPdf" align="auto">
                    <div style="background-color:#fcf4ed;border-radius:40px 40px 0 0;z-index: 2;padding: 40px 20px 20px 20px;color: #41464B">
                        <h1 style="text-align:center;line-height: 60px;font-size: 30px;font-weight: 600;color: #7a2114;">
                            测评报告预览</h1>
                        <div style="background: rgba(87,144,255,.2);padding: 20px;text-align: center;margin-bottom: 20px"
                             id="div1">

                        </div>
                        <div id="div2" style="width:100%;height:400px;"></div>

                        <div style="background: rgba(87,144,255,.2);padding: 20px;margin-top: 20px;text-align: center"
                             id="div3">

                        </div>
                        <div style="background: white;padding: 20px;margin-top: 20px; text-align: center" id="div4"></div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div style="margin-left:40%;">
        <button class="layui-btn" target="_blank" rel="external nofollow" onclick="makeMpdf('Fpa检测报告')">下载Fpa检测文件
        </button>
    </div>
</div>
</body>
<%--表头工具栏--%>
<script type="text/html" id="toolbar-head">
    <div>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="delete"><i class="layui-icon layui-icon-delete"></i>删除</button>
    </div>
</script>
<%--行内工具栏--%>
<script type="text/html" id="toolbar-line">
    <a class="layui-btn layui-btn-xs" lay-event="look">查看</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script type="text/javascript" src="https://cdn.bootcss.com/echarts/4.2.1-rc1/echarts.min.js"></script>
<script>
    layui.use(['table','form','jquery','layer'],function(){
        let table=layui.table;
        let form=layui.form;
        let $=layui.$;
        let layer=layui.layer;
        //数据的渲染
        table.render({
            elem:'#test',
            url:'${pageContext.request.contextPath}/fpa/result/queryAll',
            toolbar:'#toolbar-head',//表头左侧工具栏
            defaultToolbar:['filter', 'print', 'exports'],//表头工右侧工具栏
            height:'full-90',
            page:true,//开启分页
            cols:[[//数据渲染,二维数组
                {type: 'checkbox', fixed: 'left'}
                ,{field:'testerId', fixed: 'left', title: 'ID', sort: true}
                ,{field:'name',title: '姓名'}
                ,{field:'phone', title:'电话'}
                ,{field:'createTime', title: '测试时间'}
                ,{field:'redCount', title: '红色得分',sort: true}
                ,{field:'yellowCount', title: '黄色得分',sort: true}
                ,{field:'blueCount', title:'蓝色得分',sort: true}
                ,{field:'greenCount', title:'绿色得分',sort: true}
                ,{field:'testPlanId', title:'测试计划ID',sort: true}
                ,{fixed: 'right', title:'操作', toolbar: '#toolbar-line'}
            ]]
        });
        //表头按钮事件
        table.on('toolbar(test)',function(obj){
            let id = obj.config.id;
            let checkStatus = table.checkStatus(id);//获取数据表格中的复选框
            let data=checkStatus.data;
            if(obj.event==='delete'){
                if(data.length<=0){
                    layer.msg('至少要选择一行');
                }else{
                    layer.confirm("确认要删除吗？",function(index){
                        let idList=[];
                        for(let i=0;i<data.length;i++){
                            idList[i]=data[i].testerId;
                        }
                        //console.log(idList)
                        $.ajax({
                            url:'${pageContext.request.contextPath}/fpa/result/delete',
                            type:'POST',
                            contentType:"application/json",
                            data:JSON.stringify(idList),
                            success:function (ref){
                                table.reload('test',{
                                    page:{
                                        curr:1
                                    }
                                });
                                if(ref.status){
                                    layer.msg(ref.message,{icon:1});
                                }else {
                                    layer.msg(ref.message,{icon:5});
                                }
                            }
                        })
                        layer.close(index);
                    })
                }
            }
        });
        //行内按钮事件
        table.on('tool(test)',function(obj){
            let data=obj.data;
            if(obj.event==='del'){//删除按钮的事件
                layer.confirm('确认删除吗？',function (index){
                    let id=[];
                    id[0]=data.testerId;
                    //console.log(id)
                    $.ajax({
                        url:'${pageContext.request.contextPath}/fpa/result/delete',
                        type:'POST',
                        contentType:"application/json",
                        data:JSON.stringify(id),
                        success:function (ref) {
                            table.reload('test',{
                                page:{
                                    curr:1
                                }
                            });
                            if(ref.status){
                                layer.msg(ref.message,{icon:1});
                            }else {
                                layer.msg(ref.message,{icon:5});
                            }
                        }
                    })
                    layer.close(index)
                })
            }else if(obj.event==='look'){//查看按钮
                layer.open({
                    title:'性格测试结果图示',
                    type:1,
                    area: ['571px', '700px'],
                    content:$('#result'),
                    cancel:function (index,layero) {
                        $("#result").hide();
                    }
                });
                $.ajax({
                    url:'${pageContext.request.contextPath}/fpa/result/queryID',
                    dataType:'JSON',
                    type:'POST',
                    data:{testerId: obj.data.testerId},
                    success:function (ref){
                        let color;
                        let red=ref.data[0].redCount;
                        let blue=ref.data[0].blueCount;
                        let yellow=ref.data[0].yellowCount;
                        let green=ref.data[0].greenCount;
                        let array=[red,blue,yellow,green];
                        let arrays=array.sort(function(a,b){return b-a});
                        let MyEcharts=echarts.init(document.getElementById('div2'));
                        if(arrays[0]==red){
                            color=2;
                        }else if(arrays[0]==blue){
                            color=3;
                        }else if(arrays[0]==yellow){
                            color=1;
                        }else if(arrays[0]==green){
                            color=4;
                        }
                        let option={
                            title:{
                                text:'FPA性格测试',
                                left: 'center',
                            },
                            colorBy:'data',
                            legend: {
                                orient: 'vertical',
                                left: 'left'
                            },
                            tooltip:{//提示框组件
                                trigger:'item'
                            },
                            series: [
                                {
                                    name: 'FPA性格占比',
                                    type: 'pie',
                                    data: [
                                        { value: blue,name:'蓝色性格',itemStyle: {color: '#0055ff'}},
                                        { value: green,name:'绿色性格',itemStyle: {color: '#55aa00'}},
                                        { value: yellow,name:'黄色性格',itemStyle: {color: '#ffff00'}},
                                        { value: red,name:'红色性格',itemStyle: {color: '#ff0000'}}
                                    ],
                                    emphasis: {
                                        itemStyle: {
                                            shadowBlur: 10,
                                            shadowOffsetX: 0,
                                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                                        }
                                    }
                                }
                            ]
                        }
                        MyEcharts.setOption(option);
                        $.ajax({
                            url:'${pageContext.request.contextPath}/fpa/result/queryDiv',
                            type:'POST',
                            dataType:'JSON',
                            data:{id:color},
                            success:function (ref){
                                /*console.log(ref.data[0].div1)*/
                                $("#div1").html(ref.data[0].div1);
                                $("#div3").html(ref.data[0].div2);
                                $("#div4").html(ref.data[0].div3)
                            }
                        })
                    },
                })
            }
        });
        //模糊查询的功能
        form.on('submit(queryVague)',function(data){
            table.reload("test",{
                url:'${pageContext.request.contextPath}/fpa/result/fuzzyQuery',
                where:{
                    name: data.field.name
                    , testPlanId: data.field.testPlanId
                }
            })
            return false;
        });
    });
</script>
</html>
