<%--
  Created by IntelliJ IDEA.
  User: 情雅圣
  Date: 2022/12/9
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common.jsp" %>
<html>
<head>
    <title>日志管理</title>
</head>
<body>
<table id="test" lay-filter="test"></table>
</body>
<script>
    layui.use(['table','jquery','layer'],function (){
        let table=layui.table;
        let $=layui.$;
        table.render({
            elem:"#test",
            url:"${pageContext.request.contextPath}/log/date/findAll",
            defaultToolbar: ['filter', 'exports', 'print'],
            height: 'full-90',
            page:true,
            cols:[[
                {type:'checkbox',fixed:'left',sort: true},
                {field:'id', title: 'ID'},
                {field: 'userName',title: '用户姓名'},
                {field: 'ip',title: 'IP地址'},
                {field: 'className',title:'类名'},
                {field:'methodName',title: '方法名'},
                {field: 'params',title: '参数'},
                {field: 'createTime',title: '创建时间'}
            ]]
        })
    })
</script>
</html>
