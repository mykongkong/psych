<%--
  Created by IntelliJ IDEA.
  User: 情雅圣
  Date: 2022/12/1
  Time: 10:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>性格测试-测试计划</title>
    <style type="text/css">
        body .demo-class1 .layui-layer-title{background:#ffb800; color:#fff; border: none;}
        body .demo-class2 .layui-layer-title{background:#ff5722; color:#fff; border: none;}
        body .demo-class2 .layui-layer-btn .layui-layer-btn0{background:#ff5722;border:1px solid #ff5722;}
    </style>
    <link href="./layui/css/layui.css" rel="stylesheet">
</head>
<body>
<%--模糊查询表单--%>
    <form class="layui-form" style="margin-top: 10px">
    <div class="layui-inline">
        <label class="layui-form-label">测试名称</label>
        <div class="layui-input-inline">
            <input type="text" name="testName" placeholder="请输入测试名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-inline">
        <label class="layui-form-label">创建人</label>
        <div class="layui-input-inline">
            <input type="text" name="createBy" placeholder="请输入创建人" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-inline">
        <div class="layui-input-inline">
            <button class="layui-btn" lay-submit lay-filter="queryVague">查询</button>
            <button type="reset" class="layui-btn layui-btn-primary" id="myButton">重置</button>
        </div>
    </div>
</form>
<%--数据添加表单--%>
<form class="layui-form" id="form-sub" style="display:none; margin-top: 20px;">
    <div class="layui-form-item">
        <label class="layui-form-label">测试名称</label>
        <div class="layui-input-inline">
            <input type="text" name="testName" placeholder="请输入测试名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">开始时间</label>
        <div class="layui-input-inline">
            <input type="text" name="testBegin" id="date-action" placeholder="请输入标题" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">结束时间</label>
        <div class="layui-input-inline">
            <input type="text" name="testEnd" id="date-end" placeholder="请输入标题" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-inline">
            <input type="text" name="remark" placeholder="请输入备注" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formInsert">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<%--数据修改表单--%>
<form class="layui-form" id="form-revise" lay-filter="revise" style="display:none; margin-top: 20px;">
    <input name="id" style="display:none;"/>
    <div class="layui-form-item">
        <label class="layui-form-label">测试名称</label>
        <div class="layui-input-inline">
            <input type="text" name="testName" placeholder="请输入测试名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">开始时间</label>
        <div class="layui-input-inline">
            <input type="text" name="testBegin" id="date-begin" placeholder="请输入标题" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">结束时间</label>
        <div class="layui-input-inline">
            <input type="text" name="testEnd" id="date-over" placeholder="请输入标题" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-inline">
            <input type="text" name="remark" placeholder="请输入备注" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="button" class="layui-btn layui-btn-warm" lay-submit lay-filter="formRevise" >立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<table class="layui-hide" id="test" lay-filter="test"></table>
<div id="qrcode" style="width:150px; height:150px; display: none; margin-left: 50px;margin-top: 20px"></div>
</body>
<script src="layui/layui.js"></script>
<script src="../static/js/jquery-3.6.1.js"></script>
<%--表头左侧按钮--%>
<script type="text/html" id="toolBarDemo">
    <div>
        <button type="button" class="layui-btn" lay-event="add"><i class="layui-icon layui-icon-add-circle"></i>添加</button>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="delete"><i class="layui-icon layui-icon-delete"></i>删除</button>
    </div>
</script>
<%--行内按钮--%>
<script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-xs" lay-event="seeQRCode">查看二维码</a>
        <a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="edit">修改</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script>
    layui.use(['table','jquery','form','layer'],function (){
        let table=layui.table;
        let $=layui.$;
        let form=layui.form;
        let layer=layui.layer;
        let laydate = layui.laydate;
        //数据的渲染
        table.render({
            elem: '#test'
            ,url:'${pageContext.request.contextPath}/fpa/test/queryAll' // 此处为静态模拟数据，实际使用时需换成真实接口
            ,toolbar: '#toolBarDemo'//表头工具栏左侧区域
            ,defaultToolbar: ['filter', 'exports', 'print', {
                title: '帮助'
                ,layEvent: 'LAYTABLE_TIPS'
                ,icon: 'layui-icon-tips'
            }]//可自由配置头部工具栏右侧区域
            ,height: 'full-90' // 最大高度减去其他容器已占有高度差
            ,cellMinWidth: 80
            ,totalRow: false // 关闭合计行
            ,page: true
            ,cols: [[//数据渲染
                {type: 'checkbox', fixed: 'left'}
                ,{field:'id', fixed: 'left', width:80, title: 'ID', sort: true, totalRowText: '合计：'}
                ,{field:'testName', width:160, title: '测试名称'}
                ,{field:'testCode', title:'测试邀请码', hide: 0, width:100, edit: 'text'}
                ,{field:'testBegin', width:160, title: '开始时间'}
                ,{field:'testEnd', title: '结束时间', Width: 160}
                ,{field:'createTime', width: 160, title: '创建时间'}
                ,{field:'createBy', title:'创建者', width: 100,}
                ,{field:'updateTime', title:'修改时间', width:160}
                ,{field:'updateBy', title:'修改者', width: 100}
                ,{field:'remark', title:'备注', width: 100,totalRow: '{{= parseInt(d.TOTAL_NUMS) }} 次'}
                ,{fixed: 'right', title:'操作', width:210, minWidth: 125, toolbar: '#barDemo'}
            ]]
        });
        //tool单机时触发的事件,行内工具
        table.on('tool(test)',function (obj){
            let data=obj.data;
            if(obj.event==='edit'){
                //表单数据的自动赋值
                form.val('revise',{
                    "testName":data.testName,
                    "remark":data.remark,
                    "id":data.id
                });
                //修改表单日期框的渲染
                laydate.render({
                    elem:'#date-begin',
                    type:'datetime',
                    value:data.testBegin,
                    done:function (){
                        $.ajax({
                            url:'${pageContext.request.contextPath}/fpa/test/proofDate',
                            data:{"testEnd":$("#date-begin").val()},
                            dataType:'JSON',
                            type:"POST",
                            success:function (ref) {
                                if(ref.status){
                                    layer.tips(ref.message,'#date-begin')
                                }
                            }
                        })
                    }
                })
                laydate.render({
                    elem:'#date-over',
                    type:'datetime',
                    value:data.testEnd,
                    done:function (){
                        $.ajax({
                            url:'${pageContext.request.contextPath}/fpa/test/proofDate',
                            data:{"testEnd":$("#date-over").val()},
                            dataType:'JSON',
                            type:"POST",
                            success:function (ref) {
                                if(ref.status){
                                    layer.tips(ref.message,'#date-over')
                                }
                            }
                        })
                    }
                })
                //修改的弹出层
                layer.open({
                    type:1,
                    title:'修改',
                    content:$("#form-revise"),
                    skin:'demo-class1',
                    area:['400px','400px'],
                    cancel:function (index,layero) {
                        $("#form-revise").hide();
                    }
                })
            }else if(obj.event==='del'){
                layer.confirm("确认要删除吗？",{
                    skin:'demo-class2',
                    icon:7
                },function (index){
                    let id=[];
                    id[0]=data.id;
                    $.ajax({
                        url:'${pageContext.request.contextPath}/fpa/test/delete',
                        contentType:"application/json",
                        type:'POST',
                        data:JSON.stringify(id),
                        success:function (ref){
                            table.reload("test",{
                                page: {
                                    curr: 1 //重新从第 1 页开始
                                }
                            })
                            layer.msg(ref.message,{icon:1});
                        }
                    })
                    obj.del();
                    layer.close(index);
                })
            }else if(obj.event==='seeQRCode'){
                layer.open({
                    title:'查看二维码',
                    type:1,
                    skin:'layui-layer-molv',
                    content:$("#qrcode"),
                    area: ['300px', '300px'],
                    cancel:function (index,layero) {
                        $("#qrcode").hide();
                    }
                })
            }
        });
        //表单头部的工具
        table.on('toolbar(test)',function(obj){
            let id = obj.config.id;
            let checkStatus = table.checkStatus(id);//获取数据表格中的复选框
            let data=checkStatus.data;
            if(obj.event==='add'){
                layer.open({
                    type:1,
                    title:'添加',
                    content:$("#form-sub"),
                    skin:'layui-layer-molv',
                    area:['400px','400px'],
                    cancel:function (index,layero) {
                        $("#form-sub").hide();
                    }
                })
            }else if(obj.event==='delete'){//批量的删除数据
                if(data.length<=0){
                    layer.open({
                        title:"提示",
                        type:0,
                        content:'至少选中一行！',
                        skin:'demo-class2',
                        icon:7
                    })
                }else{
                    layer.confirm("确认要删除吗？",{
                        skin:'demo-class2',
                        icon:7
                    },function (index){
                        let idArray=[];
                        for(let i=0;i<data.length;i++){
                            idArray[i]=data[i].id;
                        }
                        $.ajax({
                            url:'${pageContext.request.contextPath}/fpa/test/delete',
                            contentType : "application/json",
                            data:JSON.stringify(idArray),
                            type:'POST',
                            success:function (ref){
                                table.reload("test",{
                                    page: {
                                        curr: 1 //重新从第 1 页开始
                                    }
                                });
                                layer.msg(ref.message,{icon:1});
                            }
                        })
                        layer.close(index);
                    })
                }
            }
        });
        //添加表单时间框的渲染
        laydate.render({
            elem:'#date-action',
            type:'datetime',
            value:new Date(),
            done:function (){
                $.ajax({
                    url:'${pageContext.request.contextPath}/fpa/test/proofDate',
                    data:{"testEnd":$("#date-action").val()},
                    dataType:'JSON',
                    type:"POST",
                    success:function (ref) {
                        if(ref.status){
                            layer.tips(ref.message,'#date-action')
                        }
                    }
                })
            }

        })
        laydate.render({
            elem:'#date-end',
            type:'datetime',
            value:new Date(new Date().getTime()+2 * 86400 * 1000),
            done:function (){
                $.ajax({
                    url:'${pageContext.request.contextPath}/fpa/test/proofDate',
                    data:{"testEnd":$("#date-end").val()},
                    dataType:'JSON',
                    type:"POST",
                    success:function (ref) {
                        if(ref.status){
                            layer.tips(ref.message,'#date-end')
                        }
                    }
                })
            }
        });
        //执行添加表单被提交事件
        form.on('submit(formInsert)',function(data){
            $.ajax({
                url:'${pageContext.request.contextPath}/fpa/test/insert',
                dataType:'JSON',
                type:'POST',
                data:data.field,
                success:function(result){
                    $("#form-sub").hide();
                    layer.closeAll();
                    table.reload("test",{
                        page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                    layer.msg(result.message,{icon:1});
                }
            })
            return false;
        });
        //执行修改表单被提交事件
        form.on('submit(formRevise)',function (data){
            $.ajax({
                url:'${pageContext.request.contextPath}/fpa/test/update',
                dataType:'JSON',
                type:'POST',
                data:data.field,
                success:function(result){
                    $("#form-revise").hide();
                    console.log(result.message)
                    layer.closeAll();
                    //重新加载数据
                    table.reload("test",{
                        page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                    if(result.status){
                        layer.msg(result.message,{icon:1});
                    }else {
                        layer.msg(result.message,{icon:5});
                    }
                }
            })
            return false;
        });
        //执行模糊查询表单被提交事件
        form.on('submit(queryVague)',function (data){
            table.reload("test",{
                url:'${pageContext.request.contextPath}/fpa/test/fuzzyQuery',
                where:{
                    testName: data.field.testName
                    , createBy: data.field.createBy
                }
            })
            return false;
        });
        //渲染二维码
        layui.config({
            base:'${pageContext.request.contextPath}/fpa/layui/',//加载配置文件的路径
        }).extend({
            qrcode: 'qrcode'//方法：文件名
        }).use('qrcode',function (){
            let $=layui.$;
            $("#qrcode").qrcode({
                render:'canvas',
                text: "http://192.168.8.4:8080/${pageContext.request.contextPath}/test/testRecode.jsp",
                //宽高和颜色可以不设置
                width: 200,
                height: 200,
                foreground: "#000000",  //二维码上层颜色
                background: "#FFF"      //背景颜色
            })
        })
    })
</script>
</html>