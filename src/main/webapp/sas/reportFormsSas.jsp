<%--
  Created by IntelliJ IDEA.
  User: 情雅圣
  Date: 2022/12/7
  Time: 16:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common.jsp" %>
<html>
<head>
    <title>焦虑测试-报表统计</title>
</head>
<body>
<div style="background-color:#3e5c8f;width:1450px;height:730px;">
    <h2>SAS性格测报表统计</h2>
    <div style="width:1300px;height:700px;margin: auto;padding: 0;">
        <div id="zt" style="width:600px;height:300px;margin: auto;padding: 0;"></div>
        <div id="bt" style="width:1200px; height:400px;color: #ffffff "></div>
    </div>
</div>
</body>
<script type="text/javascript" src="https://cdn.bootcss.com/echarts/4.2.1-rc1/echarts.min.js"></script>

<script type="text/javascript">
    $.ajax({
        url:'${pageContext.request.contextPath}/sas/result/resultCount',
        type:'POST',
        success:function (ref){
            let myEcharts=echarts.init(document.getElementById("zt"));
            let option={
                colorBy:'data',
                legend: {
                    orient: 'vertical',
                    left: 'left'
                },
                tooltip:{//提示框组件
                    trigger:'item'
                },
                series: [
                    {
                        name: 'SAS性格占比',
                        type: 'pie',
                        data: [
                            { value: ref.normal, name: '正常',itemStyle: {color: '#27798c'}},
                            { value: ref.mild, name: '轻度焦虑',itemStyle: {color: '#a4a42e'}},
                            { value: ref.middle, name: '中度焦虑',itemStyle: {color: '#659335'}},
                            { value: ref.severe, name: '重度焦虑',itemStyle: {color: '#732d8c'}},
                        ],
                        emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            }
            myEcharts.setOption(option);
            let mEcharts=echarts.init(document.getElementById("bt"));
            let options={
                xAxis: {
                    type: 'category',
                    data: ['正常', '轻度焦虑', '中度焦虑', '重度焦虑']
                },
                tooltip:{//提示框组件
                    trigger:'item'
                },
                yAxis: {
                    type: 'value'
                },
                series: [
                    {
                        data: [ref.normal,ref.mild,ref.middle,ref.severe],
                        type: 'line',
                        smooth: true,
                        lineStyle:{
                            color:'#fff'
                        },
                        markPoint:{
                            data:[{type:'max',},{type:'min'}]
                        },
                    }
                ]
            }
            mEcharts.setOption(options);
        }
    })
</script>
</html>
