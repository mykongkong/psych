<%--
  User: dpj
  Date: 2022/11/26
  Time: 10:43
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../common.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>焦虑测试-题目管理</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
    <%--模糊查询表单--%>·
    <form class="layui-form" style="margin-top: 10px">
        <div class="layui-inline">
            <label class="layui-form-label">问题</label>
            <div class="layui-input-inline">
                <input type="text" name="question" placeholder="请输入问题" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">创建人</label>
            <div class="layui-input-inline">
                <input type="text" name="createBy" placeholder="请输入创建人" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <div class="layui-input-inline">
                <button class="layui-btn" lay-submit lay-filter="selectFormfilter">查询</button>
                <button type="reset" class="layui-btn layui-btn-primary" id="myButton">重置</button>
            </div>
        </div>
    </form>
        <%--数据表格--%>
        <table id="demo" lay-filter="test"></table>
    </body>
    <%--表头按钮--%>
    <script type="text/html" id="toolbarDemo">
        <div class="layui-btn-container">
            <button type="button" class="layui-btn" lay-event="add"><i class="layui-icon layui-icon-add-circle"></i>添加</button>
            <button type="button" class="layui-btn layui-btn-danger" lay-event="delete"><i class="layui-icon layui-icon-delete"></i>删除</button>
        </div>
    </script>
    <%--行内按钮--%>
    <script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-xs" lay-event="edit">修改</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
    <%--添加数据的表单--%>
    <form class="layui-form" id="form-sub" method="post" style="display: none">
        <div class="layui-form-item">
            <label class="layui-form-label">问题描述</label>
            <div class="layui-input-block">
                <input type="text" name="question" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">选项A</label>
            <div class="layui-input-block">
                <textarea placeholder="请输入内容" class="layui-textarea" name="optionA"></textarea>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">选项B</label>
            <div class="layui-input-block">
                <textarea placeholder="请输入内容" class="layui-textarea" name="optionB"></textarea>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">选项C</label>
            <div class="layui-input-block">
                <textarea placeholder="请输入内容" class="layui-textarea" name="optionC"></textarea>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">选项D</label>
            <div class="layui-input-block">
                <textarea placeholder="请输入内容" class="layui-textarea" name="optionD"></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">问题类型</label>
            <div class="layui-input-block">
                <select name="type" lay-filter="aihao" >
                    <option value="0">反向计分</option>
                    <option value="2" selected="">正向计分</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-block">
                <input type="checkbox" value="0" name="status" lay-skin="switch" lay-text="可用|不可用">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button  class="layui-btn" lay-submit lay-filter="demo1" id="add-button">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
    <%--修改--%>
    <form class="layui-form" id="form-upd" method="post" lay-filter="update" style="display: none">
        <input name="id" type="hidden"/>
        <div class="layui-form-item">
            <label class="layui-form-label">问题描述</label>
            <div class="layui-input-block">
                <input type="text" name="question" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">选项A</label>
            <div class="layui-input-block">
                <textarea placeholder="请输入内容" class="layui-textarea" name="optionA"></textarea>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">选项B</label>
            <div class="layui-input-block">
                <textarea placeholder="请输入内容" class="layui-textarea" name="optionB"></textarea>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">选项C</label>
            <div class="layui-input-block">
                <textarea placeholder="请输入内容" class="layui-textarea" name="optionC"></textarea>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">选项D</label>
            <div class="layui-input-block">
                <textarea placeholder="请输入内容" class="layui-textarea" name="optionD"></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">问题类型</label>
            <div class="layui-input-block">
                <select name="type" lay-filter="aihao" >
                    <option value="0">反向计分</option>
                    <option value="2" selected="">正向计分</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-block">
                <input type="checkbox" value="0" name="status" lay-skin="switch" lay-text="可用|不可用">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button  class="layui-btn" lay-submit lay-filter="demo2" id="update-button">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
    <script>
        layui.use(['table', 'jquery', 'form'], function () {
            let table = layui.table;
            let form = layui.form;
            let $ = layui.jquery;

            // 数据表格分页
            let myTable = table.render({
                elem: '#demo'
                ,height: 'full-80' // 最大高度减去其他容器已占有高度差
                , url: '${pageContext.request.contextPath}/sas/question/queryAll' // 数据接口
                , toolbar: '#toolbarDemo'
                , defaultToolbar: ['filter', 'exports', 'print']
                , cellMinWidth: 80
                , totalRow: false // 开启合计行
                , page: true // 开启分页
                , limit: 10
                , limits: [5, 10, 15, 20, 25, 30, 50, 100]
                , cols: [[//数据渲染
                    {type: 'checkbox', fixed: 'left'}
                    , {
                        field: 'id',
                        fixed: 'left',
                        width: 80,
                        title: 'ID',
                        sort: true,
                        totalRowText: '合计：'
                    }
                    , {field: 'question', width: 160, title: '问题'}
                    , {field: 'optionA', title: '选项A', hide: 0, width: 160, edit: 'text'}
                    , {field: 'optionB', width: 160, title: '选项B', sort: true}
                    , {field: 'optionC', title: '选项C', Width: 160}
                    , {
                        field: 'optionD',
                        width: 160,
                        title: '选项D',
                        sort: true,
                    }
                    , {
                        field: 'type',
                        title: '类型',
                        width: 100,
                        sort: true,
                        templet: function (d) {
                            //得到当前行数据，并拼接成自定义模板
                            if (d.type == 1) {
                                //return '可用'
                                return 'SAS正向计分'
                            } else if (d.type == 2) {
                                return 'SAS反向计分'
                            } else {
                                return '未知类型'
                            }
                        }
                    },
                    , {field: 'status', title: '状态', width: 60,
                        templet: function (d) {
                            //得到当前行数据，并拼接成自定义模板
                            if (d.status == 0) {
                                //return '可用'
                                return '<a class="layui-btn  layui-btn-xs">可用</a>'
                            } else {
                                return '<a class="layui-btn layui-btn-disabled  layui-btn-xs">失效</a>'
                            }
                        }
                    }
                    , {field: 'createTime', title: '创建时间', width: 160}
                    , {field: 'createBy', title: '创建人', width: 100}
                    , {field: 'updateTime', title: '修改时间', width: 160}
                    , {field: 'updateBy', title: '修改人', width: 100}
                    , {
                        fixed: 'right',
                        title: '操作',
                        width: 125,
                        minWidth: 125,
                        toolbar: '#barDemo'
                    }
                ]]
            });
            /*数据添加的事件*/
            table.on('toolbar(test)', function (obj) {
                let $ = layui.$;
                let id = obj.config.id;
                let checkStatus = table.checkStatus(id);
                let data=checkStatus.data
                let othis = lay(this);
                if (obj.event === 'add') {
                    layer.open({
                        title: '添加',
                        type: 1,
                        area: ['40%', '80%'],
                        cancel:function (index,layero) {
                            $("#form-sub").hide();
                            layer.close(index);
                            return false;
                        },
                        content:$("#form-sub")
                    })
                }else if(obj.event==='delete'){
                    if(data.length<=0){
                        layer.msg('至少选择一行')
                    }else {
                        layer.confirm('确定删除吗？',function(index){
                            let idList=[];
                            for(let i=0;i<data.length;i++){
                                idList[i]=data[i].id;
                            }
                            $.ajax({
                                url:'/psych/sas/question/delete',
                                contentType : "application/json" ,
                                type:'POST',
                                data:JSON.stringify(idList),
                                success:function (ref){
                                    $("#form-upd").hide();
                                    layer.closeAll();
                                    table.reload("demo",{
                                        page:{
                                            curr:1,
                                        }
                                    });
                                    if(ref.status){
                                        layer.msg(ref.message,{icon:1});
                                    }else {
                                        layer.msg(ref.message,{icon:5});
                                    }
                                }
                            })
                            layer.close(index);
                        })
                    }

                }
            });
            //触发单元格工具事件
            table.on('tool(test)', function (obj) { // 双击 toolDouble
                let $ = layui.$;
                let data = obj.data;
                if (obj.event === 'del') {
                    layer.confirm('真的删除行么', function (index) {
                        let id=[];
                        id[0]=data.id;
                        $.ajax({
                            url: "/psych/sas/question/delete",
                            contentType : "application/json" ,
                            data:JSON.stringify(id),
                            type: 'POST',
                            success:function (ref){
                                table.reload("demo",{
                                    page:{
                                        curr:1,
                                    }
                                });
                                if(ref.status){
                                    layer.msg(ref.message,{icon:1});
                                }else {
                                    layer.msg(ref.message,{icon:5});
                                }
                            }
                        })
                        layer.close(index);
                    });
                } else if (obj.event === 'edit') {
                    form.val('update',{
                        "question":data.question,
                        "optionA":data.optionA,
                        "optionB":data.optionB,
                        "optionC":data.optionC,
                        "optionD":data.optionD,
                        "type":data.type,
                        "id":data.id
                    });
                    layer.open({
                        title: '编辑',
                        type: 1,
                        area: ['40%', '80%'],
                        cancel:function (index) {
                            $("#form-upd").hide();
                            layer.close(index);
                            return false;
                        },
                        content: $("#form-upd")
                    });
                }
            });
            //修改表单的提交事件
            form.on('submit(demo2)',function (data){
                if(data.field.status==='0'){
                    data.field.status=0;
                }else {
                    data.field.status=1;
                }
                $.ajax({
                    url:'${pageContext.request.contextPath}/sas/question/update',
                    dataType: 'JSON',
                    type:'POST',
                    data:data.field,
                    success:function (ref){
                        $("#form-upd").hide();
                        layer.closeAll();
                        table.reload("demo",{
                            page:{
                                curr:1,
                            }
                        });
                        if(ref.status){
                            layer.msg(ref.message,{icon:1});
                        }else {
                            layer.msg(ref.message,{icon:5});
                        }
                    }
                })
                return false;
            });
            //修改数据的前后的操作
            form.on('submit(demo1)',function (data){
                if(data.field.status==='0'){
                    data.field.status=0;
                }else {
                    data.field.status=1;
                }
                $.ajax({
                    url:'${pageContext.request.contextPath}/sas/question/insert',
                    dataType:'JSON',
                    type:'POST',
                    data:data.field,
                    success:function (ref) {
                        $("#form-sub").hide();
                        layer.closeAll();
                        table.reload("demo",{
                            page:{
                                curr:1,
                            }
                        });
                        if(ref.status){
                            layer.msg(ref.message,{icon:1});
                        }else {
                            layer.msg(ref.message,{icon:5});
                        }
                    }
                })
                return false;
            });
            //模糊查询的前后端操作
            form.on('submit(selectFormfilter)',function(data){
                table.reload("demo",{
                    url:'/psych/sas/question/fuzzyQuery',
                    where:{
                        question: data.field.question
                        , createBy: data.field.createBy
                    }
                })
                return false;
            })
            //触发表格复选框选择
            table.on('checkbox(test)', function (obj) {
                console.log(obj)
            });

            //触发表格单选框选择
            table.on('radio(test)', function (obj) {
                console.log(obj)
            });

            // 行单击事件
            table.on('row(test)', function (obj) {
                //console.log(obj);
                //layer.closeAll('tips');
            });
            // 行双击事件
            table.on('rowDouble(test)', function (obj) {
                console.log(obj);
            });

            // 单元格编辑事件
            table.on('edit(test)', function (obj) {
                let field = obj.field //得到字段
                    , value = obj.value //得到修改后的值
                    , data = obj.data; //得到所在行所有键值

                let update = {};
                update[field] = value;
                obj.update(update);
            });
        });
    </script>
</html>