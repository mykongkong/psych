<%--
  User: dpj
  Date: 2022/12/26
  Time: 23:47
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@include file="common.jsp" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>个人信息</title>
        <style type="text/css">
            /* 设置标题样式 */
            .content-title {
                margin: 6px 0 6px;
                height: 30px;
                font-size: 2rem;
                font-weight: 600;
                font-family: PingFangSC-Regular, "Noto Serif SC", serif;
                text-align: center;
                letter-spacing: 2px;
            }

            /* 设置头像图片样式 */
            #avatarImg {
                width: 100px;
                height: 100px;
            }

            /* 设置更改头像按钮样式 */
            #modifyImgBtn {
                margin-left: 20px;
            }
        </style>
    </head>
    <body>
        <div class="personalInfo-content"
             style="width: 1000px;margin-top: 30px;">
            <div class="content-title">
                <p id="signinTitle">个人信息</p>
            </div>
            <form class="layui-form" action="" id="saveForm" lay-filter="saveFilter"
                  style="margin-right: 40px; margin-top: 20px">
                <input hidden name="updateBy">
                <div class="layui-form-item">
                    <label class="layui-form-label">部门名称</label>
                    <div class="layui-input-inline">
                        <input type="text" name="deptid" disabled value="" class="layui-input"
                               style="background-color: #eeeeee;">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">用户身份</label>
                    <div class="layui-input-inline">
                        <input type="text" name="roleId" disabled value="" class="layui-input"
                               style="background-color: #eeeeee;">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">用户ID</label>
                    <div class="layui-input-inline">
                        <input type="text" name="userid" disabled value="" class="layui-input"
                               style="background-color: #eeeeee;">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">用户名</label>
                    <div class="layui-input-inline">
                        <input type="text" name="username" required lay-verify="required"
                               placeholder="请输入用户名" value=""
                               class="layui-input">
                    </div>
                    <label></label>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">邮箱</label>
                    <div class="layui-input-inline">
                        <input type="text" lay-verify="required|email" name="email" value=""
                               class="layui-input" placeholder="请输入邮箱">
                    </div>
                    <label></label>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">手机号</label>
                    <div class="layui-input-inline">
                        <input type="text" lay-verify="required|phonenumber" name="phonenumber"
                               value=""
                               class="layui-input" placeholder="请输入手机号">
                    </div>
                    <label></label>
                </div>
                <input type="hidden" id="avatar" name="avatar" value="">
                <div class="layui-form-item">
                    <label class="layui-form-label">头像</label>
                    <div class="layui-upload">
                        <img class="layui-upload-img" id="avatarImg" src="" alt="头像">
                        <button type="button" class="layui-btn" id="modifyImgBtn">更改头像</button>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" id="saveSubmitBtn" lay-submit
                                lay-filter="saveFilterSubmit">确定
                        </button>
                        <button type="button" id="backBtn" onclick="history.back();"
                                class="layui-btn layui-btn-primary">返回
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </body>
    <script>
        layui.use(['upload', 'jquery', 'form'], function () {
            let upload = layui.upload;
            let $ = layui.jquery;
            let form = layui.form;
            let avatarImg = $("#avatarImg");

            // 表单数据回显
            $.ajax({
                url: '${pageContext.request.contextPath}/sys/user/queryByName',
                type: 'POST',
                dataType: 'JSON',
                data: {username: "${sessionScope.SESSION_USER}"},
                success: function (userInfo) {
                    console.log(userInfo);
                    form.val('saveFilter', {
                        "deptid": userInfo.dept.deptName,
                        "roleId": userInfo.role.roleName,
                        "userid": userInfo.userid,
                        "username": userInfo.username,
                        "email": userInfo.email,
                        "phonenumber": userInfo.phonenumber,
                        "avatar": "${sessionScope.SESSION_AVATAR}",
                        "updateBy": "${sessionScope.SESSION_USER}"
                    });
                    // 加载头像
                    avatarImg.attr('src', "${sessionScope.SESSION_AVATAR}");
                }
            })

            // 更改头像
            upload.render({
                elem: '#modifyImgBtn',
                url: '${pageContext.request.contextPath}/sys/user/uploadAvatar',
                method: 'POST',
                field: 'multipartFile',
                accept: 'images',
                acceptMime: 'image/*',
                done: function (res) {
                    console.log(res);
                    if (res.status) {
                        $("#avatar").val(res.message);
                        avatarImg.attr('src', res.message);
                        // 刷新页面
                        location.reload();
                    }
                }
            })

            // 保存修改
            form.on('submit(saveFilterSubmit)', function (data) {
                console.log(data.field);
                $.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/sys/user/update',
                    dataType: 'JSON',
                    data: {
                        "userid": data.field.userid,
                        "username": data.field.username,
                        "email": data.field.email,
                        "phonenumber": data.field.phonenumber,
                        "avatar": data.field.avatar,
                        "updateBy": data.field.username
                    },
                    success: function (res) {
                        if (res.status) {
                            layer.msg('成功修改个人信息！', {icon: 6});
                        } else {
                            layer.msg(res.message, {icon: 7});
                        }
                    }
                });
                return false;  //组织表单跳转
            })
        })
    </script>
</html>