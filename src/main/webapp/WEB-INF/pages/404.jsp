<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@include file="../../common.jsp" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>404</title>
    <style type="text/css">
      body, html {
        padding: 0;
        margin: 0;
        font-family: 'Quicksand', sans-serif;
        font-weight: 400;
        overflow: hidden;
        background-color: #E2E2E2;
      }

      .errorInfo {
        width: 900px;
        text-align: center;
        margin: auto;
        padding-top: 100px;
      }

      .errorInfo h1 {
        color: #009688;
        font-size: 120px;
        padding: 0;
        margin: 0;
        font-weight: 700;
        letter-spacing: 20px;
      }

      .errorInfo h3 {
        color: #009688;
        padding: 0;
        margin: -10px 0 0 0;
        font-family: 'DIN Next LT Pro', sans-serif;
        font-size: 60px;
        font-weight: 400;
        letter-spacing: 10px;
      }

      .errorInfo button {
        width: 280px;
        padding: 0;
        margin-top: 60px;
        font-size: 20px;
        font-weight: bold;
        letter-spacing: 4px;
      }
    </style>
    <link rel="stylesheet" href="../../static/css/error.css">
  </head>
  <body>
    <div class="errorInfo">
      <h1>404</h1>
      <h3>Page Not Found!</h3>
      <button type="button" class="layui-btn layui-btn-radius layui-btn-fluid"
              onclick="history.back();">
        Back To Previous
      </button>
    </div>
  </body>
</html>