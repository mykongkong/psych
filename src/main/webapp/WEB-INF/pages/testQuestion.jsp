<%--
  Created by IntelliJ IDEA.
  User: 情雅圣
  Date: 2022/12/14
  Time: 16:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common.jsp" %>
<html>
    <style type="text/css">
        .top-div {
            padding-top: 100px;
            background-color: rgba(234, 245, 255, 0.96);
        }

        .top-div-d {
            width: 85%;
            padding: 30px 30px 0px 30px;
            margin: 0 auto;
            text-align: center;
            background-color: rgba(255, 255, 255, .7);
        }

        .top-div-h {
            padding: 5px;
            font-weight: bolder
        }

        .top-div-p {
            margin: 5px 0px 0px 0px;
            color: #a9a9aa;
        }

        #body {
            width: 50%;
            margin: auto;
            margin-top: 50px;
            border-radius: 10px;
            padding: 20px;
            box-shadow: 0px 0px 12px #cacaca;
            font-size: 18px;
        }

        .mid-question {
            display: none;
        }

        #question {
            margin: 30px 0px;
        }

        .layui-input-block {
            position: relative;
            left: -100px;
            font-size: 20px;
        }

        .layui-input-blockone {
            margin-top: 15px;
            background-color: #e2ecf6;
            border-radius: 10px;
            padding: 5px 0px 10px 10px;
        }

        .bottom-div {
            text-align: center;
            margin: auto;
            margin-top: 10px;
            width: 50%;
        }

        #next_btn {
            float: right;
        }

        #pre_btn {
            float: left;
        }

        #submit_btn {
            display: inline-block;
        }
    </style>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>测试题目</title>
    </head>
    <body>
        <div class="top-div">
            <div class="top-div-d">
                <h1 class="top-div-h">心理测试</h1>
                <p class="top-div-p">完成测试，获得专业的分析报告</p>
            </div>
        </div>
        <div id="body-head">
            <div id="body">
                <div id="progressBar">
                    <div class="layui-progress layui-progress-big" lay-filter="demo"
                         lay-showPercent="true">
                        <div class="layui-progress-bar" lay-percent="1%"></div>
                    </div>
                </div>
                <div id="question">
                    <input type="hidden" id="questionLen" value="${questionList.size()}"
                    <c:forEach items="${questionList}" var="question" varStatus="status">
                        <div id="id_${status.count}" class="mid-question">
                            <label class="question-title">${status.count}、${question.question}</label>
                            <form class="layui-form" id="question-form">
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <div class="layui-input-blockone">
                                            <input type="radio" id="A_${status.count}"
                                                   lay-filter="xz" name="${question.id}" value="A"
                                                   title="${question.optionA}">
                                        </div>
                                        <div class="layui-input-blockone">
                                            <input type="radio" id="B_${status.count}"
                                                   lay-filter="xz" name="${question.id}" value="B"
                                                   title="${question.optionB}">
                                        </div>
                                        <div class="layui-input-blockone">
                                            <input type="radio" id="C_${status.count}"
                                                   lay-filter="xz" name="${question.id}" value="C"
                                                   title="${question.optionC}">
                                        </div>
                                        <div class="layui-input-blockone">
                                            <input type="radio" id="D_${status.count}"
                                                   lay-filter="xz" name="${question.id}" value="D"
                                                   title="${question.optionD}">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <div class="bottom-div">
                <button id="pre_btn" type="button" class="layui-btn  layui-bg-blue bottom-btn-left">
                    <i class="layui-icon layui-icon-left"></i>
                </button>
                <button id="submit_btn" type="submit"
                        class="layui-btn  layui-btn-warm layui-btn-radius bottom-submit">提交
                </button>
                <button id="next_btn" type="button"
                        class="layui-btn  layui-bg-blue bottom-btn-right">
                    <i class="layui-icon layui-icon-right"></i>
                </button>
            </div>
        </div>
    </body>
    <script>
        layui.use(['form', 'layer', 'element', 'jquery'], function () {
            let $ = layui.$;
            let form = layui.form;
            let element = layui.element;
            let layer = layui.layer;
            let questionLen = $("#questionLen").val();
            let index = 0;
            let resultList = [];
            form.on('radio(xz)', function (data) {
                /*定义一个对象存储一道题答案的信息*/
                let result = new Object();
                /*获取答案*/
                result.answer = data.value;
                /*获取题号*/
                result.questionId = data.elem.name;
                /*获取题目编号*/
                index = parseInt(data.elem.id.split("_")[1]);
                //将答案添加道resultList中
                if (index <= questionLen) {
                    resultList[index - 1] = result;
                } else {
                    resultList.push(result);
                }
                if (resultList.length === questionLen) {
                    $("#submit_btn").show();
                } else {
                    if (index !== questionLen) {
                        $("#question").hide()
                        $('#id_' + index).hide();
                        $('#id_' + (index + 1)).show();
                    }
                }
                element.progress('demo', Math.ceil(index / questionLen * 100) + '%')
            });
            //左按钮事件
            $("#pre_btn").click(function () {
                console.log(resultList);
                //没有做完题
                if (resultList.length !== questionLen) {
                    if (index > 0) {
                        $("#id_" + (index + 1)).hide();
                        $("#id_" + index).show();
                        if (index === 1) {
                            $("#question").show();
                        } else {
                            $("#question").hide();
                        }
                        index--;
                    }
                    //做完题
                } else {
                    if (index > 1) {
                        $("#id_" + index).hide();
                        $("#id_" + (index - 1)).show();
                        if (index === 2) {
                            $("#question").show();
                        }
                        index--;
                    }
                }
            });
            //右按钮事件
            $("#next_btn").click(function () {
                console.log(resultList);
                if (resultList.length !== questionLen) {//没做完时
                    if (index < resultList.length) {
                        index++;
                        if (index === 1) {
                            $("#question").hide();
                        }
                        $('#id_' + index).hide();
                        $('#id_' + (index + 1)).show();
                    }
                } else {//做完时
                    if (index < questionLen) {
                        if (index === 1) {
                            $("#question").hide();
                        }
                        $('#id_' + index).hide();
                        $('#id_' + (index + 1)).show();
                        index++;
                    }
                }
            });
            //提交按钮事件
            $("#submit_btn").click(function () {
                $.ajax({
                    url: '${pageContext.request.contextPath}/question/insertAll',
                    contentType: "application/json;charset=UTF-8",
                    type: 'POST',
                    data: JSON.stringify(resultList),
                    success: function (ref) {
                        window.location.href = "${pageContext.request.contextPath}" + ref.message;
                    }
                })
            })
        })
    </script>
</html>