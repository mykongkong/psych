<%--
  User: dpj
  Date: 2022/11/27
  Time: 11:36
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<link rel="stylesheet" media="all"
      href="${pageContext.request.contextPath}/static/layui/css/layui.css">
<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script src="${pageContext.request.contextPath}/static/echarts/echarts.js"></script>
<script src="${pageContext.request.contextPath}/static/js/html2canvas.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jspdf.debug.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jq.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jquery-3.6.1.js"></script>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>