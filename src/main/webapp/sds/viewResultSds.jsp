<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: dpj
  Date: 2022/12/5
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common.jsp" %>
<html>
    <head>
        <title>抑郁测试-测试结果</title>
    </head>
    <body>
        <%--模糊查询form表单--%>
        <form class="layui-form" style="margin-top: 10px">
            <div class="layui-inline">
                <label class="layui-form-label">姓名</label>
                <div class="layui-input-inline">
                    <input type="text" name="name" placeholder="请输入测试名称" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">测试ID</label>
                <div class="layui-input-inline">
                    <select name="testPlanId">
                        <option value=""></option>
                        <option value="6">6</option>
                        <option value="312">312</option>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <div class="layui-input-inline">
                    <button class="layui-btn" lay-submit lay-filter="queryVague">查询</button>
                    <button type="reset" class="layui-btn layui-btn-primary" id="myButton">重置
                    </button>
                </div>
            </div>
        </form>
        <%--数据渲染的容器--%>
        <table class="layui-hide" id="test" lay-filter="test"></table>
        <%--查看结果展示--%>
        <div id="result" style="width: 100%;height: 100%; display: none;">
            <div>
                <div>
                    <div id="myPdf" align="auto">
                        <div style="background-color:#fcf4ed;border-radius:40px 40px 0 0;z-index: 2;padding: 40px 20px 20px 20px;color: #41464B">
                            <h1 style="text-align:center;line-height: 60px;font-size: 30px;font-weight: 600;color: #7a2114;">
                                测评报告预览</h1>
                            <div id="div1"
                                 style="text-align: center;font-weight:bolder;font-size:24px;"></div>
                            <div id="div2" style="width:100%;height:400px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <%--表头工具栏--%>
    <script type="text/html" id="toolbar-head">
        <div>
            <button type="button" class="layui-btn layui-btn-danger" lay-event="delete"><i
                    class="layui-icon layui-icon-delete"></i>删除
            </button>
        </div>
    </script>
    <%--行内工具栏--%>
    <script type="text/html" id="toolbar-line">
        <a class="layui-btn layui-btn-xs" lay-event="look">查看</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
    <script type="text/javascript"
            src="https://cdn.bootcss.com/echarts/4.2.1-rc1/echarts.min.js"></script>
    <script>
        layui.use(['table', 'form', 'jquery', 'layer'], function () {
            let table = layui.table;
            let form = layui.form;
            let $ = layui.$;
            let layer = layui.layer;
            //数据的渲染
            table.render({
                elem: '#test',
                url: '${pageContext.request.contextPath}/sds/result/queryAll',
                toolbar: '#toolbar-head',//表头左侧工具栏
                defaultToolbar: ['filter', 'print', 'exports'],//表头工右侧工具栏
                height: 'full-90',
                page: true,//开启分页
                cols: [[//数据渲染,二维数组
                    {type: 'checkbox', fixed: 'left'}
                    , {field: 'id', fixed: 'left', title: 'ID', sort: true, width:60}
                    , {field: 'name', title: '姓名', width:100}
                    , {field: 'phone', title: '电话', width:120}
                    , {field: 'create_time', title: '测试时间'}
                    , {field: 'forward', title: '正向得分', sort: true}
                    , {field: 'inversion', title: '反向得分', sort: true}
                    , {field: 'testPlanId', title: '测试计划ID', sort: true}
                    , {fixed: 'right', title: '操作', toolbar: '#toolbar-line', width: 140}
                ]]
                , done: function () {
                    // 修改表格样式
                    $('.layui-cell-1-0-0').css({'overflow': 'visible'})
                    $('.laytable-cell-1-0-0').css({'padding-top': '6px'})
                    $('thead th').css({'text-align': 'center'})
                    $('table tr td').css({'text-align': 'center'})
                }
            });
            //表头按钮事件
            table.on('toolbar(test)', function (obj) {
                let id = obj.config.id;
                let checkStatus = table.checkStatus(id);//获取数据表格中的复选框
                let data = checkStatus.data;
                if (obj.event === 'delete') {
                    if (data.length <= 0) {
                        layer.msg('至少要选择一行');
                    } else {
                        layer.confirm("确认要删除吗？", function (index) {
                            let idList = [];
                            for (let i = 0; i < data.length; i++) {
                                idList[i] = data[i].testerId;
                            }
                            //console.log(idList)
                            $.ajax({
                                url: '${pageContext.request.contextPath}/sds/result/delete',
                                type: 'POST',
                                contentType: "application/json",
                                data: JSON.stringify(idList),
                                success: function (ref) {
                                    table.reload('test', {
                                        page: {
                                            curr: 1
                                        }
                                    });
                                }
                            })
                            layer.close(index);
                        })
                    }
                }
            });
            //行内按钮事件
            table.on('tool(test)', function (obj) {
                let data = obj.data;
                if (obj.event === 'del') {//删除按钮的事件
                    layer.confirm('确认删除吗？', function (index) {
                        let id = [];
                        id[0] = data.testerId;
                        //console.log(id)
                        $.ajax({
                            url: '${pageContext.request.contextPath}/sds/result/delete',
                            type: 'POST',
                            contentType: "application/json",
                            data: JSON.stringify(id),
                            success: function (ref) {
                                table.reload('test', {
                                    page: {
                                        curr: 1
                                    }
                                })
                            }
                        })
                        layer.close(index)
                    })
                } else if (obj.event === 'look') {//查看按钮
                    layer.open({
                        title: '抑郁测试结果图示',
                        type: 1,
                        area: ['571px', '620px'],
                        content: $('#result'),
                        cancel: function (index, layero) {
                            $("#result").hide();
                        }
                    });
                    $.ajax({
                        url: '${pageContext.request.contextPath}/sds/result/queryID',
                        dataType: 'JSON',
                        type: 'POST',
                        data: {id: obj.data.id},
                        success: function (ref) {
                            let forward = ref.data[0].forward;
                            let inversion = ref.data[0].inversion;
                            let score = forward + inversion;
                            let MyEcharts = echarts.init(document.getElementById('div2'));
                            let string = "";
                            if (score < 50) {
                                string = "您的测试结果为无抑郁症状"
                            } else if (score <= 60) {
                                string = "您的测试结果为轻度抑郁"
                            } else if (score <= 70) {
                                string = "您的测试结果为中度抑郁"
                            } else if (score > 70) {
                                string = "您的测试结果为重度抑郁"
                            }
                            $('#div1').html(string);
                            let option = {
                                title: {
                                    text: 'SDS性格测试',
                                    left: 'center',
                                },
                                colorBy: 'data',
                                legend: {
                                    orient: 'vertical',
                                    left: 'left'
                                },
                                tooltip: {//提示框组件
                                    trigger: 'item'
                                },
                                series: [
                                    {
                                        name: 'SDS性格占比',
                                        type: 'pie',
                                        data: [
                                            {
                                                value: forward,
                                                name: '正向得分',
                                                itemStyle: {color: '#3e5c8f'}
                                            },
                                            {
                                                value: inversion,
                                                name: '反向得分',
                                                itemStyle: {color: '#668d3c'}
                                            },
                                        ],
                                        emphasis: {
                                            itemStyle: {
                                                shadowBlur: 10,
                                                shadowOffsetX: 0,
                                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                                            }
                                        }
                                    }
                                ]
                            }
                            MyEcharts.setOption(option);
                        },
                    })
                }
            });
            //模糊查询的功能
            form.on('submit(queryVague)', function (data) {
                table.reload("test", {
                    url: '${pageContext.request.contextPath}/sds/result/fuzzyQuery',
                    where: {
                        name: data.field.name
                        , testPlanId: data.field.testPlanId
                    }
                })
                return false;
            });
        });
    </script>
</html>