<%--
  User: dpj
  Date: 2022/11/26
  Time: 10:45
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@include file="../common.jsp" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>抑郁测试-题目管理</title>
    </head>
    <body>
        <%--顶部搜索框--%>
        <form class="layui-form" action="" style="margin-top: 10px">
            <div class="layui-inline" style="width: 320px">
                <label class="layui-form-label">问题：</label>
                <div class="layui-input-inline">
                    <input type="text" name="question" autocomplete="off"
                           placeholder="请输入问题"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-inline" style="width: 320px; margin-left: -10px">
                <label class="layui-form-label">创建人：</label>
                <div class="layui-input-inline">
                    <input type="text" name="createBy" autocomplete="off" placeholder="请输入创建人"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <div class="layui-input-inline" style="width: 150px; margin-left: 30px">
                    <button class="layui-btn" lay-submit lay-filter="searchFilter">搜索</button>
                    <button type="reset" class="layui-btn layui-btn-primary" id="resetBtn">重置
                    </button>
                </div>
            </div>
        </form>
        <%--数据表格--%>
        <table id="showTable" lay-filter="tableFilter"></table>
        <%--表头工具条之添加功能弹出层--%>
        <form class="layui-form" action="" id="insertLayer"
              style="display: none;margin-right: 40px;margin-top: 20px">
            <input hidden name="createBy">
            <input hidden name="updateBy">
            <div class="layui-form-item">
                <label class="layui-form-label">问题描述</label>
                <div class="layui-input-block">
                    <input type="text" name="question" required lay-verify="required"
                           placeholder="请输入问题描述" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选项A</label>
                <div class="layui-input-block">
                    <input type="text" name="optionA" required lay-verify="required"
                           placeholder="请输入选项A的内容" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选项B</label>
                <div class="layui-input-block">
                    <input type="text" name="optionB" required lay-verify="required"
                           placeholder="请输入选项B的内容" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选项C</label>
                <div class="layui-input-block">
                    <input type="text" name="optionC" required lay-verify="required"
                           placeholder="请输入选项C的内容" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选项D</label>
                <div class="layui-input-block">
                    <input type="text" name="optionD" required lay-verify="required"
                           placeholder="请输入选项D的内容" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">问题类型</label>
                <div class="layui-input-block">
                    <select name="type" lay-verify="required">
                        <option value="1">正向计分</option>
                        <option value="2">反向计分</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-block">
                    <input type="checkbox" checked="" name="status" lay-skin="switch"
                           lay-filter="switchStatus" lay-text="可用|失效">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="insertFilter">确定</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
        <%--行内工具条之修改功能弹出层--%>
        <form class="layui-form" action="" id="updateLayer" lay-filter="updateFilter"
              style="display: none;margin-right: 40px;margin-top: 20px">
            <div class="layui-form-item">
                <input hidden name="id">
                <input hidden name="updateBy">
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">问题描述</label>
                <div class="layui-input-block">
                    <input type="text" name="question" required lay-verify="required"
                           placeholder="请输入问题描述" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选项A</label>
                <div class="layui-input-block">
                    <input type="text" name="optionA" required lay-verify="required"
                           placeholder="请输入选项A的内容" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选项B</label>
                <div class="layui-input-block">
                    <input type="text" name="optionB" required lay-verify="required"
                           placeholder="请输入选项B的内容" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选项C</label>
                <div class="layui-input-block">
                    <input type="text" name="optionC" required lay-verify="required"
                           placeholder="请输入选项C的内容" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选项D</label>
                <div class="layui-input-block">
                    <input type="text" name="optionD" required lay-verify="required"
                           placeholder="请输入选项D的内容" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">问题类型</label>
                <div class="layui-input-block">
                    <select name="type" lay-verify="required">
                        <option value="1">正向计分</option>
                        <option value="2">反向计分</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-block">
                    <input type="checkbox" checked id="status" name="status" lay-skin="switch"
                           lay-filter="switchStatus" lay-text="可用|失效">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="updateBtnFilter">确定</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
    </body>
    <%--表头工具条--%>
    <script type="text/html" id="headerToolbar">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-sm" lay-event="insertQuestion">
                <i class="layui-icon layui-icon-add-1"></i>添加
            </button>
            <button class="layui-btn layui-btn-sm layui-btn-danger" lay-event="deleteQuestions">
                <i class="layui-icon layui-icon-delete"></i>删除
            </button>
        </div>
    </script>
    <%--行内工具条--%>
    <script type="text/html" id="inlineToolbar">
        <a class="layui-btn layui-btn-xs" lay-event="updateQuestion">修改</a>
        <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="deleteQuestion">删除</a>
    </script>
    <%--状态按钮映射函数--%>
    <script type="text/html" id="deptStatus">
        {{# if (d.status === '0'){}}
        <button class="layui-btn layui-btn-xs">可用</button>
        {{# }else {}}
        <button class="layui-btn layui-btn-xs layui-btn-danger">失效</button>
        {{# }}}
    </script>
    <%--layui交互函数--%>
    <script>
        layui.use(['table', 'layer', 'jquery', 'form'], function () {
            let table = layui.table;
            let layer = layui.layer;
            let $ = layui.jquery;
            let form = layui.form;

            // 数据表格分页
            let showTable = table.render({
                elem: '#showTable'
                , height: '550'
                , url: '${pageContext.request.contextPath}/sds/question/queryAll' //数据接口
                , toolbar: '#headerToolbar' //开启表头工具条，并为其绑定左侧模板
                , defaultToolbar: ['filter', 'exports', 'print']
                , title: '抑郁测试题目表'
                , cellMinWidth: 80
                , page: true //开启分页
                , limit: 10
                , limits: [5, 10, 15]
                , cols: [
                    [ //表头
                        {type: 'checkbox', fixed: 'left'}
                        , {field: 'id', title: 'ID', width: 80, sort: true, fixed: 'left'}
                        , {field: 'question', title: '问题', width: 120, sort: true}
                        , {field: 'optionA', title: '选项A', width: 120}
                        , {field: 'optionB', title: '选项B', width: 120}
                        , {field: 'optionC', title: '选项C', width: 120}
                        , {field: 'optionD', title: '选项D', width: 120}
                        , {
                        field: 'type', title: '类型', width: 120,
                        templet: function (d) {
                            if (d.type === 1) {
                                return 'SDS正向计分'
                            } else if (d.type === 2) {
                                return 'SDS反向计分'
                            } else {
                                return '未知类型'
                            }
                        }
                    }
                        , {
                        field: 'status', title: '状态', width: 80,
                        templet: function (d) {
                            if (d.status === 0) {
                                return '<a class="layui-btn  layui-btn-xs">可用</a>'
                            } else {
                                return '<a class="layui-btn layui-btn-disabled  layui-btn-xs">失效</a>'
                            }
                        }
                    }
                        , {field: 'createTime', title: '创建时间', width: 170}
                        , {field: 'createBy', title: '创建人', width: 100}
                        , {field: 'updateTime', title: '修改时间', width: 170}
                        , {field: 'updateBy', title: '修改人', width: 100}
                        , {fixed: 'right', title: '操作', width: 130, toolbar: '#inlineToolbar'}
                    ]
                ]
                , done: function () {
                    // 修改表格样式
                    $('.layui-cell-1-0-0').css({'overflow': 'visible'})
                    $('.laytable-cell-1-0-0').css({'padding-top': '6px'})
                    $('.laytable-cell-1-0-8:not(:first)').css({'padding-top': '4px'})
                    $('thead th').css({'text-align': 'center'})
                    $('table tr td').css({'text-align': 'center'})
                }
            });

            // 顶部搜索框模糊查询
            form.on('submit(searchFilter)', function (data) {
                showTable.reload({
                    url: '${pageContext.request.contextPath}/sds/question/fuzzyQuery',
                    where: {
                        question: data.field.question,
                        createBy: data.field.createBy
                    },
                    page: {curr: 1}
                });
                return false;  // 阻止表单跳转
            })

            // 重置模糊查询表单
            $('#resetBtn').click(function () {
                showTable.reload({
                    where: {
                        question: '',
                        createBy: ''
                    },
                    page: {curr: 1}
                });  // 刷新table
            })

            // 监听表头左侧工具条事件
            table.on('toolbar(tableFilter)', function (obj) {
                let checkStatus = table.checkStatus('showTable');
                let data = checkStatus.data;
                switch (obj.event) {
                    case 'insertQuestion':
                        document.getElementById('insertLayer').reset();
                        layer.open({
                            type: 1,
                            area: '500px',
                            title: '添加问题',
                            skin: 'layui-layer-molv', //弹出层皮肤样式
                            maxmin: true,  //显示最大化和最小化按钮
                            content: $('#insertLayer'),
                            end: function () {  //防止弹出层关闭后显示在父页面中
                                $('#insertLayer').css('display', 'none');
                            }
                        })
                        break;
                    case 'deleteQuestions':
                        if (data.length === 0) {
                            layer.msg('请选择至少一个问题！', {icon: 7});
                        } else {
                            layer.confirm('你确定要删除吗？',
                                {btn: ['确定', '取消']},
                                function (index) {
                                    let ids = [];
                                    for (let i = 0; i < data.length; i++) {
                                        ids.push(data[i].id);
                                    }
                                    $.ajax({
                                        url: '${pageContext.request.contextPath}/sds/question/deleteByIds',
                                        method: 'POST',
                                        data: 'ids=' + ids, // jquery获取表单内容
                                        success: function () {
                                            layer.msg('成功删除所选问题！', {icon: 6});
                                            showTable.reload({
                                                page: {curr: 1}
                                            });  //删除数据后刷新table
                                        }
                                    })
                                    layer.close(index)
                                })
                        }
                        break;
                }
            });

            // 表头添加功能弹出层
            form.on('submit(insertFilter)', function (data) {
                if (data.field.status === 'on') {
                    data.field.status = 0;
                } else {
                    data.field.status = 1;
                }
                data.field.createBy = '${sessionScope.SESSION_USER}';
                data.field.updateBy = '${sessionScope.SESSION_USER}';
                // 提交数据表单
                $.ajax({
                    url: '${pageContext.request.contextPath}/sds/question/insert',
                    method: 'POST',
                    dataType: 'JSON',
                    data: data.field,  //jquery获取表单数据
                    success: function (res) {
                        if (res.status) {
                            layer.closeAll();
                            showTable.reload({
                                page: {curr: 1}
                            });  //添加数据后刷新table
                            layer.msg('成功添加一个问题！', {icon: 6});
                        } else {
                            layer.msg(res.message, {icon: 7});
                        }
                    }
                })
                return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
            })

            // 监听表格右侧行内工具条事件
            table.on('tool(tableFilter)', function (obj) {
                let data = obj.data;  // 获取当前行数据
                switch (obj.event) {
                    case 'updateQuestion':
                        form.val('updateFilter', {
                            'id': data.id,
                            'question': data.question,
                            'optionA': data.optionA,
                            'optionB': data.optionB,
                            'optionC': data.optionC,
                            'optionD': data.optionD,
                            'type': data.type,
                            'status': data.status,
                            'updateBy': '${sessionScope.SESSION_USER}'
                        });  // 把当前行信息回显到弹出层表单
                        layer.open({
                            type: 1,
                            area: '500px',
                            title: '修改问题',
                            skin: 'layui-layer-molv', //弹出层皮肤样式
                            maxmin: true,  //显示最大化和最小化按钮
                            content: $('#updateLayer'),
                            end: function () {  //防止弹出层关闭后显示在父页面中
                                $('#updateLayer').css('display', 'none');
                            }
                        })
                        break;
                    case 'deleteQuestion':
                        layer.confirm('你确定要删除吗？',
                            {btn: ['确定', '取消']},
                            function (index) {
                                $.ajax({
                                    url: '${pageContext.request.contextPath}/sds/question/delete',
                                    method: 'POST',
                                    data: {id: obj.data.id}, // jquery获取表单内容
                                    success: function () {
                                        layer.msg('成功添加一个问题！', {icon: 6});
                                        showTable.reload({
                                            page: {curr: 1}
                                        });  //删除数据后刷新table
                                    }
                                })
                                layer.close(index)
                            })
                        break;
                }
            })

            // 行内工具条修改功能弹出层
            form.on('submit(updateBtnFilter)', function (data) {
                if (data.field.status === 'on') {
                    data.field.status = 0;
                } else {
                    data.field.status = 1;
                }
                console.log(data.field);
                // 提交数据表单
                $.ajax({
                    url: '${pageContext.request.contextPath}/sds/question/update',
                    method: 'POST',
                    dataType: 'JSON',
                    data: data.field,  //jquery获取表单数据
                    success: function (res) {
                        if (res.status) {
                            layer.close(layer.index);
                            showTable.reload({
                                page: {curr: 1}
                            });  //修改数据后刷新table
                            layer.msg('成功修改一个问题！', {icon: 6});
                        } else {
                            layer.msg(res.message, {icon: 7});
                        }
                    }
                })
                return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
            })
        })
    </script>
</html>