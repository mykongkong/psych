<%--
  User: dpj
  Date: 2022/12/12
  Time: 9:38
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@include file="common.jsp" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>修改密码</title>
        <link rel="stylesheet" href="static/css/modifypwd.css">
    </head>
    <body>
        <div class="modify-content">
            <div class="content-title">
                <p id="modify-title">修改密码</p>
            </div>
            <form id="modifypwdForm">
                <div class="el-form-item">
                    <div class="el-input">
                        <i class="layui-icon layui-icon-username el-input__prefix"></i>
                        <input type="text" autocomplete="off" id="userAccount"
                               placeholder="用户名/邮箱/手机号" name="userAccount"
                               class="el-input__inner">
                    </div>
                    <div class="el-form-item__error" id="accountError"></div>
                </div>
                <div class="el-form-item">
                    <div class="el-input">
                        <i class="layui-icon layui-icon-password el-input__prefix"></i>
                        <input type="password" autocomplete="off" id="oldPwd"
                               placeholder="旧密码" name="oldPwd"
                               class="el-input__inner" value="">
                        <span class="el-input__suffix">
                            <img src="static/img/pwdHide.png" alt="pwd" class="eye">
                        </span>
                    </div>
                    <div class="el-form-item__error" id="oldPwdError"></div>
                </div>
                <div class="el-form-item">
                    <div class="el-input">
                        <i class="layui-icon layui-icon-password el-input__prefix"></i>
                        <input type="password" autocomplete="off" id="newPwd1"
                               placeholder="新密码" name="newPwd1"
                               class="el-input__inner" value="">
                        <span class="el-input__suffix">
                            <img src="static/img/pwdHide.png" alt="pwd" class="eye">
                        </span>
                    </div>
                    <div class="el-form-item__error" id="newPwdError1"></div>
                </div>
                <div class="el-form-item">
                    <div class="el-input">
                        <i class="layui-icon layui-icon-password el-input__prefix"></i>
                        <input type="password" autocomplete="off" id="newPwd2"
                               placeholder="确认新密码" name="newPwd2"
                               class="el-input__inner" value="">
                        <span class="el-input__suffix">
                            <img src="static/img/pwdHide.png" alt="pwd" class="eye">
                        </span>
                    </div>
                    <div class="el-form-item__error" id="newPwdError2"></div>
                </div>
                <div class="el-form-btn">
                    <button type="button" id="cancelBtn">取消</button>
                    <button type="button" id="confirmBtn">确定</button>
                </div>
            </form>
        </div>
    </body>
    <%--切换密码框小眼睛样式--%>
    <script>
        let eye = document.getElementsByClassName('eye');
        let oldPwd = document.getElementById('oldPwd');
        let newPwd1 = document.getElementById('newPwd1');
        let newPwd2 = document.getElementById('newPwd2');
        let flag = [0, 0, 0];
        // 旧密码小眼睛
        eye[0].onclick = function () {
            if (flag[0] === 0) {  //当眼睛是关闭的时候，点击后：
                oldPwd.type = "text";  //密码框样式换成文本，即可显示密码
                eye[0].src = "static/img/pwdShow.png";  //改为眼睛打开的图片
                flag[0] = 1;
            } else {
                oldPwd.type = "password";  //文本框换成密码框
                eye[0].src = "static/img/pwdHide.png";  //改为眼睛关闭的图片
                flag[0] = 0;
            }
        }
        // 新密码1小眼睛
        eye[1].onclick = function () {
            if (flag[1] === 0) {  //当眼睛是关闭的时候，点击后：
                newPwd1.type = "text";  //密码框样式换成文本，即可显示密码
                eye[1].src = "static/img/pwdShow.png";  //改为眼睛打开的图片
                flag[1] = 1;
            } else {
                newPwd1.type = "password";  //文本框换成密码框
                eye[1].src = "static/img/pwdHide.png";  //改为眼睛关闭的图片
                flag[1] = 0;
            }
        }
        // 新密码2小眼睛
        eye[2].onclick = function () {
            if (flag[2] === 0) {  //当眼睛是关闭的时候，点击后：
                newPwd2.type = "text";  //密码框样式换成文本，即可显示密码
                eye[2].src = "static/img/pwdShow.png";  //改为眼睛打开的图片
                flag[2] = 1;
            } else {
                newPwd2.type = "password";  //文本框换成密码框
                eye[2].src = "static/img/pwdHide.png";  //改为眼睛关闭的图片
                flag[2] = 0;
            }
        }
    </script>
    <%--密码校验--%>
    <script>
        // 密码校验规则
        let passwordReg =
            /^(?=.{8})(?=.*?[a-zA-Z])(?=.*?\d)(?=.*?[*?!&￥$%^#,./@";:><\[\]}{\-=+_\\|》《。，、？’‘“”~ `]).*$/;
        let lengthReg = /^(?=.{8}).*$/;
        let letterReg = /^(?=.*?[a-zA-Z]).*$/;
        let numberReg = /^(?=.*?\d).*$/;
        let characterReg = /^(?=.*?[*?!&￥$%^#,./@";:><\[\]}{\-=+_\\|》《。，、？’‘“”~ `]).*$/;

        // 关键信息是否正确填写
        let accountFlag = false;
        let oldPwdFlag = false;
        let newPwdFlag1 = false;
        let newPwdFlag2 = false;

        // 用户名信息是否已经填写
        let accountText = $("#userAccount");
        let accountError = $("#accountError");
        document.addEventListener("mousedown", (e) => {
            if (!accountText.val()) {
                if (!e.path.includes(accountText)) {
                    accountError.innerText = "请输入帐户";
                }
            }
        })

        // 旧密码是否已经填写
        $("#oldPwd").on('blur', function () {
            let oldPwdBox = $("#oldPwd");
            if (oldPwdBox.val() !== null) {
                oldPwdFlag = true;
                console.log(oldPwdFlag)
            }
        })

        // 密码框选择器
        let oldPwdText = document.getElementById("oldPwd").value;
        let oldPwdError = document.getElementById("oldPwdError");
        let newPwdText1 = document.getElementById("newPwd1").value;
        let newPwdError1 = document.getElementById("newPwdError1");
        let newPwdText2 = document.getElementById("newPwd2").value;
        let newPwdError2 = document.getElementById("newPwdError2");

        // 新密码第一次校验


        // 新密码第二次校验


        // 表单提交
        let inner = document.getElementsByClassName("el-input__inner");
        $("#confirmBtn").on("click", function () {
            $.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/sys/user/modifyPwd",
                dataType: "JSON",
                data: {
                    "account": inner[0].value,
                    "oldPwd": inner[1].value,
                    "newPwd": inner[2].value
                },
                success: function (res) {
                    if (res.status) {
                        window.location.href = "signin.jsp";
                    }
                }
            })
        })

        // 点击取消按钮返回登录界面
        $("#cancelBtn").on("click", function (){
            window.location.href = "signin.jsp";
        })
    </script>
</html>