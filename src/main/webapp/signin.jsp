<%--
  User: dpj
  Date: 2022/12/12
  Time: 9:29
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@include file="common.jsp" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>登录页面</title>
        <link rel="stylesheet" href="static/css/signin.css">
    </head>
    <body>
        <div class="signin-content">
            <div class="content-title">
                <p id="signinTitle">psych帐户登录</p>
            </div>
            <form id="signinForm">
                <div class="el-form-item">
                    <div class="el-input">
                        <i class="layui-icon layui-icon-username el-input__prefix"></i>
                        <input type="text" autocomplete="off" id="userAccount"
                               placeholder="用户名/邮箱/手机号" name="userAccount"
                               class="el-input__inner" value="${cookie.accountCookie.value}">
                    </div>
                    <div class="el-form-item__error" id="accountError"></div>
                </div>
                <div class="el-form-item">
                    <div class="el-input">
                        <i class="layui-icon layui-icon-password el-input__prefix"></i>
                        <input type="password" autocomplete="off" id="password"
                               placeholder="密码" name="password"
                               class="el-input__inner" value="${cookie.pwdCookie.value}">
                        <span class="el-input__suffix">
                            <img src="static/img/pwdHide.png" alt="pwd" id="eye">
                        </span>
                    </div>
                    <div class="el-form-item__error" id="pwdError"></div>
                </div>
                <div class="rememberMe-container">
                    <input type="checkbox" name="rememberMe" id="rememberMe-checkbox" checked/>
                    <span id="rememberMe-text">30天内记住我</span>
                </div>
                <div class="el-form-btn">
                    <button type="button" id="signinBtn">登录</button>
                </div>
            </form>
            <div class="signin-link-container">
                <div class="signin-link">
                    <a href="register.jsp">立即注册</a>
                    <span id="splitLine">|</span>
                    <a href="modifypwd.jsp">修改密码</a>
                </div>
            </div>
        </div>
    </body>
    <%--切换密码框小眼睛样式--%>
    <script>
        let pwd = document.getElementById("password");
        let eye = document.getElementById("eye");
        let flag = 0;  //默认眼睛关闭
        eye.onclick = function () {
            if (flag === 0) {  //当眼睛是关闭的时候，点击后：
                pwd.type = "text";  //密码框样式换成文本，即可显示密码
                eye.src = "static/img/pwdShow.png";  //改为眼睛打开的图片
                flag = 1;
            } else {
                pwd.type = "password";  //文本框换成密码框
                eye.src = "static/img/pwdHide.png";  //改为眼睛关闭的图片
                flag = 0;
            }
        }
    </script>
    <%--设置输入框提示信息显示条件--%>
    <script>
        let box = document.getElementsByClassName("el-input");
        let text = document.getElementsByClassName("el-input__inner");
        let error = document.getElementsByClassName("el-form-item__error");
        let innerClickFlag = [0, 0];  //必须先点击一次输入框内，才判断是否显示提示信息
        let index = [0, 1];

        // 修改用户信息输入框状态
        text[0].onclick = function () {
            if (innerClickFlag[0] === 0) {
                innerClickFlag[0] = 1;
            }
        }

        // 必须先点击一次输入框内，才判断是否显示提示信息
        document.addEventListener("mousedown", (e) => {
            if (innerClickFlag[0] === 1 && !text[0].value) {
                if (!e.path.includes(box[0])) {
                    // 显示提示信息
                    error[0].innerText = "请输入帐户";
                    // 修改输入框悬浮事件
                    box[0].onmouseover = function () {
                        box[0].style.cssText = "border: 1px solid #F56C6C";
                    }
                    box[0].onmouseout = function () {
                        box[0].style.cssText = "border: 1px solid #F56C6C";
                    }
                    // 设置输入框边框样式
                    box[0].style.cssText = "border: 1px solid #F56C6C";
                }
            }
        })

        // 给帐户输入框绑定keyup事件
        box[0].addEventListener("keyup", function () {
            if (text[0].value !== null) {
                // 关闭提示信息
                error[0].innerText = "";
                // 启用输入框悬浮事件
                box[0].onmouseover = function () {
                    box[0].style.cssText = "border: 1px solid #3498DB";
                }
                box[0].onmouseout = function () {
                    box[0].style.cssText = "border: 1px solid #B3B6B7";
                }
                // 设置输入框边框样式
                box[0].style.cssText = "border: 1px solid #B3B6B7";
            }
        })

        // 修改密码输入框状态
        text[1].onclick = function () {
            if (innerClickFlag[1] === 0) {
                innerClickFlag[1] = 1;
            }
        }

        // 必须先点击一次输入框内，才判断是否显示提示信息
        document.addEventListener("mousedown", (e) => {
            if (innerClickFlag[1] === 1 && !text[1].value) {
                if (!e.path.includes(box[1])) {
                    // 显示提示信息
                    error[1].innerText = "请输入密码";
                    // 修改输入框悬浮事件
                    box[1].onmouseover = function () {
                        box[1].style.cssText = "border: 1px solid #F56C6C";
                    }
                    box[1].onmouseout = function () {
                        box[1].style.cssText = "border: 1px solid #F56C6C";
                    }
                    // 设置输入框边框样式
                    box[1].style.cssText = "border: 1px solid #F56C6C";
                }
            }
        })

        // 给密码输入框绑定keyup事件
        box[1].addEventListener("keyup", function () {
            if (text[1].value !== null) {
                // 关闭提示信息
                error[1].innerText = "";
                // 启用输入框悬浮事件
                box[1].onmouseover = function () {
                    box[1].style.cssText = "border: 1px solid #3498DB";
                }
                box[1].onmouseout = function () {
                    box[1].style.cssText = "border: 1px solid #B3B6B7";
                }
                // 设置输入框边框样式
                box[1].style.cssText = "border: 1px solid #B3B6B7";
            }
        })
    </script>
    <%--登录验证--%>
    <script>
        let block = document.getElementsByClassName("el-input");
        let inner = document.getElementsByClassName("el-input__inner");
        let accountError = document.getElementById("accountError");
        let pwdError = document.getElementById("pwdError");
        let rememberMeBox = document.getElementById("rememberMe-checkbox");
        let rememberMe = false;  //检查单选框是否选中

        if (rememberMeBox.checked === true) {
            rememberMe = true;
        }

        $("#signinBtn").on("click", function () {
            if (inner[0].value == null || inner[0].value === "") {
                // 显示提示信息
                accountError.innerText = "请输入帐户";
                // 修改输入框悬浮事件
                block[0].onmouseover = function () {
                    block[0].style.cssText = "border: 1px solid #F56C6C";
                }
                block[0].onmouseout = function () {
                    block[0].style.cssText = "border: 1px solid #F56C6C";
                }
                // 设置输入框边框样式
                block[0].style.cssText = "border: 1px solid #F56C6C";
            }
            if (inner[1].value == null || inner[1].value === "") {
                // 显示提示信息
                pwdError.innerText = "请输入密码";
                // 修改输入框悬浮事件
                block[1].onmouseover = function () {
                    block[1].style.cssText = "border: 1px solid #F56C6C";
                }
                block[1].onmouseout = function () {
                    block[1].style.cssText = "border: 1px solid #F56C6C";
                }
                // 设置输入框边框样式
                block[1].style.cssText = "border: 1px solid #F56C6C";
            }
            if (inner[0].value !== "" && inner[1].value !== "") {
                $.ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/signin",
                    dataType: "JSON",
                    async: false,  //设置同步（ajax请求此处需求为跳转而不是局部页面刷新）
                    data: {
                        "account": inner[0].value,
                        "pwdSignin": inner[1].value,
                        "rememberMe": rememberMe
                    },
                    success: function (data) {
                        if (data === 1) {
                            window.location.href = "home.jsp";
                        } else if (data === 0) {
                            // 显示提示信息
                            pwdError.innerText = "帐户或密码错误，请重新输入";
                            // 修改输入框悬浮事件
                            block[1].onmouseover = function () {
                                block[1].style.cssText = "border: 1px solid #F56C6C";
                            }
                            block[1].onmouseout = function () {
                                block[1].style.cssText = "border: 1px solid #F56C6C";
                            }
                            // 设置输入框边框样式
                            block[1].style.cssText = "border: 1px solid #F56C6C";
                        }
                    }
                })
            }
        })
    </script>
</html>