<%--
User: dpj
Date: 2022/11/25
Time: 15:21
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@include file="common.jsp" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>psych心理测试后台管理系统首页</title>
    </head>
    <body class="layui-layout-body close-footer" style="bottom: 0">
        <div class="layui-layout layui-layout-admin" style="bottom: 0">
            <div class="layui-header">
                <div class="layui-logo">psych测试后台管理</div>
                <!-- 头部区域 -->
                <ul class="layui-nav layui-layout-left">
                    <!-- 移动端显示 -->
                    <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm"
                        lay-header-event="menuLeft">
                        <i class="layui-icon layui-icon-spread-left"></i>
                    </li>
                </ul>
                <ul class="layui-nav layui-layout-right">
                    <li class="layui-nav-item layui-hide layui-show-md-inline-block">
                        <a href="javascript:">
                            <img src="${sessionScope.SESSION_AVATAR}" class="layui-nav-img"
                                 alt="头像">${sessionScope.SESSION_USER}
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a href="javascript:" id="personalInfo">
                                <i class="layui-icon layui-icon-username"></i>&ensp; 个人信息</a></dd>
                            <dd><a href="javascript:" id="signOut">
                                <i class="layui-icon layui-icon-logout"></i>&ensp; 注销登录</a></dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <div class="layui-side layui-bg-black">
                <div class="layui-side-scroll">
                    <!-- 左侧导航区域 -->
                    <ul class="layui-nav layui-nav-tree" lay-filter="test">
                        <li class="layui-nav-item">
                            <a class="" href="javascript:"><i
                                    class="layui-icon layui-icon-theme"></i>&ensp;性格测试FPA</a>
                            <dl class="layui-nav-child">
                                <dd><a href="fpa/showQuestionFpa.jsp">题目管理</a></dd>
                                <dd><a href="fpa/showTestPlanFpa.jsp">测试计划</a></dd>
                                <dd><a href="fpa/viewResultFpa.jsp">测试结果</a></dd>
                                <dd><a href="fpa/reportFormsFpa.jsp">报表统计</a></dd>
                            </dl>
                        </li>
                        <li class="layui-nav-item">
                            <a class="" href="javascript:"><i
                                    class="layui-icon layui-icon-friends"></i>&ensp;焦虑测试SAS</a>
                            <dl class="layui-nav-child">
                                <dd><a href="sas/showQuestionSas.jsp">题目管理</a></dd>
                                <dd><a href="sas/showTestPlanSas.jsp">测试计划</a></dd>
                                <dd><a href="sas/viewResultSas.jsp">测试结果</a></dd>
                                <dd><a href="sas/reportFormsSas.jsp">报表统计</a></dd>
                            </dl>
                        </li>
                        <li class="layui-nav-item">
                            <a class="" href="javascript:"><i
                                    class="layui-icon layui-icon-menu-fill"></i>&ensp;抑郁测试SDS</a>
                            <dl class="layui-nav-child">
                                <dd><a href="sds/showQuestionSds.jsp">题目管理</a></dd>
                                <dd><a href="sds/showTestPlanSds.jsp">测试计划</a></dd>
                                <dd><a href="sds/viewResultSds.jsp">测试结果</a></dd>
                                <dd><a href="sds/reportFormsSds.jsp">报表统计</a></dd>
                            </dl>
                        </li>
                        <li class="layui-nav-item">
                            <a class="" href="javascript:"><i
                                    class="layui-icon layui-icon-set-fill"></i>&ensp;系统管理</a>
                            <dl class="layui-nav-child">
                                <dd><a href="sys/showDept.jsp">部门管理</a></dd>
                                <dd><a href="sys/showRole.jsp">角色管理</a></dd>
                                <dd><a href="sys/showMenu.jsp">菜单管理</a></dd>
                                <dd><a href="sys/showUser.jsp">用户管理</a></dd>
                            </dl>
                        </li>
                        <li class="layui-nav-item">
                            <a class="" href="javascript:"><i
                                    class="layui-icon layui-icon-log"></i>&ensp;日志管理</a>
                            <dl class="layui-nav-child">
                                <dd><a href="fpa/logDate.jsp">操作日志</a></dd>
                            </dl>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="layui-body">
                <iframe id="iframMain" width="100%" height="100%" src="hello.jsp">
                </iframe>
            </div>
        </div>
        <script>
            // 关闭layui框架的空白页脚
            $(".layui-body").css("bottom", 0);

            // JavaScript代码区域
            layui.use(['jquery', 'layer'], function () {
                let layer = layui.layer
                    , $ = layui.jquery;

                $(function () {
                    //点击左侧导航栏，右侧显示内容
                    $("dd>a").click(function (e) {
                        //阻断超链接的跳转事件
                        e.preventDefault();
                        //把左侧导航栏超链接的href属性赋值给iframe的src属性
                        $("#iframMain").attr("src", $(this).attr("href"));
                    })
                })
            });
        </script>
        <%--个人信息--%>
        <script>
            $("#personalInfo").on("click", function (e){
                //阻断超链接的跳转事件
                e.preventDefault();
                //把个人信息超链接赋值给iframe的src属性
                $("#iframMain").attr("src", "personalInfo.jsp");
            })
        </script>
        <%--注销登录--%>
        <script>
            $("#signOut").on("click", function () {
                $.ajax({
                    type: "GET",
                    url: "${pageContext.request.contextPath}/signout",
                    async: false,  //设置同步（ajax请求此处需求为跳转而不是局部页面刷新）
                    dataType: "JSON",
                    success: function (data) {
                        if (data === 1) {
                            window.location.href = "signin.jsp";
                        }
                    }
                })
            })
        </script>
    </body>
</html>