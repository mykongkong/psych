<%--
  Created by IntelliJ IDEA.
  User: 情雅圣
  Date: 2022/12/12
  Time: 11:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common.jsp" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta charset="utf-8">
    <title>测试登录页面</title>
    <style type="text/css">
        #recode{
            width: 500px;
            height: 500px;
            background-color:rgb(15, 63, 70,0.5);
            margin: auto;
        }
        body{
            background-color:#175963;
        }
        .holder{
            width: 300px;
            height: 300px;
            position: relative;
            top: 15%;
        }
        .layui-input{
            text-align: center;
            width: 300px;
            height: 40px;
            border: 1px solid #ffffff;
            border-radius:30px;
            background-color:transparent;
            font-size: 18px;
            color: #ffffff;
            margin-top: 10px;
        }
        .layui-btn{
            width: 300px;
            height: 40px;
            border: 1px solid #ffffff;
            border-radius:30px;
            background-color:transparent;
            font-size: 18px;
            color: #ffffff;
            margin-top: 10px;

        }
        #head{
            display: block;
            font-size: 36px;
            color: #fff;
            text-align: center;
            margin-top: 150px;
            padding-top: 40px;
        }
    </style>
</head>
<body>
<form class="layui-form" id="recode">
    <p id="head">心理测试</p>
    <div class="holder">
    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="text" name="name" lay-verify="required|nameOne|title" autocomplete="off" placeholder="请输姓名" class="layui-input" >
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <div class="layui-input-block">
                <input type="tel" name="phone" lay-verify="required|phone" autocomplete="off" placeholder="请输手机号" class="layui-input demo-phone">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="text" name="testCode" lay-verify="required" placeholder="请输入验证码" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="button" class="layui-btn" lay-submit lay-filter="testBtn">开始测试</button>
        </div>
    </div>
    </div>
</form>
<script>
    layui.use(['layer','jquery','form'],function (){
        let layer=layui.layer;
        let $=layui.$;
        let form=layui.form;
        //校验名字
        form.verify({
            nameOne:function (value, item) { //value：表单的值、item：表单的DOM对象
                if (!new RegExp("^[\u4E00-\u9FA5]{2,4}$").test(value)) {
                    return '姓名只能是2到4位的汉字';
                }
            }
        })
        form.on('submit(testBtn)',function (data){
            $.ajax({
                url:'${pageContext.request.contextPath}/question/queryTestCode',
                data:data.field,
                dataType:'JSON',
                type:'POST',
                success:function (ref){
                    if(ref.status){
                        window.location.href="${pageContext.request.contextPath}/question/queryQuestion"
                    }else {
                       layer.msg(ref.message,{icon:5})
                    }
                }
            })
        })
    })
</script>
</body>
</html>
