<%--
  User: dpj
  Date: 2022/11/30
  Time: 23:11
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@include file="common.jsp" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>404</title>
        <link rel="stylesheet" href="static/css/error.css"/>
    </head>
    <body>
        <div class="errorInfo">
            <h1>500</h1>
            <h3>Internal Server Error!</h3>
            <button type="button" class="layui-btn layui-btn-radius layui-btn-fluid"
                    onclick="history.back();">
                Back To Previous
            </button>
        </div>
    </body>
</html>