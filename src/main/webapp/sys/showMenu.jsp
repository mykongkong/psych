<%--
  User: dpj
  Date: 2022/12/6
  Time: 11:12
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@include file="../common.jsp" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>菜单管理</title>
    </head>
    <body>
        <%--数据表格--%>
        <table id="showMenu" lay-filter="tableFilter"></table>
        <%--表头工具条添加功能弹出层--%>
        <form class="layui-form" action="" id="addMenuForm"
              style="display: none; margin-right: 40px; margin-top: 20px">
            <input hidden name="createBy" value="${sessionScope.SESSION_USER}">
            <input hidden name="updateBy" value="${sessionScope.SESSION_USER}">
            <div class="layui-form-item">
                <input type="hidden" name="parentId" id="parent_id">
                <label class="layui-form-label">上级菜单</label>
                <div class="layui-input-inline">
                    <input type="text" name="parentName" placeholder="单击选择上级菜单"
                           autocomplete="off" class="layui-input parent_name">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">菜单类型</label>
                <div class="layui-input-block">
                    <input type="radio" name="menuType" lay-filter="menuType-filter" value="M"
                           title="目录">
                    <input type="radio" name="menuType" lay-filter="menuType-filter" value="C"
                           title="菜单" checked>
                    <input type="radio" name="menuType" lay-filter="menuType-filter" value="F"
                           title="按钮">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">菜单名称</label>
                <div class="layui-input-inline">
                    <input type="text" name="menuName" required lay-verify="required"
                           placeholder="请输入菜单名称" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item menu_url">
                <label class="layui-form-label">请求地址</label>
                <div class="layui-input-inline">
                    <input type="text" name="url" placeholder="请输入请求地址" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item menu_perms">
                <label class="layui-form-label">权限标识</label>
                <div class="layui-input-inline">
                    <input type="text" name="perms" placeholder="请输入权限标识" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">显示排序</label>
                <div class="layui-input-inline">
                    <input type="number" name="orderNum" required lay-verify="required"
                           placeholder="请输入显示排序" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-inline">
                    <input type="text" name="remark" lay-verify="" placeholder="请输入备注信息"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit id="saveMenuButton"
                            lay-filter="formSaveMenu">确定
                    </button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
        <%--行内工具条修改功能弹出层--%>
        <%--上级菜单弹出层之选择菜单权限--%>
        <div class="layui-form-item" id="parentMenu" style="display: none; margin-top: 20px">
            <label class="layui-form-label">菜单权限</label>
            <div class="layui-input-inline">
                <div id="menuTreeChoose" class="demo-tree-more"></div>
            </div>
        </div>
    </body>
    <%--表头工具条--%>
    <script type="text/html" id="headerToolbar">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-xs" lay-event="insert">
                <i class="layui-icon layui-icon-add-1"></i>添加
            </button>
            <button id="expandAll" class="layui-btn layui-btn-xs layui-btn-primary">
                <i class="layui-icon layui-icon-shrink-right"></i>展开全部
            </button>
            <button id="foldAll" class="layui-btn layui-btn-xs layui-btn-primary">
                <i class="layui-icon layui-icon-spread-left"></i>折叠全部
            </button>
        </div>
    </script>
    <%--行内工具条--%>
    <script type="text/html" id="inlineToolbar">
        <a class="layui-btn layui-btn-xs" lay-event="update">修改</a>
        <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="delete">删除</a>
    </script>
    <script>
        //自定义layui组件的目录
        layui.config({
            base: '${pageContext.request.contextPath}/static/layui/modules/'
        }).extend({
            //设定组件别名
            treeTable: 'treeTable',
        });
        layui.use(['layer', 'jquery', 'form', 'treeTable', 'tree', 'util'], function () {
            let layer = layui.layer;
            let $ = layui.jquery;
            let form = layui.form;
            let util = layui.util;
            let treeTable = layui.treeTable;
            let tree = layui.tree;
            let index;

            // 数据表格
            let menuTable = treeTable.render({
                elem: '#showMenu'
                , url: '${pageContext.request.contextPath}/sys/menu/queryAll'
                , toolbar: '#headerToolbar' // 表头工具条，位于左侧
                , title: '菜单信息表'
                , cellMinWidth: 80
                , tree: {
                    iconIndex: 2,
                    isPidData: true,  //开启父子结构
                    idName: 'menuId',
                    pidName: 'parentId'
                }
                , defaultToolbar: ['filter', 'exports', 'print']
                , cols: [
                    [
                        {type: 'numbers'},
                        {field: 'menuId', title: '编号', minWidth: 40},
                        {field: 'menuName', title: '菜单名称', minWidth: 140},
                        {field: 'orderNum', title: '排序', minWidth: 30},
                        {field: 'url', title: '请求地址', minWidth: 80},
                        {
                            field: 'menuType', title: '类型', minWidth: 50,
                            templet: function (type) {
                                if (type.menuType === 'M') {
                                    return '<button type="button" class="layui-btn layui-btn-normal layui-btn-xs">目录</button>'
                                } else if (type.menuType === 'C') {
                                    return '<button type="button" class="layui-btn  layui-btn-xs">菜单</button>'
                                } else {
                                    return '<button type="button" class="layui-btn  layui-btn-warm layui-btn-xs">按钮</button>'
                                }
                            }
                        },
                        {field: 'perms', title: '权限标识'},
                        {field: 'remark', title: '备注', minWidth: 60},
                        {field: 'createTime', title: '创建时间'},
                        {
                            fixed: 'right',
                            align: 'center',
                            toolbar: '#inlineToolbar',
                            title: '操作',
                            width: 130
                        }
                    ]
                ]
                , done: function () {
                    // 修改表格样式
                    $('thead th').css({'text-align': 'center'})
                }
            });

            // 全部展开
            $('#expandAll').click(function () {
                menuTable.expandAll();
            });

            // 全部折叠
            $('#foldAll').click(function () {
                menuTable.foldAll();
            });

            // 监听表头工具条事件
            treeTable.on('toolbar(showMenu)', function (obj) {
                if (obj.event === 'insert') {
                    document.getElementById("addMenuForm").reset();
                    $.ajax({
                        url: '${pageContext.request.contextPath}/sys/menu/queryAllAsTree',
                        dataType: 'JSON',
                        contentType: "application/json;charset=UTF-8",
                        type: 'GET',
                        success: function (result) {
                            console.log(result);
                            tree.render({
                                elem: '#menuTreeChoose'
                                , data: result
                                , onlyIconControl: true
                                , showLine: true
                                , click: function (obj) {
                                    $("#parent_id").val(obj.data.id);
                                    $(".parent_name").val(obj.data.title);
                                    layer.close(index);
                                }
                            });
                        }
                    });
                    layer.open({
                        title: '添加新的菜单',
                        type: 1,
                        skin: 'layui-layer-molv', //弹出层皮肤样式
                        content: $('#addMenuForm'),
                        area: ['400px', '500px'],
                        end: function () {  //防止弹出层关闭后显示在父页面中
                            $('#addMenuForm').css('display', 'none');
                        }
                    });
                }
            });

            // 监听行内工具条事件

            // 弹出层菜单类型单选框样式
            form.on('radio(menuType-filter)', function (data) {
                let menuType = data.value;
                let menuUrlBox = $(".menu_url");
                let menuPermsBox = $(".menu_perms");
                switch (menuType) {
                    case 'M':
                        menuUrlBox.css('display', 'none');
                        menuPermsBox.css('display', 'none');
                        break;
                    case 'C':
                        menuUrlBox.css('display', 'block');
                        menuPermsBox.css('display', 'block');
                        break;
                    case 'F':
                        menuUrlBox.css('display', 'none');
                        menuPermsBox.css('display', 'block');
                        break;
                }
            });

            // 表头工具条添加功能弹出层
            form.on('submit(formSaveMenu)', function (data) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/sys/menu/insert',
                    dataType: 'JSON',
                    type: 'POST',
                    data: data.field,
                    success: function (res) {
                        if (res.status) {
                            layer.closeAll();
                            menuTable.reload();
                            layer.msg('成功添加一条数据！', {icon: 6});
                        } else {
                            layer.msg(res.message, {icon: 7});
                        }
                    }
                });
                return false;
            });

            // 行内工具条修改功能弹出层

            // 点击上级菜单弹出菜单树
            $(".parent_name").click(function () {
                index = layer.open({
                    title: '选择上级菜单',
                    type: 1,
                    skin: 'layui-layer-molv', //弹出层皮肤样式
                    content: $('#parentMenu'),
                    area: ['400px', '350px'],
                    end: function () {  //防止弹出层关闭后显示在父页面中
                        $('#parentMenu').css('display', 'none');
                    }
                });
            });
        })
    </script>
</html>