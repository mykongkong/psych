<%--
  User: dpj
  Date: 2022/11/29
  Time: 15:36
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@include file="../common.jsp" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>角色管理</title>
    </head>
    <body>
        <%--数据表格--%>
        <table id="showTable" lay-filter="tableFilter"></table>
        <%--表头工具条之添加功能弹出层--%>
        <form class="layui-form" action="" id="insertLayer"
              style="display: none;margin-right: 40px;margin-top: 20px">
            <div class="layui-form-item">
                <label class="layui-form-label">角色名称</label>
                <div class="layui-input-block">
                    <input id="roleName" type="text" name="roleName" placeholder="请输入角色名称"
                           required lay-verify="required" autocomplete="off"
                           class="layui-input"><span style="color: red"></span>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">角色关键字</label>
                <div class="layui-input-block">
                    <input id="roleKey" type="text" name="roleKey" placeholder="请输入角色关键字"
                           required lay-verify="required" autocomplete="off"
                           class="layui-input"><span style="color: red"></span>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">显示顺序</label>
                <div class="layui-input-block">
                    <input type="number" name="roleSort" placeholder="请输入显示顺序" required
                           lay-verify="required" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-block">
                    <input type="checkbox" checked="" name="status" lay-skin="switch"
                           lay-filter="switchTest" lay-text="可用|失效">
                </div>
            </div>
            <%--<input hidden name="createBy" th:value="${session.username}">--%>
            <div class="layui-form-item">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <input type="text" name="remark" placeholder="请输入备注" required
                           lay-verify="required" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <%--树形菜单--%>
            <div class="layui-form-item">
                <label class="layui-form-label" style="float: left">菜单权限</label>
                <div style="margin-left: 120px">
                    <div id="insertTree" class="demo-tree-more"></div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit id="insertBtn"
                            lay-filter="insertFilter">确定
                    </button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
        <%--行内工具条之修改功能弹出层--%>
    </body>
    <%--表头工具条--%>
    <script type="text/html" id="headerToolbar">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-sm" lay-event="insert">
                <i class="layui-icon layui-icon-add-1"></i>添加
            </button>
        </div>
    </script>
    <%--行内工具条--%>
    <script type="text/html" id="inlineToolbar">
        <a class="layui-btn layui-btn-xs" lay-event="updateDept">修改</a>
        <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="delete">删除</a>
    </script>
    <%--状态按钮映射函数--%>
    <script type="text/html" id="dataStatus">
        {{# if (d.status === '0'){}}
        <button class="layui-btn layui-btn-xs">可用</button>
        {{# }else {}}
        <button class="layui-btn layui-btn-xs layui-btn-danger">失效</button>
        {{# }}}
    </script>
    <%--layui交互函数--%>
    <script>
        layui.use(['table', 'layer', 'jquery', 'form', 'tree', 'util'], function () {
            let table = layui.table;
            let layer = layui.layer;
            let $ = layui.jquery;
            let form = layui.form;
            let tree = layui.tree
            let util = layui.util

            // 数据表格
            let showTable = table.render({
                id: 'showTable' // 设置渲染后的table
                , elem: '#showTable' // 根据id取首次渲染的table
                , height: 550
                , cellMinWidth: 80
                , url: '${pageContext.request.contextPath}/sys/role/queryAll' // 数据接口
                , toolbar: '#headerToolbar' // 表头工具条，位于左侧
                , defaultToolbar: ['filter', 'exports', 'print'] // 图标工具条，位于表头右侧
                , title: '角色信息表'
                , page: false // 不使用分页
                , cols: [
                    [   // 标题行
                        {field: 'roleId', title: '编号', width: 80, sort: true}
                        , {field: 'roleName', title: '名称', width: 100}
                        , {field: 'roleKey', title: '角色关键字', width: 120}
                        , {field: 'roleSort', title: '显示顺序', width: 120, sort: true}
                        , {field: 'status', title: '状态', width: 70, toolbar: "#dataStatus"}
                        , {field: 'createBy', title: '创建人', width: 80}
                        , {field: 'createTime', title: '创建时间', width: 160, sort: true}
                        , {field: 'updateBy', title: '修改人', width: 80}
                        , {field: 'updateTime', title: '修改时间', width: 160, sort: true}
                        , {fixed: 'right', title: '操作', toolbar: '#inlineToolbar', width: 130}
                    ]
                ]
                , done: function () {
                    // 修改表格样式
                    $('.laytable-cell-1-0-5:not(:first)').css({'padding-top': '4px'})
                    $('thead th').css({'text-align': 'center'})
                    $('table tr td').css({'text-align': 'center'})
                }
            });

            // 监听表头左侧工具条事件
            table.on('toolbar(tableFilter)', function (obj) {
                let checkStatus = table.checkStatus('showTable');
                let data = checkStatus.data;
                switch (obj.event) {
                    case 'insert':
                        document.getElementById('insertLayer').reset();
                        layer.open({
                            type: 1,
                            area: '500px',
                            title: '添加新的角色',
                            skin: 'layui-layer-molv', //弹出层皮肤样式
                            content: $('#insertLayer'),
                            end: function () {  //防止弹出层关闭后显示在父页面中
                                $('#insertLayer').css('display', 'none');
                            }
                        })
                        break;
                }
            })




            // 查询树形菜单的数据，回显在弹出层中
            $.ajax({
                url: '${pageContext.request.contextPath}/sys/menu/queryAllAsTree',
                type: 'GET',
                dataType: 'JSON',
                success: function (selectedMenuList){
                    tree.render({
                        elem: '#insertTree',
                        data: selectedMenuList,
                        showCheckbox: true,
                        id: 'insertTreeMenu'
                    });
                    tree.render({
                        elem: '#updateTree',
                        data: selectedMenuList,
                        showCheckbox: true,
                        id: 'updateTreeMenu'
                    });
                }
            })
        })
    </script>
</html>