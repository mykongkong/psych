<%--
  User: dpj
  Date: 2022/11/26
  Time: 8:19
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@include file="../common.jsp" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>系统管理-部门管理</title>
    </head>
    <body>
        <%--顶部搜索框--%>
        <form class="layui-form" action="" style="margin-top: 10px">
            <div class="layui-inline" style="width: 320px">
                <label class="layui-form-label">部门名称：</label>
                <div class="layui-input-inline">
                    <input type="text" name="deptName" autocomplete="off" placeholder="请输入部门名称"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-inline" style="width: 320px; margin-left: -10px">
                <label class="layui-form-label">创建人：</label>
                <div class="layui-input-inline">
                    <input type="text" name="createBy" autocomplete="off" placeholder="请输入创建人"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <div class="layui-input-inline" style="width: 150px; margin-left: 30px">
                    <button class="layui-btn" lay-submit lay-filter="searchFilter">搜索</button>
                    <button type="reset" class="layui-btn layui-btn-primary" id="resetBtn">重置
                    </button>
                </div>
            </div>
        </form>
        <%--数据表格--%>
        <table id="showTable" lay-filter="tableFilter"></table>
        <%--表头工具条之添加功能弹出层--%>
        <form class="layui-form" action="" id="insertLayer"
              style="display: none; margin-right: 40px; margin-top: 20px">
            <input hidden name="createBy">
            <input hidden name="updateBy">
            <div class="layui-form-item">
                <label class="layui-form-label">部门名称</label>
                <div class="layui-input-block">
                    <input id="deptName" type="text" name="deptName" autocomplete="off"
                           placeholder="请输入部门名称" lay-verify="required" class="layui-input">
                    <span style="color: red"></span>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">显示顺序</label>
                <div class="layui-input-block">
                    <input type="number" name="orderNum" autocomplete="off"
                           placeholder="请输入显示顺序"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-block">
                    <input type="checkbox" checked="" name="status" lay-skin="switch"
                           lay-filter="switchStatus" lay-text="可用|失效">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="insertFilter">确定</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
        <%--行内工具条之修改功能弹出层--%>
        <form class="layui-form" action="" id="updateLayer" lay-filter="updateFilter"
              style="display: none;margin-right: 40px;margin-top: 20px">
            <input hidden name="updateBy">
            <input hidden name="deptId">
            <div class="layui-form-item">
                <label class="layui-form-label">部门名称</label>
                <div class="layui-input-block">
                    <input id="editDeptName" type="text" name="deptName" autocomplete="off"
                           placeholder="请输入部门名称" lay-verify="required"
                           class="layui-input"><span style="color: red"></span>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">显示顺序</label>
                <div class="layui-input-block">
                    <input type="number" name="orderNum" autocomplete="off"
                           placeholder="请输入显示顺序"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-block">
                    <input type="checkbox" checked="" id="status" name="status" lay-skin="switch"
                           lay-filter="switchStatus" lay-text="可用|失效">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="updateBtnFilter">确定</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
    </body>
    <%--表头工具条--%>
    <script type="text/html" id="headerToolbar">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-sm" lay-event="insertDept">
                <i class="layui-icon layui-icon-add-1"></i>添加
            </button>
            <button class="layui-btn layui-btn-sm layui-btn-danger" lay-event="deleteDepts">
                <i class="layui-icon layui-icon-delete"></i>删除
            </button>
        </div>
    </script>
    <%--行内工具条--%>
    <script type="text/html" id="inlineToolbar">
        <a class="layui-btn layui-btn-xs" lay-event="updateDept">修改</a>
        <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="deleteDept">删除</a>
    </script>
    <%--状态按钮映射函数--%>
    <script type="text/html" id="dataStatus">
        {{# if (d.status === '0'){}}
        <button class="layui-btn layui-btn-xs">可用</button>
        {{# }else {}}
        <button class="layui-btn layui-btn-xs layui-btn-danger">失效</button>
        {{# }}}
    </script>
    <%--layui交互函数--%>
    <script>
        layui.use(['table', 'layer', 'jquery', 'form'], function () {
            let table = layui.table;
            let layer = layui.layer;
            let $ = layui.jquery;
            let form = layui.form;

            // 数据表格分页
            let showTable = table.render({
                id: 'showTable' // 设置渲染后的table
                , elem: '#showTable' // 根据id取首次渲染的table
                , height: '550'
                , cellMinWidth: 80
                , url: '${pageContext.request.contextPath}/sys/dept/queryAll' // 数据接口
                , toolbar: '#headerToolbar' // 表头工具条，位于左侧
                , defaultToolbar: ['filter', 'exports', 'print'] // 图标工具条，位于表头右侧
                , title: '部门信息表'
                , page: true //开启分页
                , limit: 10
                , limits: [5, 10, 15]
                , cols: [
                    [   // 标题行
                        {type: 'checkbox', fixed: 'left'}  //复选框
                        , {field: 'deptId', title: '编号', width: 80, sort: true, fixed: true}
                        , {field: 'deptName', title: '部门名称', width: 100}
                        , {field: 'orderNum', title: '排序', width: 80, sort: true}
                        , {field: 'status', title: '状态', width: 70, toolbar: "#dataStatus"}
                        , {field: 'createBy', title: '创建人', width: 80}
                        , {field: 'createTime', title: '创建时间', width: 160, sort: true}
                        , {field: 'updateBy', title: '修改人', width: 80}
                        , {field: 'updateTime', title: '修改时间', width: 160, sort: true}
                        , {fixed: 'right', title: '操作', toolbar: '#inlineToolbar', width: 130}
                    ]
                ]
                , done: function () {
                    // 修改表格样式
                    $('.layui-cell-1-0-0').css({'overflow': 'visible'})
                    $('.laytable-cell-1-0-0').css({'padding-top': '6px'})
                    $('.laytable-cell-1-0-4:not(:first)').css({'padding-top': '4px'})
                    $('thead th').css({'text-align': 'center'})
                    $('table tr td').css({'text-align': 'center'})
                }
            });

            // 顶部搜索框模糊查询
            form.on('submit(searchFilter)', function (data) {
                showTable.reload({
                    url: '${pageContext.request.contextPath}/sys/dept/fuzzyQuery',
                    where: {
                        deptName: data.field.deptName,
                        createBy: data.field.createBy
                    },
                    page: {curr: 1}
                });
                return false;  // 阻止表单跳转
            })

            // 重置模糊查询表单
            $('#resetBtn').click(function () {
                showTable.reload({
                    where: {
                        deptName: '',
                        createBy: ''
                    },
                    page: {curr: 1}
                });  // 刷新table
            })

            // 监听表头左侧工具条事件
            table.on('toolbar(tableFilter)', function (obj) {
                let checkStatus = table.checkStatus('showTable');
                let data = checkStatus.data;
                switch (obj.event) {
                    case 'insertDept':
                        document.getElementById('insertLayer').reset();
                        layer.open({
                            type: 1,
                            area: '500px',
                            title: '添加新的部门',
                            skin: 'layui-layer-molv', //弹出层皮肤样式
                            content: $('#insertLayer'),
                            end: function () {  //防止弹出层关闭后显示在父页面中
                                $('#insertLayer').css('display', 'none');
                            }
                        })
                        break;
                    case 'deleteDepts':
                        if (data.length === 0) {
                            layer.msg('请选择至少一行数据！', {icon: 7});
                        } else {
                            layer.confirm('你确定要删除吗？',
                                {btn: ['确定', '取消']},
                                function (index) {
                                    let deptIds = [];
                                    for (let i = 0; i < data.length; i++) {
                                        deptIds.push(data[i].deptId);
                                    }
                                    $.ajax({
                                        url: '${pageContext.request.contextPath}/sys/dept/deleteByIds',
                                        method: 'POST',
                                        data: 'deptIds=' + deptIds, // jquery获取表单内容
                                        success: function () {
                                            layer.msg('成功删除所选数据！', {icon: 6});
                                            showTable.reload({
                                                page: {curr: 1}
                                            });  //删除数据后刷新table
                                        }
                                    })
                                    layer.close(index)
                                })
                        }
                        break;
                }
            });

            // 表头添加功能弹出层
            form.on('submit(insertFilter)', function (data) {
                if (data.field.status === 'on') {
                    data.field.status = 0;
                } else {
                    data.field.status = 1;
                }
                data.field.createBy = '${sessionScope.SESSION_USER}';
                data.field.updateBy = '${sessionScope.SESSION_USER}';
                console.log("d");
                // 提交数据表单
                if (!isNaN(data.field.orderNum)) {
                    $.ajax({
                        url: '${pageContext.request.contextPath}/sys/dept/insert',
                        method: 'POST',
                        dataType: 'JSON',
                        data: data.field,  //jquery获取表单数据
                        success: function (res) {
                            if (res.status) {
                                layer.closeAll();
                                showTable.reload({
                                    page: {curr: 1}
                                });  //添加数据后刷新table
                                layer.msg('成功添加一条数据！', {icon: 6});
                            } else {
                                layer.msg(res.message, {icon: 7});
                            }
                        }
                    })
                } else {
                    layer.msg('显示顺序只能是数字！');
                }
                return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
            })

            // 监听表格右侧行内工具条事件
            table.on('tool(tableFilter)', function (obj) {
                let data = obj.data;  // 获取当前行数据
                switch (obj.event) {
                    case 'updateDept':
                        form.val('updateFilter', {
                            'deptId': data.deptId,
                            'deptName': data.deptName,
                            'orderNum': data.orderNum,
                            'status': data.status,
                            'updateBy': '${sessionScope.SESSION_USER}'
                        });  // 把当前行信息回显到弹出层表单
                        layer.open({
                            type: 1,
                            area: '500px',
                            title: '修改部门信息',
                            skin: 'layui-layer-molv', //弹出层皮肤样式
                            content: $('#updateLayer'),
                            end: function () {  //防止弹出层关闭后显示在父页面中
                                $('#updateLayer').css('display', 'none');
                            }
                        })
                        break;
                    case 'deleteDept':
                        layer.confirm('你确定要删除吗？',
                            {btn: ['确定', '取消']},
                            function (index) {
                                $.ajax({
                                    url: '${pageContext.request.contextPath}/sys/dept/delete',
                                    method: 'POST',
                                    data: {deptId: obj.data.deptId}, // jquery获取表单内容
                                    success: function () {
                                        layer.msg('成功删除一条数据！', {icon: 6});
                                        showTable.reload({
                                            page: {curr: 1}
                                        });  //删除数据后刷新table
                                    }
                                })
                                layer.close(index)
                            })
                        break;
                }
            })

            // 行内工具条修改功能弹出层
            form.on('submit(updateBtnFilter)', function (data) {
                if (data.field.status === 'on') {
                    data.field.status = 0;
                } else {
                    data.field.status = 1;
                }
                // 提交数据表单
                $.ajax({
                    url: '${pageContext.request.contextPath}/sys/dept/update',
                    method: 'POST',
                    dataType: 'JSON',
                    data: data.field,  //jquery获取表单数据
                    success: function (res) {
                        if (res.status) {
                            layer.closeAll();
                            showTable.reload({
                                page: {curr: 1}
                            });  //修改数据后刷新table
                            layer.msg('成功修改一条数据！', {icon: 6});
                        } else {
                            layer.msg(res.message, {icon: 7});
                        }
                    }
                })
                return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
            })
        });
    </script>
</html>