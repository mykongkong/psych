<%--
  User: dpj
  Date: 2022/12/6
  Time: 11:11
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@include file="../common.jsp" %>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>用户管理</title>
    </head>
    <body>
        <%--顶部搜索框--%>
        <form class="layui-form" action="" style="margin-top: 10px">
            <div class="layui-inline" style="width: 320px">
                <label class="layui-form-label">用户编号：</label>
                <div class="layui-input-inline">
                    <input type="text" name="userid" autocomplete="off" placeholder="请输入用户编号"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-inline" style="width: 320px; margin-left: -10px">
                <label class="layui-form-label">用户名：</label>
                <div class="layui-input-inline">
                    <input type="text" name="username" autocomplete="off" placeholder="请输入用户名"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <div class="layui-input-inline" style="width: 150px; margin-left: 30px">
                    <button class="layui-btn" lay-submit lay-filter="searchFilter">搜索</button>
                    <button type="reset" class="layui-btn layui-btn-primary" id="resetBtn">重置
                    </button>
                </div>
            </div>
        </form>
        <%--数据表格--%>
        <table id="showTable" lay-filter="tableFilter"></table>
        <%--表头工具条添加功能弹出层--%>
        <form class="layui-form" action="" id="insertForm" lay-filter="insertFilter"
              style="display: none; margin-right: 40px; margin-top: 20px">
            <input hidden name="createBy">
            <input hidden name="updateBy">
            <div class="layui-form-item">
                <label class="layui-form-label">部门名称</label>
                <div class="layui-input-inline">
                    <select name="deptid" lay-verify="required" id="deptList">
                        <option selected hidden disabled value="">请选择用户所属部门</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">用户名</label>
                <div class="layui-input-inline">
                    <input type="text" name="username" required lay-verify="required"
                           placeholder="请输入用户名" value=""
                           class="layui-input">
                </div>
                <label></label>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">邮箱</label>
                <div class="layui-input-inline">
                    <input type="text" lay-verify="required|email" name="email" value=""
                           class="layui-input" placeholder="请输入邮箱">
                </div>
                <label></label>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">手机号</label>
                <div class="layui-input-inline">
                    <input type="text" lay-verify="required|phonenumber" name="phonenumber" value=""
                           class="layui-input" placeholder="请输入手机号">
                </div>
                <label></label>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">性别</label>
                <div class="layui-input-block">
                    <input type="radio" name="sex" value="1" title="男" checked>
                    <input type="radio" name="sex" value="0" title="女">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">密码</label>
                <div class="layui-input-inline">
                    <input type="text" name="password" lay-reqtext="" lay-verify="required"
                           placeholder="请输入密码" value="" class="layui-input">
                </div>
                <label></label>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">用户身份</label>
                <div class="layui-input-inline">
                    <select name="roleId" lay-verify="required" id="roleList">
                        <option selected hidden disabled value="">请选择用户所属身份</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-inline">
                    <input type="text" name="remark" value="" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" id="insertSubmitBtn" lay-submit
                            lay-filter="insertFilterSubmit" disabled="disabled"
                            style="background-color: #C2C2C2; cursor: not-allowed">确定
                    </button>
                    <button type="reset" id="insertResetBtn" class="layui-btn layui-btn-primary">重置
                    </button>
                </div>
            </div>
        </form>
        <%--行内工具条修改功能弹出层--%>
        <form class="layui-form" action="" id="updateForm" lay-filter="updateFilter"
              style="display: none; margin-right: 40px; margin-top: 20px">
            <input hidden name="updateBy">
            <input hidden name="userid">
            <div class="layui-form-item">
                <label class="layui-form-label">部门名称</label>
                <div class="layui-input-inline">
                    <select name="deptid" lay-verify="required" id="deptListUpdate">
                        <option selected hidden disabled value="">请选择用户所属部门</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">用户名</label>
                <div class="layui-input-inline">
                    <input type="text" name="username" required lay-verify="required"
                           placeholder="请输入用户名" value=""
                           class="layui-input">
                </div>
                <label></label>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">邮箱</label>
                <div class="layui-input-inline">
                    <input type="text" lay-verify="required|email" name="email" value=""
                           class="layui-input" placeholder="请输入邮箱">
                </div>
                <label></label>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">手机号</label>
                <div class="layui-input-inline">
                    <input type="text" lay-verify="required|phonenumber" name="phonenumber" value=""
                           class="layui-input" placeholder="请输入手机号">
                </div>
                <label></label>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">性别</label>
                <div class="layui-input-block" id="updateSex">
                    <input type="radio" name="sex" value="1" title="男">
                    <input type="radio" name="sex" value="0" title="女">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-inline">
                    <input type="checkbox" name="status" lay-skin="switch" lay-filter="switch"
                           lay-text="可用|失效">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">用户身份</label>
                <div class="layui-input-inline">
                    <select name="roleId" lay-verify="required" id="roleListUpdate">
                        <option selected hidden disabled value="">请选择用户所属身份</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-inline">
                    <input type="text" name="remark" value="" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" id="updateSubmitBtn" lay-submit
                            lay-filter="updateFilterSubmit" disabled="disabled"
                            style="background-color: #C2C2C2; cursor: not-allowed">确定
                    </button>
                    <button type="reset" id="updateResetBtn" class="layui-btn layui-btn-primary">重置
                    </button>
                </div>
            </div>
        </form>
        <%--行内工具条重置密码功能弹出层--%>
        <form class="layui-form" action="" id="resetPwdForm" lay-filter="resetPwdFilter"
              style="display: none; margin-right: 40px; margin-top: 20px">
            <input hidden name="username">
            <div class="layui-form-item">
                <label class="layui-form-label">新密码</label>
                <div class="layui-input-inline">
                    <input type="text" name="password" lay-reqtext="" lay-verify="required"
                           placeholder="请输入新密码" value="" class="layui-input">
                </div>
                <label></label>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" id="resetPwdSubmitBtn" lay-submit
                            lay-filter="resetPwdFilterSubmit" disabled="disabled"
                            style="background-color: #C2C2C2; cursor: not-allowed">确定
                    </button>
                    <button type="reset" id="resetPwdResetBtn"
                            class="layui-btn layui-btn-primary">重置
                    </button>
                </div>
            </div>
        </form>
    </body>
    <%--表头工具条--%>
    <script type="text/html" id="headerToolbar">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-sm" lay-event="insert">
                <i class="layui-icon layui-icon-add-1"></i>添加
            </button>
            <button class="layui-btn layui-btn-sm layui-btn-danger" lay-event="deleteByIds">
                <i class="layui-icon layui-icon-delete"></i>删除
            </button>
        </div>
    </script>
    <%--行内工具条--%>
    <script type="text/html" id="inlineToolbar">
        <a class="layui-btn layui-btn-xs" lay-event="update">修改</a>
        <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="resetPwd">重置密码</a>
        <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="delete">删除</a>
    </script>
    <%--状态按钮映射函数--%>
    <script type="text/html" id="dataStatus">
        {{# if (d.status === '1'){}}
        <button class="layui-btn layui-btn-xs">可用</button>
        {{# }else {}}
        <button class="layui-btn layui-btn-xs layui-btn-danger">失效</button>
        {{# }}}
    </script>
    <script>
        layui.use(['table', 'layer', 'jquery', 'form'], function () {
            let table = layui.table;
            let layer = layui.layer;
            let $ = layui.jquery;
            let form = layui.form;

            // 数据表格分页
            let showTable = table.render({
                id: 'showTable' // 设置渲染后的table
                , elem: '#showTable' // 根据id取首次渲染的table
                , height: '550'
                , cellMinWidth: 80
                , url: '${pageContext.request.contextPath}/sys/user/queryAll' // 数据接口
                , toolbar: '#headerToolbar' // 表头工具条，位于左侧
                , defaultToolbar: ['filter', 'exports', 'print'] // 图标工具条，位于表头右侧
                , title: '部门信息表'
                , page: true //开启分页
                , limit: 10
                , limits: [5, 10, 15]
                , cols: [
                    [   // 标题行
                        {type: 'checkbox', fixed: 'left'}  //复选框
                        , {field: 'userid', title: '用户ID', width: 100, sort: true, fixed: 'left'}
                        , {
                        field: 'dept', title: '部门名称', width: 100,
                        templet: function (d) {
                            return d.dept.deptName;
                        }
                    }
                        , {field: 'username', title: '用户名', width: 100, sort: true}
                        , {
                        field: 'sex', title: '性别', width: 60,
                        templet: function (d) {
                            if (d.sex === '1') {
                                return '男';
                            } else {
                                return '女';
                            }
                        }
                    }
                        , {field: 'phonenumber', title: '手机号', width: 120, sort: true}
                        , {field: 'email', title: '邮箱', width: 180}
                        , {field: 'password', title: '密码', width: 100}
                        , {field: 'salt', title: '盐值', width: 100}
                        , {
                        field: 'avatar', title: '头像', width: 100,
                        templet: function (d) {
                            return '<img width="100px" height="50px" src="' + d.avatar + '"  alt="头像">'
                        }
                    }
                        , {field: 'status', title: '状态', width: 70, toolbar: "#dataStatus"}
                        , {
                        field: 'role', title: '角色', width: 80,
                        templet: function (d) {
                            return d.role.roleName;
                        }
                    }
                        , {field: 'createBy', title: '创建人', width: 80}
                        , {field: 'createTime', title: '创建时间', width: 160, sort: true}
                        , {field: 'updateBy', title: '修改人', width: 80}
                        , {field: 'updateTime', title: '修改时间', width: 160, sort: true}
                        , {field: 'remark', title: '备注', width: 100}
                        , {fixed: 'right', title: '操作', toolbar: '#inlineToolbar', width: 210}
                    ]
                ]
                , done: function () {
                    // 修改表格样式
                    $('.layui-cell-1-0-0').css({'overflow': 'visible'})
                    $('.laytable-cell-1-0-0').css({'padding-top': '6px'})
                    $('.laytable-cell-1-0-10:not(:first)').css({'padding-top': '10px'})
                    $('thead th').css({'text-align': 'center'})
                    $('table tr td').css({'text-align': 'center'})
                }
            })

            // 顶部搜索框模糊查询
            form.on('submit(searchFilter)', function (data) {
                showTable.reload({
                    url: '${pageContext.request.contextPath}/sys/user/fuzzyQuery',
                    where: {
                        userid: data.field.userid,
                        username: data.field.username
                    },
                    page: {curr: 1}
                });
                return false;  // 阻止表单跳转
            })

            // 重置模糊查询表单
            $('#resetBtn').click(function () {
                showTable.reload({
                    where: {
                        userid: '',
                        username: ''
                    },
                    page: {curr: 1}
                });  // 刷新table
            })

            // 表头工具条添加功能弹出层关键信息是否正确填写
            let deptInsertFlag = false;
            let roleInsertFlag = false;
            let usernameInsertFlag = false;
            let emailInsertFlag = false;
            let phoneInsertFlag = false;
            let passwordInsertFlag = false;

            // 表头工具条添加功能校验
            // 表头工具条添加功能弹出层确定按钮
            let insertSubmitBtn = $("#insertSubmitBtn");

            // 表头工具条添加功能弹出层确定按钮禁用
            function InsertSubmitBtnBlur() {
                insertSubmitBtn.attr("disabled", "disabled").attr("style", "background-color: #C2C2C2;" +
                    "cursor: not-allowed");
            }

            // 表头工具条添加功能弹出层确定按钮可用
            function InsertSubmitBtnClear() {
                insertSubmitBtn.removeAttr("disabled", "disabled").removeAttr("style",
                    "background-color: #C2C2C2;" +
                    "cursor: not-allowed");
            }

            // 表头工具条添加功能之校验部门是否正确选择
            $("#deptList").on('change', function () {
                // 检查部门的名称是否正确选择
                console.log(deptInsertFlag);
                let deptOption = $("#deptList option:selected");
                if (deptOption.text() !== "请选择用户所属部门") {
                    deptInsertFlag = true;
                    console.log(deptInsertFlag);
                }
            })

            // 表头工具条添加功能之校验角色是否正确选择

            // 表头工具条添加功能之用户名校验
            $("#insertForm input[name='username']").on('blur', function () {
                let usernameBox = $("#insertForm input[name='username']");
                if (usernameBox.val() === "") {
                    usernameBox.parent().next().html("<span id='usernameInsertError'>用户名不能为空！</span>").attr("style",
                        "color: #FF5722;" +
                        "line-height: 38px");
                    InsertSubmitBtnBlur();
                } else {
                    // 唯一性校验
                    $.ajax({
                        url: '${pageContext.request.contextPath}/sys/user/hasAccount',
                        type: 'GET',
                        dataType: 'JSON',
                        data: {
                            account: usernameBox.val(),
                            accountType: 0
                        },
                        success: function (res) {
                            if (res.status) {
                                usernameBox.parent().next().html("<i id='usernameInsertOk' class='layui-icon layui-icon-ok-circle'></i>").attr("style", "color: #5FB878; line-height: 38px; font-size: 30px");
                                usernameInsertFlag = true;
                            } else {
                                usernameBox.parent().next().html("<span id='usernameInsertError'>用户名已注册！</span>").attr("style",
                                    "color: #FF5722;" +
                                    "line-height: 38px");
                                InsertSubmitBtnBlur();
                            }
                        }
                    })
                }
            })

            // 表头工具条添加功能之邮箱校验
            $("#insertForm input[name='email']").on('blur', function () {
                let emailBox = $("#insertForm input[name='email']");
                let emailReg = new RegExp("^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$");
                if (emailBox.val() === "") {
                    emailBox.parent().next().html("<span id='emailInsertError'>邮箱不能为空！</span>").attr("style",
                        "color: #FF5722;" +
                        "line-height: 38px");
                    InsertSubmitBtnBlur();
                } else {
                    if (!emailReg.test(emailBox.val())) {
                        emailBox.parent().next().html("<span id='emailInsertError'>邮箱格式错误！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        InsertSubmitBtnBlur();
                    } else {
                        // 唯一性校验
                        $.ajax({
                            url: '${pageContext.request.contextPath}/sys/user/hasAccount',
                            type: 'GET',
                            dataType: 'JSON',
                            data: {
                                account: emailBox.val(),
                                accountType: 1
                            },
                            success: function (res) {
                                if (res.status) {
                                    emailBox.parent().next().html("<i id='emailInsertOk' class='layui-icon layui-icon-ok-circle'></i>").attr("style", "color: #5FB878; line-height: 38px; font-size: 30px");
                                    emailInsertFlag = true;
                                } else {
                                    emailBox.parent().next().html("<span id='emailInsertError'>邮箱已注册！</span>").attr("style",
                                        "color: #FF5722;" +
                                        "line-height: 38px");
                                    InsertSubmitBtnBlur();
                                }
                            }
                        })
                    }
                }
            })

            // 表头工具条添加功能之手机号校验
            $("#insertForm input[name='phonenumber']").on('blur', function () {
                let phoneBox = $("#insertForm input[name='phonenumber']");
                let phoneReg = new RegExp("^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$");
                if (phoneBox.val() === "") {
                    phoneBox.parent().next().html("<span id='phoneInsertError'>手机号不能为空！</span>").attr("style",
                        "color: #FF5722;" +
                        "line-height: 38px");
                    InsertSubmitBtnBlur();
                } else {
                    if (!phoneReg.test(phoneBox.val())) {
                        phoneBox.parent().next().html("<span id='phoneInsertError'>手机号格式错误！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        InsertSubmitBtnBlur();
                    } else {
                        // 唯一性校验
                        $.ajax({
                            url: '${pageContext.request.contextPath}/sys/user/hasAccount',
                            type: 'GET',
                            dataType: 'JSON',
                            data: {
                                account: phoneBox.val(),
                                accountType: 2
                            },
                            success: function (res) {
                                if (res.status) {
                                    phoneBox.parent().next().html("<i id='phoneInsertOk' class='layui-icon layui-icon-ok-circle'></i>").attr("style", "color: #5FB878; line-height: 38px; font-size: 30px");
                                    phoneInsertFlag = true;
                                } else {
                                    phoneBox.parent().next().html("<span id='phoneInsertError'>手机号已注册！</span>").attr("style",
                                        "color: #FF5722;" +
                                        "line-height: 38px");
                                    InsertSubmitBtnBlur();
                                }
                            }
                        })
                    }
                }
            })

            // 表头工具条添加功能之密码校验
            let passwordReg =
                /^(?=.{8})(?=.*?[a-zA-Z])(?=.*?\d)(?=.*?[*?!&￥$%^#,./@";:><\[\]}{\-=+_\\|》《。，、？’‘“”~ `]).*$/;
            let lengthReg = /^(?=.{8}).*$/;
            let letterReg = /^(?=.*?[a-zA-Z]).*$/;
            let numberReg = /^(?=.*?\d).*$/;
            let characterReg = /^(?=.*?[*?!&￥$%^#,./@";:><\[\]}{\-=+_\\|》《。，、？’‘“”~ `]).*$/;
            $("#insertForm input[name='password']").on('blur', function () {
                let passwordBox = $("#insertForm input[name='password']");
                let passwordVal = passwordBox.val();
                if (passwordVal === "") {
                    passwordBox.parent().next().html("<span id='passwordInsertError'>密码不能为空！</span>").attr("style",
                        "color: #FF5722;" +
                        "line-height: 38px");
                    InsertSubmitBtnBlur();
                } else if (!passwordReg.test(passwordVal)) {
                    if (!lengthReg.test(passwordVal)) {
                        passwordBox.parent().next().html("<span id='passwordInsertError'>密码不能小于8位！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        InsertSubmitBtnBlur();
                    } else if (!letterReg.test(passwordVal)) {
                        passwordBox.parent().next().html("<span id='passwordInsertError'>密码必须包含至少一个字母！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        InsertSubmitBtnBlur();
                    } else if (!numberReg.test(passwordVal)) {
                        passwordBox.parent().next().html("<span id='passwordInsertError'>密码必须包含至少一个数字！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        InsertSubmitBtnBlur();
                    } else if (!characterReg.test(passwordVal)) {
                        passwordBox.parent().next().html("<span id='passwordInsertError'>密码必须包含至少一个特殊字符！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        InsertSubmitBtnBlur();
                    }
                } else {
                    passwordBox.parent().next().html("<i id='passwordInsertOk' class='layui-icon layui-icon-ok-circle'></i>").attr("style", "color: #5FB878; line-height: 38px; font-size: 30px");
                    passwordInsertFlag = true;
                    InsertSubmitBtnClear();
                }
            })

            // 表头工具条添加功能之重置按钮移除校验信息
            $("#insertResetBtn").on("click", function () {
                // 关键信息没有填写
                deptInsertFlag = false;
                roleInsertFlag = false;
                usernameInsertFlag = false;
                emailInsertFlag = false;
                phoneInsertFlag = false;
                passwordInsertFlag = false;
                // 移除用户名校验信息
                let usernameInsertError = document.getElementById('usernameInsertError');
                if (usernameInsertError) {
                    usernameInsertError.remove();
                }
                let usernameInsertOk = document.getElementById('usernameInsertOk');
                if (usernameInsertOk) {
                    usernameInsertOk.remove();
                }
                // 移除邮箱校验信息
                let emailInsertError = document.getElementById('emailInsertError');
                if (emailInsertError) {
                    emailInsertError.remove();
                }
                let emailInsertOk = document.getElementById('emailInsertOk');
                if (emailInsertOk) {
                    emailInsertOk.remove();
                }
                // 移除手机号校验信息
                let phoneInsertError = document.getElementById('phoneInsertError');
                if (phoneInsertError) {
                    phoneInsertError.remove();
                }
                let phoneInsertOk = document.getElementById('phoneInsertOk');
                if (phoneInsertOk) {
                    phoneInsertOk.remove();
                }
                // 移除密码校验信息
                let passwordInsertError = document.getElementById('passwordInsertError');
                if (passwordInsertError) {
                    passwordInsertError.remove();
                }
                let passwordInsertOk = document.getElementById('passwordInsertOk');
                if (passwordInsertOk) {
                    passwordInsertOk.remove();
                }
                // 确定按钮恢复禁用
                InsertSubmitBtnBlur();
            })

            // 监听表头工具条事件
            table.on('toolbar(tableFilter)', function (obj) {
                let checkStatus = table.checkStatus('showTable');
                let data = checkStatus.data;
                switch (obj.event) {
                    case 'insert':
                        // 重置添加功能弹出层表单内容
                        document.getElementById("insertForm").reset();
                        // 加载部门下拉列表
                        $.ajax({
                            url: '${pageContext.request.contextPath}/sys/user/queryDeptList',
                            type: 'GET',
                            dataType: 'JSON',
                            async: false,
                            success: function (deptListData) {
                                let deptListSelect = $('#deptList');
                                // 清空下拉框
                                deptListSelect.empty();
                                // 添加提示语
                                deptListSelect.append("<option selected hidden disabled value=''>请选择用户所属部门</option>");
                                // 循环填充下拉框
                                $.each(deptListData, function (index, val) {
                                    deptListSelect.append(
                                        "<option value='" + val.deptId + "'>" + val.deptName +
                                        "</option>"
                                    );
                                });
                            }
                        });
                        // 加载角色下拉列表
                        $.ajax({
                            url: '${pageContext.request.contextPath}/sys/user/queryRoleList',
                            type: 'GET',
                            dataType: 'JSON',
                            async: false,
                            success: function (deptListData) {
                                let roleListSelect = $('#roleList');
                                // 清空下拉框
                                roleListSelect.empty();
                                // 添加提示语
                                roleListSelect.append("<option selected hidden disabled value=''>请选择用户所属角色</option>");
                                // 循环填充下拉框
                                $.each(deptListData, function (index, val) {
                                    roleListSelect.append(
                                        "<option value='" + val.roleId + "'>" + val.roleName +
                                        "</option>"
                                    );
                                })
                            }
                        });
                        // 渲染添加功能弹出层表单的两个下拉框
                        form.render('select', 'insertFilter');
                        // 添加功能弹出层
                        layer.open({
                            type: 1,
                            area: '560px',
                            title: '添加用户',
                            skin: 'layui-layer-molv', //弹出层皮肤样式
                            maxmin: true,  //显示最大化和最小化按钮
                            content: $('#insertForm'),
                            end: function () {  //防止弹出层关闭后显示在父页面中
                                $('#insertForm').css('display', 'none');
                            }
                        });
                        break;
                    case 'deleteByIds':
                        if (data.length === 0) {
                            layer.msg('请选择至少一个用户！', {icon: 7});
                        } else {
                            layer.confirm('你确定要删除吗？',
                                {btn: ['确定', '取消']},
                                function (index) {
                                    let userIds = [];
                                    for (let i = 0; i < data.length; i++) {
                                        userIds.push(data[i].userid);
                                    }
                                    $.ajax({
                                        url: '${pageContext.request.contextPath}/sys/user/deleteByIds',
                                        method: 'POST',
                                        data: 'userIds=' + userIds, // jquery获取表单内容
                                        success: function () {
                                            layer.msg('成功删除所选用户！', {icon: 6});
                                            showTable.reload({
                                                page: {curr: 1}
                                            });  //删除数据后刷新table
                                        }
                                    })
                                    layer.close(index);
                                })
                        }
                        break;
                }
            })

            // 表头工具条之添加功能弹出层
            form.on('submit(insertFilterSubmit)', function (data) {
                if (data.field.status === 'on') {
                    data.field.status = 1;
                } else {
                    data.field.status = 0;
                }
                data.field.createBy = '${sessionScope.SESSION_USER}';
                data.field.updateBy = '${sessionScope.SESSION_USER}';
                // 添加功能弹出层的关键信息正确填写后才能提交表单
                // 检查用户身份是否正确选择
                let roleOption = $("#roleList option:selected");
                if (roleOption.text() !== "请选择用户所属身份") {
                    roleInsertFlag = true;
                }
                // 提交数据表单
                if (deptInsertFlag && roleInsertFlag && usernameInsertFlag && emailInsertFlag
                    && phoneInsertFlag && passwordInsertFlag) {
                    InsertSubmitBtnClear();
                    $.ajax({
                        url: '${pageContext.request.contextPath}/sys/user/insert',
                        method: 'POST',
                        dataType: 'JSON',
                        data: data.field,  //jquery获取表单数据
                        success: function (res) {
                            if (res.status) {
                                layer.closeAll();
                                showTable.reload({
                                    page: {curr: 1}
                                });  //添加数据后刷新table
                                layer.msg('成功添加一个用户！', {icon: 6});
                            } else {
                                layer.msg(res.message, {icon: 7});
                            }
                        }
                    })
                }
                return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
            })

            // 行内工具条修改功能关键信息是否填写
            let deptUpdateFlag = false;
            let roleUpdateFlag = false;
            let usernameUpdateFlag = false;
            let emailUpdateFlag = false;
            let phoneUpdateFlag = false;

            // 行内工具条修改功能校验
            // 行内工具条修改功能弹出层确定按钮
            let updateSubmitBtn = $("#updateSubmitBtn");

            // 行内工具条修改功能弹出层确定按钮禁用
            function UpdateSubmitBtnBlur() {
                updateSubmitBtn.attr("disabled", "disabled").attr("style",
                    "background-color: #C2C2C2;" +
                    "cursor: not-allowed");
            }

            // 行内工具条修改功能弹出层确定按钮启用
            function UpdateSubmitBtnClear() {
                updateSubmitBtn.removeAttr("disabled", "disabled").removeAttr("style",
                    "background-color: #C2C2C2;" +
                    "cursor: not-allowed");
            }

            // 行内工具条修改功能之用户名校验
            $("#updateForm input[name='username']").on('blur', function () {
                let usernameBox = $("#updateForm input[name='username']");
                let useridBox = $("#updateForm input[name='userid']");
                if (usernameBox.val() === "") {
                    usernameBox.parent().next().html("<span id='usernameUpdateError'>用户名不能为空！</span>").attr("style",
                        "color: #FF5722;" +
                        "line-height: 38px");
                    UpdateSubmitBtnBlur();
                } else {
                    // 唯一性校验
                    $.ajax({
                        url: '${pageContext.request.contextPath}/sys/user/accountDuplicate',
                        type: 'GET',
                        dataType: 'JSON',
                        data: {
                            account: usernameBox.val(),
                            accountType: 0,
                            userid: useridBox.val()
                        },
                        success: function (res) {
                            if (res.status) {
                                usernameBox.parent().next().html("<i id='usernameUpdateOk' class='layui-icon layui-icon-ok-circle'></i>").attr("style", "color: #5FB878; line-height: 38px; font-size: 30px");
                                usernameUpdateFlag = true;
                            } else {
                                usernameBox.parent().next().html("<span id='usernameUpdaterError'>用户名已注册！</span>").attr("style",
                                    "color: #FF5722;" +
                                    "line-height: 38px");
                                UpdateSubmitBtnBlur();
                            }
                        }
                    })
                }
            })

            // 行内工具条修改功能之邮箱校验
            $("#updateForm input[name='email']").on('blur', function () {
                let emailBox = $("#updateForm input[name='email']");
                let useridBox = $("#updateForm input[name='userid']");
                let emailReg = new RegExp("^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$");
                if (emailBox.val() === "") {
                    emailBox.parent().next().html("<span id='emailUpdateError'>邮箱不能为空！</span>").attr("style",
                        "color: #FF5722;" +
                        "line-height: 38px");
                    UpdateSubmitBtnBlur();
                } else {
                    if (!emailReg.test(emailBox.val())) {
                        emailBox.parent().next().html("<span id='emailUpdateError'>邮箱格式错误！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        UpdateSubmitBtnBlur();
                    } else {
                        // 唯一性校验
                        $.ajax({
                            url: '${pageContext.request.contextPath}/sys/user/accountDuplicate',
                            type: 'GET',
                            dataType: 'JSON',
                            data: {
                                account: emailBox.val(),
                                accountType: 1,
                                userid: useridBox.val()
                            },
                            success: function (res) {
                                if (res.status) {
                                    emailBox.parent().next().html("<i id='emailUpdateOk' class='layui-icon layui-icon-ok-circle'></i>").attr("style", "color: #5FB878; line-height: 38px; font-size: 30px");
                                    emailUpdateFlag = true;
                                } else {
                                    emailBox.parent().next().html("<span id='emailUpdateError'>邮箱已注册！</span>").attr("style",
                                        "color: #FF5722;" +
                                        "line-height: 38px");
                                    UpdateSubmitBtnBlur();
                                }
                            }
                        })
                    }
                }
            })

            // 行内工具条修改功能之手机号校验
            $("#updateForm input[name='phonenumber']").on('blur', function () {
                let phoneBox = $("#updateForm input[name='phonenumber']");
                let useridBox = $("#updateForm input[name='userid']");
                let phoneReg = new RegExp("^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$");
                if (phoneBox.val() === "") {
                    phoneBox.parent().next().html("<span id='phoneUpdateError'>手机号不能为空！</span>").attr("style",
                        "color: #FF5722;" +
                        "line-height: 38px");
                    UpdateSubmitBtnBlur();
                } else {
                    if (!phoneReg.test(phoneBox.val())) {
                        phoneBox.parent().next().html("<span id='phoneUpdateError'>手机号格式错误！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        UpdateSubmitBtnBlur();
                    } else {
                        // 唯一性校验
                        $.ajax({
                            url: '${pageContext.request.contextPath}/sys/user/accountDuplicate',
                            type: 'GET',
                            dataType: 'JSON',
                            data: {
                                account: phoneBox.val(),
                                accountType: 2,
                                userid: useridBox.val()
                            },
                            success: function (res) {
                                if (res.status) {
                                    phoneBox.parent().next().html("<i id='phoneUpdateOk' class='layui-icon layui-icon-ok-circle'></i>").attr("style", "color: #5FB878; line-height: 38px; font-size: 30px");
                                    phoneUpdateFlag = true;
                                    UpdateSubmitBtnClear();
                                } else {
                                    phoneBox.parent().next().html("<span id='phoneUpdateError'>手机号已注册！</span>").attr("style",
                                        "color: #FF5722;" +
                                        "line-height: 38px");
                                    UpdateSubmitBtnBlur();
                                }
                            }
                        })
                    }
                }
            })

            // 行内工具条修改功能确定按钮移除表单校验信息
            $("#updateResetBtn").on('click', function () {
                // 关键信息设置成未填写
                deptUpdateFlag = false;
                roleUpdateFlag = false;
                usernameUpdateFlag = false;
                emailUpdateFlag = false;
                phoneUpdateFlag = false;
                // 清除用户名校验信息
                let usernameUpdateError = document.getElementById('usernameUpdateError');
                if (usernameUpdateError) {
                    usernameUpdateError.remove();
                }
                let usernameUpdateOk = document.getElementById('usernameUpdateOk');
                if (usernameUpdateOk) {
                    usernameUpdateOk.remove();
                }
                // 清除邮箱校验信息
                let emailUpdateError = document.getElementById('emailUpdateError');
                if (emailUpdateError) {
                    emailUpdateError.remove();
                }
                let emailUpdateOk = document.getElementById('emailUpdateOk');
                if (emailUpdateOk) {
                    emailUpdateOk.remove();
                }
                // 清除手机号校验信息
                let phoneUpdateError = document.getElementById('phoneUpdateError');
                if (phoneUpdateError) {
                    phoneUpdateError.remove();
                }
                let phoneUpdateOk = document.getElementById('phoneUpdateOk');
                if (phoneUpdateOk) {
                    phoneUpdateOk.remove();
                }
                // 确定按钮设置为禁用
                UpdateSubmitBtnBlur();
            })

            // 行内工具条重置密码确定按钮
            let resetPwdSubmitBtn = $('#resetPwdSubmitBtn');

            // 行内工具条重置密码确定按钮禁用
            function ResetPwdSubmitBtnBlur() {
                resetPwdSubmitBtn.attr("disabled", "disabled").attr("style",
                    "background-color: #C2C2C2;" +
                    "cursor: not-allowed");
            }

            // 行内工具条重置密码确定按钮启用
            function ResetPwdSubmitBtnClear() {
                resetPwdSubmitBtn.removeAttr("disabled", "disabled").removeAttr("style",
                    "background-color: #C2C2C2;" +
                    "cursor: not-allowed");
            }

            // 行内工具条重置密码校验
            $("#resetPwdForm input[name='password']").on('blur', function () {
                let passwordBox = $("#resetPwdForm input[name='password']");
                let passwordVal = passwordBox.val();
                if (passwordVal === "") {
                    passwordBox.parent().next().html("<span id='passwordResetError'>密码不能为空！</span>").attr("style",
                        "color: #FF5722;" +
                        "line-height: 38px");
                    ResetPwdSubmitBtnBlur();
                } else if (!passwordReg.test(passwordVal)) {
                    if (!lengthReg.test(passwordVal)) {
                        passwordBox.parent().next().html("<span id='passwordResetError'>密码不能小于8位！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        ResetPwdSubmitBtnBlur();
                    } else if (!letterReg.test(passwordVal)) {
                        passwordBox.parent().next().html("<span id='passwordResetError'>密码必须包含至少一个字母！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        ResetPwdSubmitBtnBlur();
                    } else if (!numberReg.test(passwordVal)) {
                        passwordBox.parent().next().html("<span id='passwordResetError'>密码必须包含至少一个数字！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        ResetPwdSubmitBtnBlur();
                    } else if (!characterReg.test(passwordVal)) {
                        passwordBox.parent().next().html("<span id='passwordResetError'>密码必须包含至少一个特殊字符！</span>").attr("style",
                            "color: #FF5722;" +
                            "line-height: 38px");
                        ResetPwdSubmitBtnBlur();
                    }
                } else {
                    passwordBox.parent().next().html("<i id='passwordResetOk' class='layui-icon layui-icon-ok-circle'></i>").attr("style", "color: #5FB878; line-height: 38px; font-size: 30px");
                    ResetPwdSubmitBtnClear();
                }
            })

            // 行内工具条重置密码确定按钮移除密码校验信息
            $("#resetPwdResetBtn").on('click', function () {
                // 清除密码校验信息
                let passwordResetError = document.getElementById('passwordResetError');
                if (passwordResetError) {
                    passwordResetError.remove();
                }
                let passwordResetOk = document.getElementById('passwordResetOk');
                if (passwordResetOk) {
                    passwordResetOk.remove();
                }
                // 确定按钮设置为禁用
                ResetPwdSubmitBtnBlur();
            })

            // 监听行内工具条事件
            table.on('tool(tableFilter)', function (obj) {
                let data = obj.data;  //获取当前行数据
                console.log(data)
                switch (obj.event) {
                    case 'update':
                        // 重置修改功能弹出层表单内容
                        document.getElementById("updateForm").reset();
                        // 加载部门下拉列表
                        $.ajax({
                            url: '${pageContext.request.contextPath}/sys/user/queryDeptList',
                            type: 'GET',
                            dataType: 'JSON',
                            async: false,
                            success: function (deptListData) {
                                let deptListSelect = $('#deptListUpdate');
                                // 清空下拉框
                                deptListSelect.empty();
                                // 添加提示语
                                deptListSelect.append("<option selected hidden disabled value=''>请选择用户所属部门</option>");
                                // 循环填充下拉框
                                $.each(deptListData, function (index, val) {
                                    deptListSelect.append(
                                        "<option value='" + val.deptId + "'>" + val.deptName +
                                        "</option>"
                                    );
                                })
                            }
                        });
                        // 加载角色下拉列表
                        $.ajax({
                            url: '${pageContext.request.contextPath}/sys/user/queryRoleList',
                            type: 'GET',
                            dataType: 'JSON',
                            async: false,
                            success: function (deptListData) {
                                let roleListSelect = $('#roleListUpdate');
                                // 清空下拉框
                                roleListSelect.empty();
                                // 添加提示语
                                roleListSelect.append("<option selected hidden disabled value=''>请选择用户所属角色</option>");
                                // 循环填充下拉框
                                $.each(deptListData, function (index, val) {
                                    roleListSelect.append(
                                        "<option value='" + val.roleId + "'>" + val.roleName +
                                        "</option>"
                                    );
                                })
                            }
                        });
                        // 渲染添加功能弹出层表单的两个下拉框
                        form.render('select', 'updateFilter');
                        // 把当前行信息回显到弹出层表单
                        form.val('updateFilter', {
                            "userid": data.userid,
                            "deptid": data.deptid,
                            "username": data.username,
                            "email": data.email,
                            "phonenumber": data.phonenumber,
                            "sex": data.sex,
                            "roleId": data.roleId,
                            'status': data.status,
                            "remark": data.remark,
                            'updateBy': '${sessionScope.SESSION_USER}'
                        });
                        // 性别回显
                        let sexBox = document.getElementsByName('sex');
                        for (let i = 0; i < sexBox.length; i++) {
                            if (sexBox[i].value === data.sex) {
                                sexBox[i].checked = true;
                                break;
                            }
                        }
                        // 状态回显
                        if (data.status === '1') {
                            $("#updateForm input[type=checkbox]").prop("checked", "checked");
                        } else {
                            $("#updateForm input[type=checkbox]").prop("checked", "");
                        }
                        // 添加功能弹出层
                        layer.open({
                            type: 1,
                            area: '560px',
                            title: '修改用户信息',
                            skin: 'layui-layer-molv', //弹出层皮肤样式
                            maxmin: true,  //显示最大化和最小化按钮
                            content: $('#updateForm'),
                            end: function () {  //防止弹出层关闭后显示在父页面中
                                $('#updateForm').css('display', 'none');
                            }
                        });
                        break;
                    case 'resetPwd':
                        // 重置弹出层表单内容
                        document.getElementById("resetPwdForm").reset();
                        // 弹出层表单数据回显
                        form.val('resetPwdFilter', {
                            username: data.username
                        })
                        // 重置密码弹出层
                        layer.open({
                            type: 1,
                            area: '560px',
                            title: '重置密码',
                            skin: 'layui-layer-molv', //弹出层皮肤样式
                            maxmin: true,  //显示最大化和最小化按钮
                            content: $('#resetPwdForm'),
                            end: function () {  //防止弹出层关闭后显示在父页面中
                                $('#resetPwdForm').css('display', 'none');
                            }
                        });
                        break;
                    case 'delete':
                        layer.confirm('你确定要删除吗？',
                            {btn: ['确定', '取消']},
                            function (index) {
                                $.ajax({
                                    url: '${pageContext.request.contextPath}/sys/user/delete',
                                    method: 'POST',
                                    data: {userid: obj.data.userid}, // jquery获取表单内容
                                    success: function () {
                                        layer.msg('成功删除一条数据！', {icon: 6});
                                        showTable.reload({
                                            page: {curr: 1}
                                        });  //删除数据后刷新table
                                    }
                                })
                                layer.close(index)
                            })
                        break;
                }
            })

            // 行内工具条之修改功能弹出层
            form.on('submit(updateFilterSubmit)', function (data) {
                if (data.field.status === 'on') {
                    data.field.status = 1;
                } else {
                    data.field.status = 0;
                }
                // 修改功能弹出层的关键信息正确填写后才能提交表单
                // 检查部门的名称是否正确选择
                let deptOption = $("#deptListUpdate option:selected");
                if (deptOption.text() !== "请选择用户所属部门") {
                    deptUpdateFlag = true;
                    console.log(deptUpdateFlag);
                }
                // 检查用户身份是否正确选择
                let roleOption = $("#roleListUpdate option:selected");
                if (roleOption.text() !== "请选择用户所属身份") {
                    roleUpdateFlag = true;
                    console.log(roleUpdateFlag);
                }
                // 提交数据表单
                if (deptUpdateFlag && roleUpdateFlag && usernameUpdateFlag && emailUpdateFlag
                    && phoneUpdateFlag) {
                    UpdateSubmitBtnClear();
                    console.log(data);
                    $.ajax({
                        url: '${pageContext.request.contextPath}/sys/user/update',
                        method: 'POST',
                        dataType: 'JSON',
                        data: data.field,  //jquery获取表单数据
                        success: function (res) {
                            if (res.status) {
                                layer.closeAll();
                                showTable.reload({
                                    page: {curr: 1}
                                });  //添加数据后刷新table
                                layer.msg('成功修改用户信息！', {icon: 6});
                            } else {
                                layer.msg(res.message, {icon: 7});
                            }
                        }
                    })
                }
                return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
            })

            // 行内工具条之重置密码弹出层
            form.on('submit(resetPwdFilterSubmit)', function () {
                let usernameBox = $("#resetPwdForm input[name='username']");
                let newPwdBox = $("#resetPwdForm input[name='password']");
                $.ajax({
                    url: '${pageContext.request.contextPath}/sys/user/resetPwd',
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        username: usernameBox.val(),
                        pwdSignin: newPwdBox.val()
                    },
                    success: function (res) {
                        if (res.status) {
                            layer.closeAll();
                            showTable.reload({
                                page: {curr: 1}
                            });  //添加数据后刷新table
                            layer.msg('成功重置密码！', {icon: 6});
                        }
                    }
                })
                return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
            })
        })
    </script>
</html>