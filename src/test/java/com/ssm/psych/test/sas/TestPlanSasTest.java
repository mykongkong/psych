package com.ssm.psych.test.sas;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.TestPlanSas;
import com.ssm.psych.service.sas.TestPlanSasService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-05 10:44
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class TestPlanSasTest {
    @Autowired
    private TestPlanSasService testPlanSasService;
    //测试查询数据
    @Test
    public void testQueryAll(){
        PageInfo<TestPlanSas> sasPageInfo = testPlanSasService.queryAll(1, 10);
        System.out.println(sasPageInfo);
    }
    //测试修改数据
    @Test
    public void testUpdate(){
        TestPlanSas testPlanSas = new TestPlanSas();
        testPlanSas.setId(7);
        testPlanSas.setTestName("我的对的");
        testPlanSas.setTestBegin(new Date());
        testPlanSas.setTestEnd(new Date());
        testPlanSas.setTestCode(586123);
        testPlanSas.setRemark("我是谁");
        AjaxResult update = testPlanSasService.update(testPlanSas);
        System.out.println(update);
    }
    //测试添加数据
    @Test
    public void testInsert(){
        TestPlanSas testPlanSas = new TestPlanSas();
        testPlanSas.setTestName("创造");
        testPlanSas.setTestBegin(new Date());
        testPlanSas.setTestEnd(new Date());
        testPlanSas.setCreateTime(new Date());
        testPlanSas.setTestCode(856752);
        testPlanSas.setRemark("是");
        AjaxResult insert = testPlanSasService.insert(testPlanSas);
        System.out.println(insert);
    }
    //测试删除
    @Test
    public void testDelete(){
        Integer[] id={8,9};
        AjaxResult delete = testPlanSasService.delete(id);
        System.out.println(delete);
    }
    //测试模糊查询
    @Test
    public void testFuzzyQuery(){
        String testName="我";
        String createBy="";
        PageInfo<TestPlanSas> testPlanSasPageInfo = testPlanSasService.fuzzyQuery(testName, createBy);
        System.out.println(testPlanSasPageInfo);
    }
} 
