package com.ssm.psych.test.sas;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.sas.ViewTestResultSasDao;
import com.ssm.psych.domain.ViewTestResultFpa;
import com.ssm.psych.domain.ViewTestResultSas;
import com.ssm.psych.service.sas.ViewTestResultSasService;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-07 19:44
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class ViewResultSasTest {
    @Autowired
    private ViewTestResultSasService viewTestResultSasService;
    @Autowired
    private ViewTestResultSasDao viewTestResultSasDao;
    //测试查询数据
    @Test
    public void testQueryAll(){
        PageInfo<ViewTestResultSas> pageInfo = viewTestResultSasService.queryAll(1, 10);
        System.out.println(pageInfo);
    }
    //测试通过ID查对应的数据
    @Test
    public void testQueryID(){
        PageInfo<ViewTestResultSas> pageInfo = viewTestResultSasService.queryID(215);
        System.out.println(pageInfo);
    }
    //测试删除数据
    @Test
    public void testDelete(){
        Integer[] i={306};
        AjaxResult delete = viewTestResultSasService.delete(i);
        System.out.println(delete);
    }
    @Test
    public void fuzzyQuery(){
        ViewTestResultSas viewTestResultFpa=new ViewTestResultSas();
        viewTestResultFpa.setName("张亚洲");
        PageInfo<ViewTestResultSas> viewTestResultFpaPageInfo = viewTestResultSasService.fuzzyQuery(viewTestResultFpa);
        System.out.println(viewTestResultFpaPageInfo);
    }
    @Test
    public void s(){
        int normal=0;
        int mild=0;
        int middle=0;
        int severe=0;
        for (ViewTestResultSas testResultSas : viewTestResultSasDao.queryAll()) {
            int f=testResultSas.getForward();
            int ins=testResultSas.getInversion();
            int score=f+ins;
            if(score<40){
                normal++;
            }else if (score<=46){
                mild++;
            }else if (score<=55){
                middle++;
            }else if (score>55){
                severe++;
            }
        }
        System.out.println(normal);
        System.out.println(mild);
        System.out.println(middle);
        System.out.println(severe);
    }
} 
