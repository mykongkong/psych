package com.ssm.psych.test.sas;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.QuestionSas;
import com.ssm.psych.service.sas.QuestionSasService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Description 3.焦虑测试-题目管理-测试题目的增删改查
 * @Author dpj
 * @Date 2022-11-26 10:37
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class QuestionSasTest {
    @Autowired
    private QuestionSasService questionSasService;

    @Test
    public void testQueryById() {
        QuestionSas questionSas = questionSasService.queryById(1);
        System.out.println(questionSas);
    }

    @Test
    public void testQueryAll() {
        PageInfo<QuestionSas> pageInfo = questionSasService.queryAll(1, 5);
        System.out.println(pageInfo);
    }
    //测试修改数据
    @Test
    public void testUpdate(){
        QuestionSas questionSas=new QuestionSas();
        questionSas.setQuestion("我想飞");
        questionSas.setOptionA("飞到远方");
        questionSas.setOptionB("飞到太阳系");
        questionSas.setOptionC("飞到你的心里");
        questionSas.setOptionD("飞到世界各地");
        questionSas.setType(1);
        questionSas.setStatus(0);
        questionSas.setId(35);
        AjaxResult update = questionSasService.update(questionSas);
        System.out.println(update);
    }
    //测试添加数据
    @Test
    public void testInsert(){
        QuestionSas questionSas=new QuestionSas();
        questionSas.setQuestion("生存和死亡这是个问题");
        questionSas.setOptionA("生存");
        questionSas.setOptionB("死亡");
        questionSas.setOptionC("彻底疯狂");
        questionSas.setOptionD("心中沉睡的野兽");
        questionSas.setType(1);
        questionSas.setStatus(1);
        AjaxResult insert = questionSasService.insert(questionSas);
        System.out.println(insert);
    }
    //测试模糊查询
    @Test
    public void testFuzzyQuery(){
        QuestionSas questionSas = new QuestionSas();
        questionSas.setQuestion("沉睡");
        PageInfo<QuestionSas> questionSasPageInfo = questionSasService.fuzzyQuery(questionSas);
        System.out.println(questionSasPageInfo);
    }
    //测试删除功能
    @Test
    public void testDelete(){
        Integer[] i={39,40};
        AjaxResult delete = questionSasService.delete(i);
        System.out.println(delete);
    }
}