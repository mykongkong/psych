package com.ssm.psych.test.fpa;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.fpa.QuestionFpaDao;
import com.ssm.psych.domain.QuestionFpa;
import com.ssm.psych.service.fpa.QuestionFpaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 1.性格测试-题目管理-测试题目的增删改查
 * @Author dpj
 * @Date 2022-11-26 10:34
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class QuestionFpaTest {
    @Autowired
    private QuestionFpaService questionFpaService;
    @Autowired
    private QuestionFpaDao questionFpaDao;

    //测试findAll()的查询
    @Test
    public void testFindAll(){
        List<QuestionFpa> all = questionFpaService.findAll(1,10);
        int size = all.size();
        System.out.println(size);
        for (QuestionFpa q:all){
            System.out.println(q);
        }
    }
    //测试add()添加
    @Test
    public void testAdd(){
        QuestionFpa questionFpa1=new QuestionFpa();
        questionFpa1.setQuestion("没有问题");
        questionFpa1.setOptionA("AAA");
        questionFpa1.setOptionB("BBB");
        questionFpa1.setOptionD("DDD");
        questionFpa1.setOptionC("CCC");
        questionFpa1.setType(1);
        questionFpa1.setStatus(0);
        AjaxResult add = questionFpaService.add(questionFpa1);
        System.out.println(add);
    }
    //测试删除数据
    @Test
    public void testDelete(){
        int i = questionFpaService.deleteId(45);
        System.out.println(i);
    }
    //测试修改数据
    @Test
    public void testUpdate(){
        QuestionFpa questionFpa1=new QuestionFpa();
        questionFpa1.setQuestion("有问题");
        questionFpa1.setOptionA("AAA");
        questionFpa1.setOptionB("BBB");
        questionFpa1.setOptionD("DDD");
        questionFpa1.setOptionC("CCC");
        questionFpa1.setType(1);
        questionFpa1.setStatus(0);
        questionFpa1.setId(38);
        questionFpaService.update(questionFpa1);
    }
    //模糊查询数据
    @Test
    public void testFuzzyQuery(){
        QuestionFpa questionFpa1=new QuestionFpa();
        questionFpa1.setCreateBy("admin");
        List<QuestionFpa> questionFpas = questionFpaService.fuzzyQuery(questionFpa1);
        System.out.println(questionFpas);
    }
    //批量删除
    @Test
    public void testDeleteByIds(){

    }
}