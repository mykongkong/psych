package com.ssm.psych.test.fpa;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.dao.fpa.ViewTestResultFpaDao;
import com.ssm.psych.domain.TblResultDiv;
import com.ssm.psych.domain.ViewTestResultFpa;
import com.ssm.psych.service.fpa.ViewTestResultFpaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-05 15:42
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class ViewTestResultFpaTest {
    @Autowired
    private ViewTestResultFpaService viewTestResultFpaService;
    @Autowired
    private ViewTestResultFpaDao viewTestResultFpaDao;
    //测试查询
    @Test
    public void queryAll(){
        PageInfo<ViewTestResultFpa> fpaPageInfo = viewTestResultFpaService.queryAll(1, 10);
        System.out.println(fpaPageInfo);
    }
    //测试删除数据
    @Test
    public void delete(){
        Integer[] idList={29,30};
        AjaxResult delete = viewTestResultFpaService.delete(idList);
        System.out.println(delete);
    }
    //测试模糊查询
    @Test
    public void fuzzyQuery(){
        ViewTestResultFpa viewTestResultFpa=new ViewTestResultFpa();
        viewTestResultFpa.setTestPlanId(1);
        PageInfo<ViewTestResultFpa> viewTestResultFpaPageInfo = viewTestResultFpaService.fuzzyQuery(viewTestResultFpa);
        System.out.println(viewTestResultFpaPageInfo);
    }
    //通过ID查数据
    @Test
    public void testQueryID(){
        PageInfo<ViewTestResultFpa> pageInfo = viewTestResultFpaService.queryID(3);
        System.out.println(pageInfo);
    }
    //测试查询Div数据
    @Test
    public void testQueryDiv(){
        PageInfo<TblResultDiv> div = viewTestResultFpaService.queryDiv(1);
        System.out.println(div);
    }
    @Test
    public void testSelectAll() {
        int redNumber = 0;
        int blueNumber = 0;
        int greenNumber = 0;
        int yellowNumber = 0;
        int reNumber=0;
        for (ViewTestResultFpa resultFpa :viewTestResultFpaDao.queryAll()) {
            //System.out.println(resultFpa);
            int red = resultFpa.getRedCount();
            int green = resultFpa.getGreenCount();
            int blue = resultFpa.getBlueCount();
            int yellow = resultFpa.getYellowCount();
            Integer[] arr = {red, green, blue, yellow};
            Arrays.sort(arr);
            int x=0;
            int max=arr[arr.length-1];
            if(max==red){
                if(x<1){
                    redNumber++;
                }
                x++;
            }
            if(max==green){
                if(x<1){
                   greenNumber++;
                }else if(x==1){
                    redNumber--;
                }
                x++;
            }
            if(max==blue){
                if(x<1){
                    blueNumber++;
                }else if(x==1){
                    greenNumber--;
                }
                x++;
            }
            if(max==yellow){
                if(x<1){
                    yellowNumber++;
                }else if(x==1){
                    blueNumber--;
                }
                x++;
            }
            if(x>1){
                reNumber++;
            }
        }
        System.out.println(redNumber);
        System.out.println(blueNumber);
        System.out.println(greenNumber);
        System.out.println(yellowNumber);
        System.out.println(reNumber);
    }
}
