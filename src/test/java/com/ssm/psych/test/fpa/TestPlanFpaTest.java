package com.ssm.psych.test.fpa;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.TestPlanFpa;
import com.ssm.psych.service.fpa.TestPlanFpaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-01 16:09
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class TestPlanFpaTest {
    @Autowired
    private TestPlanFpaService testPlanFpaService;
    //测试查询所有数据
    @Test
    public void testQueryAll(){
        PageInfo<TestPlanFpa> testPlanFpas = testPlanFpaService.queryAll(1,2);
        System.out.println(testPlanFpas);
    }
    //测试添加数据
    @Test
    public void testInsert(){
        TestPlanFpa testPlanFpa =new TestPlanFpa();
        testPlanFpa.setTestName("计划租房");
        testPlanFpa.setTestBegin(new Date());
        testPlanFpa.setTestEnd(new Date());
        testPlanFpa.setTestCode(556600);
        testPlanFpa.setCreateBy("admin");
        testPlanFpa.setRemark("租房");
        AjaxResult insert = testPlanFpaService.insert(testPlanFpa);
        System.out.println(insert);
    }
    //测试随机数
    @Test
    public void testInteger(){
        int i=new Random().nextInt(10);
        int j=new Random().nextInt(10)*10;
        System.out.println(j);
        int k=new Random().nextInt(10)*100;
        int l=new Random().nextInt(10)*1000;
        int m=new Random().nextInt(10)*10000;
        int n=new Random().nextInt(10)*100000;
        System.out.println(n+m+l+k+j+i);
    }
    //测试修改数据
    @Test
    public void testUpdate(){
        TestPlanFpa testPlanFpa =new TestPlanFpa();
        testPlanFpa.setTestName("测试修改");
        testPlanFpa.setTestBegin(new Date());
        testPlanFpa.setTestEnd(new Date());
        testPlanFpa.setUpdateBy("mys");
        testPlanFpa.setId(20);
        testPlanFpa.setRemark("修改");
        AjaxResult update = testPlanFpaService.update(testPlanFpa);
        System.out.println(update);
    }
    //批量删除数据
    @Test
    public void testDeleteByIds(){
        Integer[] ids={19,20};
        AjaxResult ajaxResult = testPlanFpaService.deleteByIds(ids);
        System.out.println(ajaxResult);
    }
    //模糊查询数据
    @Test
    public void testFuzzyQuery(){
        TestPlanFpa testPlanFpa=new TestPlanFpa();
        testPlanFpa.setTestName("计划");
        PageInfo<TestPlanFpa> testPlanFpaPageInfo = testPlanFpaService.fuzzyQuery(testPlanFpa);
        System.out.println(testPlanFpaPageInfo);

    }
} 
