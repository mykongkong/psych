package com.ssm.psych.test.log;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.TesterVo;
import com.ssm.psych.domain.ViewTestPlan;
import com.ssm.psych.service.test.TblTestResultFpaService;
import com.ssm.psych.service.test.ViewTestPlanService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-13 19:32
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class ViewTestPlanTest {
    @Autowired
    private ViewTestPlanService viewTestPlanService;
    @Autowired
    private TblTestResultFpaService testResultFpaService;
    @Test
    public void testQueryPhone(){
        TesterVo testerVo=new TesterVo();
        testerVo.setPhone("15236226201");
        AjaxResult ajaxResult = testResultFpaService.queryPhone(testerVo);
    }
} 
