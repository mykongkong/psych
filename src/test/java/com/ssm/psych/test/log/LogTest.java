package com.ssm.psych.test.log;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.domain.Log;
import com.ssm.psych.service.log.LogDateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @Description TODO
 * @Author 情雅圣
 * @Date 2022-12-09 16:57
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class LogTest {
    @Autowired
    private LogDateService logDateService;
    @Test
    public void findAll(){
        PageInfo<Log> all = logDateService.findAll(1, 10);
        System.out.println(all);
    }
    @Test
    public void insert(){
        Log log=new Log();
        log.setUserName("64546");
        log.setClassName("name1");
        log.setIP("ip");
        log.setParams("arrays");
        log.setMethodName("methodName");
        log.setCreateTime(new Date());
        logDateService.insert(log);
    }
} 
