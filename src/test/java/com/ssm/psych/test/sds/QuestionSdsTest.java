package com.ssm.psych.test.sds;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.QuestionSds;
import com.ssm.psych.service.sds.QuestionSdsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Description 2.抑郁测试-题目管理-测试题目的增删改查
 * @Author dpj
 * @Date 2022-11-26 10:39
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class QuestionSdsTest {
    @Autowired
    private QuestionSdsService questionSdsService;

    @Test
    public void testQueryById() {
        QuestionSds questionSds = questionSdsService.queryById(1);
        System.out.println(questionSds);
    }

    @Test
    public void testQueryAll() {
        PageInfo<QuestionSds> pageInfo = questionSdsService.queryAll(1, 10);
        System.out.println(pageInfo);
    }

    @Test
    public void testFuzzyQuery() {
        PageInfo<QuestionSds> pageInfo = questionSdsService.fuzzyQuery("希望", "", 1, 5);
        System.out.println(pageInfo);
    }

    @Test
    public void testInsert() {
        QuestionSds questionSds = new QuestionSds();
        questionSds.setQuestion("抑郁测试问题3");
        questionSds.setOptionA("A3");
        questionSds.setOptionB("B3");
        questionSds.setOptionC("C3");
        questionSds.setOptionD("D3");
        questionSds.setType(1);
        questionSds.setStatus(0);
        questionSds.setCreateBy("dpj");
        questionSds.setUpdateBy("dpj");
        AjaxResult result = questionSdsService.insert(questionSds);
        System.out.println(result);
    }

    @Test
    public void testUpdate() {
        QuestionSds questionSds = new QuestionSds();
        questionSds.setId(37);
        questionSds.setQuestion("抑郁测试问题3");
        questionSds.setOptionA("A31");
        questionSds.setOptionB("B3");
        questionSds.setOptionC("C3");
        questionSds.setOptionD("D3");
        questionSds.setType(1);
        questionSds.setStatus(0);
        AjaxResult result = questionSdsService.update(questionSds);
        System.out.println(result);
    }

    @Test
    public void testDelete() {
        AjaxResult result = questionSdsService.delete(31);
        System.out.println(result);
    }

    @Test
    public void testDeleteByIds() {
        Integer[] ids = {32, 33};
        AjaxResult result = questionSdsService.deleteByIds(ids);
        System.out.println(result);
    }
}