package com.ssm.psych.test.sys;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.TableData;
import com.ssm.psych.domain.Dept;
import com.ssm.psych.service.sys.DeptService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Description 4.系统管理-部门管理-测试部门的增删改查
 * @Author dpj
 * @Date 2022-11-26 09:18
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class DeptTest {
    @Autowired
    private DeptService deptService;

    @Test
    public void testQueryById() {
        Dept dept = deptService.queryById(11);
        System.out.println(dept);
    }

    @Test
    public void testQueryAll() {
        PageInfo<Dept> pageInfo = deptService.queryAll(1, 10);
        System.out.println(new TableData<>(pageInfo.getTotal(), pageInfo.getList()));
    }

    @Test
    public void testFuzzyQuery() {
        PageInfo<Dept> pageInfo = deptService.fuzzyQuery("乐", "", 1, 10);
        System.out.println(new TableData<>(pageInfo.getTotal(), pageInfo.getList()));
    }

    @Test
    public void testInsert() {
        Dept dept = new Dept();
        dept.setDeptName("美食部门");
        dept.setOrderNum(1);
        dept.setStatus("0");
        dept.setDelFlag("1");
        dept.setCreateBy("dpj");
        dept.setUpdateBy("dpj");
        AjaxResult result = deptService.insert(dept);
        System.out.println(result);
    }

    @Test
    public void testUpdate() {
        Dept dept = new Dept();
        dept.setDeptId(211);
        dept.setDeptName("美食部门");
        dept.setUpdateBy("dpj4");
        AjaxResult result = deptService.update(dept);
        System.out.println(result);
    }

    @Test
    public void testDelete() {
        AjaxResult result = deptService.delete(189);
        System.out.println(result);
    }

    @Test
    public void testDeleteByIds() {
        Integer[] ids = {198};
        AjaxResult result = deptService.deleteByIds(ids);
        System.out.println(result);
    }
}