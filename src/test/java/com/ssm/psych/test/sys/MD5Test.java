package com.ssm.psych.test.sys;

import com.ssm.psych.domain.User;
import com.ssm.psych.service.sys.UserService;
import com.ssm.psych.utils.MD5Utils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

/**
 * @Description 测试MD5工具类
 * @Author dpj
 * @Date 2022-12-17 09:40
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class MD5Test {
    @Autowired
    private UserService userService;

    @Test
    public void testGetMD5Pwd() {
        String[] md5PwdAndSalt = MD5Utils.getMD5PwdAndSalt("dpj", "12345678s,");
        System.out.println(Arrays.toString(md5PwdAndSalt));
    }

    @Test
    public void testIsMD5Pwd() {
        User user = userService.queryByAccount("psych");
        boolean flag2 = MD5Utils.isMD5Pwd(user.getSalt(), "1234", user.getPassword());
        System.out.println(flag2);
    }

    @Test
    public void testRandom32String() {
        char[] saltBase = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        String salt = RandomStringUtils.random(32, saltBase);
        System.out.println(salt);
    }
}