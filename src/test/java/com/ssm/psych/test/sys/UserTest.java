package com.ssm.psych.test.sys;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.Dept;
import com.ssm.psych.domain.Role;
import com.ssm.psych.domain.User;
import com.ssm.psych.service.sys.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @Description 4.系统管理-用户管理-测试类
 * @Author dpj
 * @Date 2022-12-06 12:19
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class UserTest {
    @Autowired
    private UserService userService;

    @Test
    public void testQueryById() {
        User user = userService.queryById(27);
        System.out.println(user);
    }

    @Test
    public void testQueryByAccount(){
        User user = userService.queryByAccount("admin");
        System.out.println(user);
    }

    @Test
    public void testHasAccount(){
        AjaxResult result = userService.hasAccount("admin", 0);
        System.out.println(result);
    }

    @Test
    public void testAccountDuplicate(){
        AjaxResult result = userService.accountDuplicate("admin", 0, 27);
        System.out.println(result);
    }

    @Test
    public void testQueryByName(){
        User user = userService.queryByName("psych");
        System.out.println(user);
    }

    @Test
    public void testQueryAll() {
        PageInfo<User> pageInfo = userService.queryAll(1, 2);
        System.out.println(pageInfo);
    }

    @Test
    public void testFuzzyQuery(){
        PageInfo<User> pageInfo = userService.fuzzyQuery(27, "", 1, 2);
        System.out.println(pageInfo);
    }

    @Test
    public void testQueryDeptList(){
        List<Dept> deptList = userService.queryDeptList();
        for (Dept dept : deptList) {
            System.out.println(dept.getDeptName());
        }
    }

    @Test
    public void testQueryRoleList(){
        List<Role> roleList = userService.queryRoleList();
        for (Role role : roleList) {
            System.out.println(role.getRoleName());
        }
    }

    @Test
    public void testInsert(){
        User user = new User();
        user.setUsername("dpj16");
        user.setEmail("126545@qq.com");
        user.setPhonenumber("186784589");
        user.setPassword("1234");
        AjaxResult result = userService.insert(user);
        System.out.println(result);
    }

    @Test
    public void testUpdate(){
        User user = new User();
        user.setUserid(28);
        user.setUsername("test34");
        user.setEmail("1267345@qq.com");
        user.setPhonenumber("18888884589");
        user.setPassword("1234");
        AjaxResult result = userService.update(user);
        System.out.println(result);
    }

    @Test
    public void testDelete(){
        AjaxResult result = userService.delete(3);
        System.out.println(result);
    }

    @Test
    public void testDeleteByIds(){
        Integer[] userIds = {4,5};
        AjaxResult result = userService.deleteByIds(userIds);
        System.out.println(result);
    }

    @Test
    public void testResetPwd(){
        AjaxResult result = userService.resetPwd("dpj", "12345678t,");
        System.out.println(result);
    }

    @Test
    public void testModifyPwd(){
        AjaxResult result = userService.modifyPwd("dpj", "12345678s,", "12345678t,");
        System.out.println(result);
    }
}