package com.ssm.psych.test.sys;

import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.common.MenuTree;
import com.ssm.psych.domain.Menu;
import com.ssm.psych.service.sys.MenuService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Description 4.系统管理-菜单管理-测试类
 * @Author dpj
 * @Date 2022-12-07 09:46
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class MenuTest {
    @Autowired
    private MenuService menuService;

    @Test
    public void testQueryById() {
        Menu menu = menuService.queryById(3);
        System.out.println(menu);
    }

    @Test
    public void testQueryByName() {
        Menu menu = menuService.queryByName("系统管理");
        System.out.println(menu);
    }

    @Test
    public void testQueryAll() {
        List<Menu> menuList = menuService.queryAll();
        menuList.forEach(System.out::println);
    }

    @Test
    public void testQueryListByParentId() {
        List<Menu> menuList = menuService.queryListByParentId(1);
        menuList.forEach(System.out::println);
    }

    @Test
    public void testInsert() {
        Menu menu = new Menu();
        menu.setMenuName("系统管理");
        AjaxResult result = menuService.insert(menu);
        System.out.println(result);
    }

    @Test
    public void testMenuTree() {
        List<MenuTree> result = new ArrayList<>();
        List<Menu> menuList = menuService.queryAll();
        int len = menuList.size();
        for (Menu menu : menuList) {
            if (menu.getParentId() == 0) {
                MenuTree level1Node = new MenuTree();
                level1Node.setId(menu.getMenuId());
                level1Node.setTitle(menu.getMenuName());
                result.add(level1Node);
                List<Menu> menuList1 = menuService.queryListByParentId(menu.getMenuId());
                ArrayList<MenuTree> menuTreeList1 = new ArrayList<>();
                level1Node.setChildren(menuTreeList1);
                len--;
                for (Menu menu1 : menuList1) {
                    MenuTree level2Node = new MenuTree();
                    level2Node.setId(menu1.getMenuId());
                    level2Node.setTitle(menu1.getMenuName());
                    menuTreeList1.add(level2Node);
                    List<Menu> menuList2 = menuService.queryListByParentId(menu1.getMenuId());
                    ArrayList<MenuTree> menuTreeList2 = new ArrayList<>();
                    level2Node.setChildren(menuTreeList2);
                    len--;
                    for (Menu menu2 : menuList2) {
                        MenuTree level3Node = new MenuTree();
                        level3Node.setId(menu2.getMenuId());
                        level3Node.setTitle(menu2.getMenuName());
                        menuTreeList2.add(level3Node);
                        len--;
                    }
                }
            }
        }
        System.out.println(result);
        System.out.println(len);
    }
}