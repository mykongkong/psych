package com.ssm.psych.test.sys;

import com.github.pagehelper.PageInfo;
import com.ssm.psych.common.AjaxResult;
import com.ssm.psych.domain.Role;
import com.ssm.psych.service.sys.RoleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @Description 4.系统管理-角色管理-测试类
 * @Author dpj
 * @Date 2022-12-08 08:26
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class RoleTest {
    @Autowired
    private RoleService roleService;

    @Test
    public void testQueryById() {
        Role role = roleService.queryById(1);
        System.out.println(role);
    }

    @Test
    public void testQueryByName() {
        Role role = roleService.queryByName("管理员");
        System.out.println(role);
    }

    @Test
    public void testQueryAll() {
        PageInfo<Role> pageInfo = roleService.queryAll(1, 5);
        System.out.println(pageInfo);
    }

    @Test
    public void testInsert() {
        Role role = new Role();
        role.setRoleId(5);
        role.setRoleName("只因");
        role.setRoleKey("code");
        role.setRoleSort(4);
        role.setStatus("0");
        role.setCreateBy("dpj");
        role.setUpdateBy("dpj");
        role.setRemark("小黑子");
        AjaxResult result = roleService.insert(role);
        System.out.println(result);
    }

    @Test
    public void testUpdate() {
        Role role = new Role();
        role.setRoleId(4);
        role.setRoleName("管理员");
        role.setRoleKey("manager");
        AjaxResult result = roleService.update(role);
        System.out.println(result);
    }

    @Test
    public void testDelete() {
        AjaxResult result = roleService.delete(5);
        System.out.println(result);
    }

    @Test
    public void testInsertRoleMenu() {
        AjaxResult result = roleService.insertRoleMenu(4, 1);
        System.out.println(result);
    }

    @Test
    public void testDeleteRoleMenu() {
        AjaxResult result = roleService.deleteRoleMenu(4, 1);
        System.out.println(result);
    }

    @Test
    public void testQuerySelectedMenuList() {
        List<Integer> selectedMenuList = roleService.querySelectedMenuList(1);
        System.out.println(selectedMenuList);
    }
}